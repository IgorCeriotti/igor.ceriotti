import React from 'react'

const ListaAvaliacoes = props => {
  const { listaEpisodios } = props.location.state
  return listaEpisodios.avaliados.map( e => {
    return <li>{ `${ e.nome } - ${ e.nota }` }</li>
  } )
}

export default ListaAvaliacoes
