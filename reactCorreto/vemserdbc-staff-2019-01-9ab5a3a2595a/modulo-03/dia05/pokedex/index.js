
let numeroAnterior = 0
let clicouNoBotao = false
let numero = 0;

function digitarPokemon() {
  const renderizar = new Renderizar()
  console.log( renderizar.gerarRandom() )
  if ( !clicouNoBotao ) {
    numero = parseInt( document.forms['myForm']['fname'].value, 10 );
  } else if ( clicouNoBotao ) {
    numero = renderizar.gerarRandom()
    clicouNoBotao = false
  }

  const isNumber = !isNaN( numero )
  if ( !isNumber ) {
    alert( 'Digite um id válido' )
  } else if ( numeroAnterior !== numero ) {
    console.log( 'oi' )
    numeroAnterior = numero

    const pokeApi = new PokeApi()
    pokeApi.buscar( numero )
      .then( pokemonServidor => {
        const poke = new Pokemon( pokemonServidor )

        renderizar.renderizarPokemonNaTela( poke )
      } )
  }
}

function gerarPokemonAleatorio() {
  clicouNoBotao = true
  digitarPokemon()
}
