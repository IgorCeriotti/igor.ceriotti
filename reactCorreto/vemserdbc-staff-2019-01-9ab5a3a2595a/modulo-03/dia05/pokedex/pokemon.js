class Pokemon { // eslint-disable-line no-unused-vars
  constructor( objVindoDaApi ) {
    this.nome = objVindoDaApi.name
    this.thumbUrl = objVindoDaApi.sprites.front_default
    this._altura = objVindoDaApi.height
    this._weight = objVindoDaApi.weight

    this.tipos = []
    for ( let i = 0; i < objVindoDaApi.types.length; i += 1 ) {
      const typeName = objVindoDaApi.types[i].type.name
      this.tipos.push( typeName )
    }

    this.stats = []
    for ( let i = 0; i < objVindoDaApi.stats.length; i += 1 ) {
      const statName = objVindoDaApi.stats[i].stat.name
      const statPercentage = objVindoDaApi.stats[i].base_stat
      this.stats.push( statName )
      this.stats.push( statPercentage )
    }
  }

  // pokemon.altura
  get altura() {
    return this._altura * 10
  }

  get weight() {
    return this._weight / 10
  }
}
