  // function Jedi(nome) {
  //   this.nome = nome
  // }
  
  // Jedi.prototype.atacarComSabre = function(){
  //   console.log( `${ this.nome } atacar com sabre!` )
  // }
  
  class Jedi{
    constructor(nome, idade){
      this.nome = nome
      this.idade = idade 
    }

    //problema de undefined pois utiliza this da window
    atacarComSabre(){
      setTimeout(function(){
        console.log( `${ this.nome } atacar com sabre!` )

      }, 1000)
    }

    atacarComBind(){
      let funcao = function(){
        console.log( `${ this.nome } atacar com sabre!` )
      }
      funcao = funcao.bind(this)
      setTimeout(funcao, 1000)
    }

    atacarComSelf() {
      // var that = this
      // var thiz = this
      var self = this
      setTimeout( function() {
        console.log( `${ self.nome } atacar com sabre!` )
      }, 1000 )
    }
  
    atacarComArrowFunctions() {
      setTimeout( () => {
        console.log( `${ this.nome } atacar com sabre!` )
      }, 1000 )
    }
  
  }
  
    var luke = new Jedi('Luke', 23)
    luke.atacarComSabre()
    luke.atacarComArrowFunctions()
    var maceWindu = new Jedi('Mace Windu')
    maceWindu.atacarComSabre()
    maceWindu.atacarComArrowFunctions()


    var JsonSemIdade = JSON.stringify(luke, function(campo, valor){
      if (campo === 'idade'){
        return undefined
      }
      return valor 
    })

    var objInterpretado = JSON.parse(JsonSemIdade, function(campo,valor){  //parse tranforma uma string em objeto para poder ser manipulado
      if (campo === 'nome'){
        return valor.toUpperCase()
      }
      return valor
    })
