//var hello = 'global'
function start(){
    var hello = 'oi'
    console.log(hello)

    // console.log(hello)
    // var hello = 'oi'

    //HOISTING / ASTEAMENTO:  funcoes funciona mesmo fazendo o codigo acima. declaracao de variaveis e tal, nao. 
    //var hello
    //console.log(hello)
    //hello = 'oi'
}
start() //chamada da função 



//template strings

console.log(assim())

function assim() {
    console.log("function assim")
}

var assimFn = function() {
    console.log("var assimFn")
}

console.log(assimFn())

//bugEscopo()

function bugEscopo() {
    // for (var i = 0; i < 5; i++) {
    //     console.log(i)
    // }
    // console.log("i é:" + ++i)
    for (let i = 0; i < 5; i++) {
        console.log(i)
    }
    console.log("i é:" + ++i)
}

// const goku = { nome: "Goku" }
// goku.nome = "Son Goku"
// goku = { name: "Other Goku" } // erro

// https://developer.mozilla.org/en-US/docs/Glossary/Truthy
// https://developer.mozilla.org/en-US/docs/Glossary/Falsy

var nome = "Bernardo"
var cidade = "Porto Alegre"
var objeto = { nome, cidade };

function imprimirCidade( cidade = "Londres" ) {
    console.log(cidade)
}
imprimirCidade()



// IIFE - https://developer.mozilla.org/en-US/docs/Glossary/IIFE
!(function() {
    console.log(arguments[0])
    console.log(`Gostando de JS? ${ arguments[1].gostandoDeJs === true ? 'sim' : 'não' }`)
    function interna() {
        var x = 'x'
        return x
    }
    console.log(`o valor de x é: ${ interna() }!!`)
    var x
})('JavaScript é legal!', { nome: 'Nome', gostandoDeJs: 'VOLTA BLUEJ PMDZ' })
//externa()









//var hello = 'global'
// function start() {
//     // console.log(hello)
//     // var hello = 'oi'
//     var hello
//     console.log(hello)
//     hello = 'oi'
// }
// start()
