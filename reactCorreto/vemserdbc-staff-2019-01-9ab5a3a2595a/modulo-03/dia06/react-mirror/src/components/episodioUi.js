import React,  {Component} from 'react'


export default class EpisodioUi extends Component {
    render () {
        const {episodio} = this.props 
        return (
            <> {/* pode retornar vazia, nao precisa ser div  */}
            <h2>{ episodio.nome }</h2>
            <img src={ episodio.thumbUrl } alt={ episodio.nome }></img>
            <span>{episodio.assistido ? 'Sim' : 'Não'}, já assisti {episodio.qtdVezesAssistido} vez(es) </span>
            <span>{episodio.duracaoEmMin}</span>
            <span>{episodio.temporadaEpisodio}</span>
            <span>{episodio.nota || 'Sem nota'}</span>
            </> 
        )
    }
}