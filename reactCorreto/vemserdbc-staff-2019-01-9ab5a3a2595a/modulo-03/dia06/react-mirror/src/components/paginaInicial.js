
import React, { Component } from 'react';
import MeuInputNumero from '../components/shared/meuInputNumero'
import MeuBotao from '../components/shared/meuBotao';

import ListaAvaliacoes from '../listaAvaliacoes' 

import EpisodioUi from './episodioUi'
import MensagemFlash from './shared/mensagemFlash'


export default class PaginaInicial extends Component {

  constructor(props) {
    super(props)
    this.listaEpisodios = this.props.listaEpisodios
    // definindo estado inicial de um componente
    this.state = {
      episodio: this.listaEpisodios.episodioAleatorio,
      deveExibirMensagem: false,
      mensagem: '',
      deveExibirErro: false
    }
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodioAleatorio
    this.setState({
      episodio, deveExibirMensagem: false
    })
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido(episodio)
    this.setState({
      episodio,
    })
  }

  exibeMensagem = ({ cor, mensagem }) => {
    this.setState({
      cor, mensagem, deveExibirMensagem: true
    })
  }

  registrarNota = ({ valor, erro }) => {
    if (erro) {
      // se tiver erro no campo, atualizamos estado com exibição e já retornamos,
      // para não rodar o código como se estivesse válida a obrigatoriedade
      return this.setState({
        deveExibirErro: erro
      })
    } else {
      this.setState({
        deveExibirErro: erro
      })
    }

    const { episodio } = this.state
    let cor, mensagem
    if (episodio.validarNota(valor)) {
      episodio.avaliar(valor)
      // TODO: centralizar mensagens num arquivo
      cor = 'verde'
      mensagem = 'Registramos sua nota!'
    } else {
      // TODO: centralizar mensagens num arquivo
      cor = 'vermelho'
      mensagem = 'Informar uma nota válida (entre 1 e 5)'
    }
    this.exibeMensagem({ cor, mensagem })

  }

  atualizarMensagem = devoExibir => {
    this.setState({
      deveExibirMensagem: devoExibir
    })
  }

  render() {
    const { deveExibirMensagem, mensagem, cor, episodio, deveExibirErro } = this.state
    return (
      <React.Fragment>
        <MensagemFlash atualizarMensagem={this.atualizarMensagem} cor={cor} deveExibirMensagem={deveExibirMensagem} mensagem={mensagem} segundos={5} />
        <EpisodioUi episodio={episodio} />
        <MeuInputNumero placeholder="1 a 5"
          mensagemCampo="Qual sua nota para este episódio?"
          obrigatorio={true}
          atualizarValor={this.registrarNota}
          deveExibirErro={deveExibirErro}
          visivel={episodio.assistido || false} />
        <div className="botoes">
          <MeuBotao
            cor={'verde'}
            texto={'Próximo'}
            quandoClicar={this.sortear}
          />
          <MeuBotao
            cor={'azul'}
            texto={'Já assisti'}
            quandoClicar={this.marcarComoAssistido}
          />
          <MeuBotao
            cor={'vermelho'}
            texto={'Ver notas'}
            link='avaliacoes'
          />
        
          {/* <button className="btn verde" onClick={ this.sortear }>Próximo</button> */}
          {/* <button className="btn azul" onClick={ this.marcarComoAssistido.bind( this ) }>Já assisti</button> */}
        </div>
      </React.Fragment>

    )
  }


}