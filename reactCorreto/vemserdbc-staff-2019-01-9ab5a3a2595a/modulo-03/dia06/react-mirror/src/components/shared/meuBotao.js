import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import './meuBotao.css'


// export default class MeuBotao extends Component {
//     render(){
//         const {texto, corBotao, onclick} = this.props 

//         return(
//             <React.Fragment>

//                 <button className={`btn ${corBotao}`} onClick={onclick}>{texto}</button>


//             </React.Fragment>
//         )
//     }
// }

// MeuBotao.propTypes = {
//     texto: PropTypes.string.isRequired,
//     onclick: PropTypes.func.isRequired,
//     cor: PropTypes.string,
// }

// MeuBotao.defaultProps = {
//     cor: 'verde'
// }

//simples nao precisa ser uma classe exatamente


const MeuBotao = ({ cor, texto, quandoClicar, link }) => 
  
    <React.Fragment>
      <button className={`btn ${cor}`} onClick={quandoClicar}>{ link === undefined ? texto : <Link to={`/${link}`}> {texto} </Link>}</button>
    </React.Fragment>
 


  MeuBotao.propTypes = {
    texto: PropTypes.string.isRequired,
    quandoClicar: PropTypes.func,
    cor: PropTypes.oneOf(['verde', 'azul', 'vermelho']),
    link: PropTypes.string,
  }

  MeuBotao.defaultProps = {
    cor: 'verde'
  }



export default MeuBotao