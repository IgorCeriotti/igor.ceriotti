import React, {Component} from 'react'
import './mensagemFlash.css'
import PropTypes from 'prop-types'

export default class MensagemFlash extends Component {

    constructor( props ){
        super(props)
        this.idsTimeouts = []
        this.animacao = '' 
    }

    limparTimeouts(){
        this.idsTimeouts.forEach ( clearTimeout )
    }

    componentWillUnmount(){
        this.limparTimeouts()
    }

    componentDidUpdate ( prevProps ) {
        const { deveExibirMensagem, atualizarMensagem, segundos } = this.props
        if (prevProps.deveExibirMensagem !== deveExibirMensagem ) {
          //TODO: limpar timeouts
          this.idsTimeouts.forEach( clearTimeout )
            
    
          const novoTimeout = setTimeout ( () => {
            atualizarMensagem(false)
          }, segundos * 1000)
          this.idsTimeouts.push ( novoTimeout )
        }
      
      }
    
    fechar = () => {
        console.log('fechar')
        this.props.atualizarMensagem(false) 
        /* this.props.deveExibirMensagem = false */ //this.props nao podem ser alteradas! só state. 
    }
    
    render(){
        const {deveExibirMensagem, mensagem, cor} = this.props 
        
        if (this.animacao || deveExibirMensagem){
            this.animacao = deveExibirMensagem ? 'fade-in' : 'fade-out'
        }

        return (
       
        <span onClick={this.fechar } className={`flash ${cor} ${this.animacao}`} >{mensagem}</span>
            
        
        )
    }
}

MensagemFlash.propTypes = {
    mensagem: PropTypes.string.isRequired,
    deveExibirMensagem: PropTypes.bool.isRequired,
    atualizarMensagem: PropTypes.func.isRequired,
    segundos: PropTypes.number,
    cor: PropTypes.string
}

MensagemFlash.defaultProps = {
    cor: 'verde',
    segundos: 5 
}