import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'

import './App.css';
import './components/shared/meuBotao.css'

import PaginaInicial from './components/paginaInicial'
import ListaAvaliacoes from './listaAvaliacoes'
import ListaEpisodios from './models/listaEpisodios'



class App extends Component {
  constructor(props){
    this.listaEpisodios = new ListaEpisodios()
  }
  render() {
    return (
      <div className="App">
        <Router>
          <React.Fragment>
            <header className="App-header">
            <Route
              path='/' exact component={PaginaInicial}
              render={(props) => <PaginaInicial {...props} isAuthed={true} />}
              />
              {/* <Route path="/" exact component={PaginaInicial } /> */}
              
              <Route path="/avaliacoes" component={ListaAvaliacoes} />
            </header>
          </React.Fragment>
        </Router>
      </div>
    );
  }
}

export default App;

