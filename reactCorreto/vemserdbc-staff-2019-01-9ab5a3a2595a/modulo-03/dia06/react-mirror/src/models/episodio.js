// TODO: extrair para arquivo de prototypes
Number.prototype.estaEntre = function ( a, b ) {
     return a <= this && this <= b
    }


export default class Episodio {
    constructor ( nome, duracao, temporada, ordemEpisodio, thumbUrl, qtdVezesAssistido ){
        this.nome = nome
        this.duracao = duracao
        this.temporada = temporada
        this.ordemEpisodio = ordemEpisodio
        this.thumbUrl = thumbUrl
        this.qtdVezesAssistido = qtdVezesAssistido || 0 //tem valor? se tem é o valor dela, se nao é 0
    }


    validarNota( nota ) {
        nota = parseInt(nota)
        //return nota >= 1 && nota <= 5 
        return nota.estaEntre( 1, 5 )

    }

    avaliar(nota){
        this.nota = parseInt(nota)
        this.assistido = true
    }



    get duracaoEmMin () {
        return `${(this.duracao )} min`
    }

    get temporadaEpisodio(){
        return `S${this.temporada.toString().padStart(2, '0')}E${this.ordemEpisodio.toString().padStart(2, '0')}`
    }
}