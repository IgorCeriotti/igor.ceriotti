function cardapioTiaIFood( veggie = true, comLactose = false ) {
  var cardapio = [
    'enroladinho de salsicha',
    'cuca de uva'
  ]

  if ( comLactose ) {
    cardapio = cardapio.concat( 'pastel de queijo' )
  }


  cardapio = cardapio.concat( [
    'pastel de carne',
    'empada de legumes marabijosa'
  ] )

  if ( veggie ) {
    // TODO: remover alimentos com carne (é obrigatório usar splice!)
    const indiceEnroladinho = cardapio.indexOf( 'enroladinho de salsicha' )
    arr = cardapio.splice(  indiceEnroladinho, 1 )

    const indicePastelCarne = cardapio.indexOf( 'pastel de carne' )
    arr = cardapio.splice( indicePastelCarne, 1 )
  }
  return cardapio
}

cardapioTiaIFood() // esperado: [ 'cuca de uva', 'pastel de queijo', 'empada de legumes marabijosa' ]
