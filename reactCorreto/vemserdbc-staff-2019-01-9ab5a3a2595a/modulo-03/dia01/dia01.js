var hello = "hello"

function somar(a,b){
    return a+b 
}

var resultado = somar(1,2)
console.log(resultado)


//Exercicio 4 -closure o nome disso? adicionar - high end function. curring? 
//var adicionar = op1 => op2 => op1 + op2 arrow functions 
function adicionar(operando1){
    return function(operando2){
        return operando1 + operando2
    }
}
console.log(adicionar(3)(4))



//Exercicio 5
function imprimirBRL(val){
    var valInteiro= parseInt(val);
    var valQueb= val- valInteiro;
    
    valQueb = parseFloat(valQueb).toFixed(2)*100
    if(valQueb<10){
      valQueb= "0" + valQueb;
    }
    
    var valComPontos=""
    var cont =0;
    for(var c = valInteiro.toString().length-1;c>=0;c--){
    
      if(cont===3){
         cont=0;
         valComPontos= "." + valComPontos;
      }
      cont++;
      valComPontos=  valInteiro.toString().charAt(c)+ valComPontos;
    
    }
    return "R$" +valComPontos+","+valQueb.toString();
    }
