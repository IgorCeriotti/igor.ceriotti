package br.com.dbccompany.Integracao;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Repository.CorretorRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CorretorIntegrationTest {
    @MockBean
    private CorretorRepository corretorRepository;

    @Autowired
    ApplicationContext context;

    @Before
    public void setUp(){
        Corretor corretor= new Corretor();
        corretor.setNome("Lucia");
        corretor.setEmail("lucia@lucia");
        corretor.setCpf(13);

        Mockito.when(corretorRepository.findByNome(corretor.getNome())).thenReturn(corretor);
        Mockito.when(corretorRepository.findByCpf(corretor.getCpf())).thenReturn(corretor);
        Mockito.when(corretorRepository.findByEmail(corretor.getEmail())).thenReturn(corretor);
    }

    @Test
    public void buscarPorNome(){
        String nome= "Lucia";
        Corretor found = corretorRepository.findByNome(nome);
        assertThat(found.getNome()).isEqualTo(nome);
    }

    @Test
    public void buscarPorEmail(){
        String email= "lucia@lucia";
        Corretor found = corretorRepository.findByEmail(email);
        assertThat(found.getEmail()).isEqualTo(email);
    }

    @Test
    public void buscarPorCpf(){
        long cpf = 13l;
        Corretor found = corretorRepository.findByCpf(cpf);
        assertThat(found.getCpf()).isEqualTo(cpf);
    }

}