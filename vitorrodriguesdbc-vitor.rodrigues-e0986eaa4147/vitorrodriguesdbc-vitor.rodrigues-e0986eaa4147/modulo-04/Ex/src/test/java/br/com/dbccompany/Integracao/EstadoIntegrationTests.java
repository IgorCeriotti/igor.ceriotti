package br.com.dbccompany.Integracao;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Repository.SeguradoraRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EstadoIntegrationTests {
	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private SeguradoraRepository seguradoraRepository;
	
	@Test
	public void buscarSeguradoraPorId() {
		Seguradora seguradora = new Seguradora();
		entityManager.persist(seguradora);
		entityManager.flush();
		
		Seguradora found = seguradoraRepository.findById(seguradora.getId()).get();	
		
		assertThat(found.getId()).isEqualTo(seguradora.getId());
		
	}
}
