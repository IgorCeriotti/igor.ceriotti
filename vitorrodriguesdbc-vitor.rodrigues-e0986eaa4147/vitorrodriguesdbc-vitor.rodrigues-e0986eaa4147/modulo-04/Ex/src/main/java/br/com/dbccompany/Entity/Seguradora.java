package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Seguradora {
	@Id
	@SequenceGenerator(name="SEGURADORA_SEQ",sequenceName="SEGURADORA_SEQ")
	@GeneratedValue(generator="SEGURADORA_SEQ",strategy=GenerationType.SEQUENCE)
	private long id;
	
	@Column(name="NOME")
	private String nome;
	
	@Column(name="CNPJ")
	private long cnpj;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "seguradora_enderecos", joinColumns
	        = {
	        		@JoinColumn(name = "id_seguradora")},
	        inverseJoinColumns
	        = {
	            @JoinColumn(name = "id_enderecos")})
	private List<Enderecos> enderecos = new ArrayList<>();
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "seguradora_servicos", joinColumns
	        = {
	        		@JoinColumn(name = "id_seguradora")},
	        inverseJoinColumns
	        = {
	            @JoinColumn(name = "id_servico")})
	private List<Servicos> servicos = new ArrayList<>();
	
	@OneToMany (mappedBy = "seguradora", cascade = CascadeType.ALL)
	private List<ServicoContratado> servicosContrados = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getCnpj() {
		return cnpj;
	}

	public void setCnpj(long cnpj) {
		this.cnpj = cnpj;
	}

	public List<Enderecos> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Enderecos> enderecos) {
		this.enderecos = enderecos;
	}

	public List<Servicos> getServicos() {
		return servicos;
	}

	public void setServicos(List<Servicos> servicos) {
		this.servicos = servicos;
	}

	public List<ServicoContratado> getServicosContrados() {
		return servicosContrados;
	}

	public void setServicosContrados(List<ServicoContratado> servicosContrados) {
		this.servicosContrados = servicosContrados;
	}


}
