package br.com.dbccompany.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Repository.SeguradoraRepository;

@Service
public class SeguradoraService {
	
	@Autowired
	public SeguradoraRepository seguradoraRepository;
	
	public Seguradora buscarPorCodigo(long id) {
		return seguradoraRepository.findById(id).get();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Seguradora salvar(Seguradora seguradora) {
		
		return seguradoraRepository.save(seguradora);
		
	}
	
	@Transactional(rollbackFor= Exception.class)
	public Seguradora buscarPorNome(String nome) {
		
		return seguradoraRepository.findByNome(nome);
	}
	
	
	public List<Seguradora> buscarTodas() {
		return (List<Seguradora>) seguradoraRepository.findAll();
	}
}
