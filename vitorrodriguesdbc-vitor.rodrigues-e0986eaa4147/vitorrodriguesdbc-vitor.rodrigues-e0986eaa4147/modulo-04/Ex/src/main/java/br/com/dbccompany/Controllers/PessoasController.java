package br.com.dbccompany.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Pessoas;
import br.com.dbccompany.Service.PessoasService;

@Controller
@RequestMapping("/api/pessoas")
public class PessoasController {
	
	@Autowired
	 public PessoasService pessoasService;
	
	@PostMapping(value="/nova")
	@ResponseBody
	public Pessoas novaPessoa(@RequestBody Pessoas pessoa) {
		return pessoasService.salvar(pessoa);
	}
	
	@GetMapping(value="/{codigo}")
	@ResponseBody
	public Pessoas buscarPorId(@PathVariable long codigo) {
		return pessoasService.buscarPorCodigo(codigo);
	}
	
}
