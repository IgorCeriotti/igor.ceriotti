package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Pessoas {
	@Id
	@SequenceGenerator(name="PESSOAS_SEQ",sequenceName="PESSOAS_SEQ")
	@GeneratedValue(generator="PESSOAS_SEQ",strategy=GenerationType.SEQUENCE)
	private long id;
	
	@Column(name="NOME")
	private String nome;
	
	@Column(name="CPF")
	private long cpf;
	
	@Column(name="TELEFONE")
	private long telefone;
	
	@Column(name="PAI")
	private String pai;
	
	@Column(name="MAE")
	private String mae;
	
	@Column(name="EMAIL")
	private String email;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "pessoas_enderecos", joinColumns
	        = {
	        		@JoinColumn(name = "id_pessoas")},
	        inverseJoinColumns
	        = {
	            @JoinColumn(name = "id_enderecos")})
	private List<Enderecos> enderecos = new ArrayList<>();
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getCpf() {
		return cpf;
	}

	public void setCpf(long cpf) {
		this.cpf = cpf;
	}

	public long getTelefone() {
		return telefone;
	}

	public void setTelefone(long telefone) {
		this.telefone = telefone;
	}

	public String getPai() {
		return pai;
	}

	public void setPai(String pai) {
		this.pai = pai;
	}

	public String getMae() {
		return mae;
	}

	public void setMae(String mae) {
		this.mae = mae;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Enderecos> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Enderecos> enderecos) {
		this.enderecos = enderecos;
	}

	

}
