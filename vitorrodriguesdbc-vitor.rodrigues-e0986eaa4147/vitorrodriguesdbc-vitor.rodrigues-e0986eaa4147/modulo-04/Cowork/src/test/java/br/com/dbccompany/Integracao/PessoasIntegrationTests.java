package br.com.dbccompany.Integracao;

import br.com.dbccompany.Entity.Pessoas;
import br.com.dbccompany.Repository.PessoasRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PessoasIntegrationTests {

    @MockBean
    private PessoasRepository pessoasRepository;

    @Autowired
    ApplicationContext context;

    @Before
    public void setUp(){
        Pessoas pessoa = new Pessoas();
        pessoa.setNome("Joao");
        pessoa.setEmail("joao@joao");
        pessoa.setCpf(13);
        pessoa.setMae("Lu");
        pessoa.setPai("La");

        Mockito.when(pessoasRepository.findByNome((pessoa.getNome()))).thenReturn(pessoa);
        Mockito.when(pessoasRepository.findByCpf(pessoa.getCpf())).thenReturn(pessoa);
        Mockito.when(pessoasRepository.findByEmail(pessoa.getEmail())).thenReturn(pessoa);

    }

    @Test
   public void buscarPessoasPorNome() {
        String nome = "Joao";
        Pessoas found = pessoasRepository.findByNome(nome);
        assertThat(found.getNome()).isEqualTo((nome));
    }

    @Test
    public void buscarPorEmail(){
        String email= "joao@joao";
        Pessoas found = pessoasRepository.findByEmail(email);
        assertThat(found.getEmail()).isEqualTo(email);
    }

    @Test
    public void buscarPorCpf(){
        long cpf = 13l;
        Pessoas found = pessoasRepository.findByCpf(cpf);
        assertThat(found.getCpf()).isEqualTo(cpf);
    }
}
