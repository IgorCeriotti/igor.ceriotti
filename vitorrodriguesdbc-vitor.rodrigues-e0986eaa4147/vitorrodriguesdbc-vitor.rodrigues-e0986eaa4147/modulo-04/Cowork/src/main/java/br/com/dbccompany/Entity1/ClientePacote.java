package br.com.dbccompany.Entity1;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
@Table(name = "CLIENTE_PACOTE")
public class ClientePacote {
    @Id
    @SequenceGenerator(name="CLIENTE_PESSOA_SEQ",sequenceName="CLIENTE_PESSOA_SEQ")
    @GeneratedValue(generator="CLIENTE_PESSOA_SEQ",strategy= GenerationType.SEQUENCE)
    private long id;

    @Column(name = "QUANTIDADE")
    private long quantidade;

    @ManyToOne
    @JoinColumn(name="id_cliente")
    private Cliente cliente;

    @ManyToOne
    @JoinColumn(name="id_pacote")
    private Pacote pacote;

    @OneToMany (mappedBy = "clientePacote", cascade = CascadeType.ALL)
    private List<Pagamento> pagamentos = new ArrayList<>();
}
