package br.com.dbccompany.Repository1;

import br.com.dbccompany.Entity1.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {
}
