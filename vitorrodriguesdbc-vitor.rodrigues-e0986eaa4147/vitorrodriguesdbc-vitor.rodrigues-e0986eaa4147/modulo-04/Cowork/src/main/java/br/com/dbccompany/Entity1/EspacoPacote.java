package br.com.dbccompany.Entity1;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
@Table(name = "ESPACO_PACOTE")
public class EspacoPacote {
    @Id
    @SequenceGenerator(name="ESPACO_PACOTE_SEQ",sequenceName="ESPACO_PACOTE_SEQ")
    @GeneratedValue(generator="ESPACO_PACOTE_SEQ",strategy= GenerationType.SEQUENCE)
    private long id;

    @Column(name = "QUANTIDADE")
    private long quantidade;

    @Column(name = "PRAZO")
    private long prazo;

    @ManyToOne
    @JoinColumn(name="id_Espaco")
    private Espaco espaco;

    @ManyToOne
    @JoinColumn(name="id_pacote")
    private Pacote pacote;
}
