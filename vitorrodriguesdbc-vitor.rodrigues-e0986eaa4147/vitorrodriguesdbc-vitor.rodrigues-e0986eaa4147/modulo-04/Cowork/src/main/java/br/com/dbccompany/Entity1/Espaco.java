package br.com.dbccompany.Entity1;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Espaco {
    @Id
    @SequenceGenerator(name="PESSOAS_SEQ",sequenceName="PESSOAS_SEQ")
    @GeneratedValue(generator="PESSOAS_SEQ",strategy= GenerationType.SEQUENCE)
    private long id;

    @Column(name="NOME")
    private String nome;

    @Column(name = "VALOR")
    private double valor;

    @Column(name="QTD_PESSOAS")
    private  long qtdPessoas;

    @OneToMany (mappedBy = "espaco", cascade = CascadeType.ALL)
    private List<EspacoPacote> espacoPacotes = new ArrayList<>();

    @OneToMany (mappedBy = "espaco", cascade = CascadeType.ALL)
    private List<SaldoCliente> saldoClientes = new ArrayList<>();
}
