package br.com.dbccompany.Repository1;

import br.com.dbccompany.Entity1.Contratacao;
import org.springframework.data.repository.CrudRepository;

public interface ContratacaoRepository extends CrudRepository<Contratacao, Long> {
}
