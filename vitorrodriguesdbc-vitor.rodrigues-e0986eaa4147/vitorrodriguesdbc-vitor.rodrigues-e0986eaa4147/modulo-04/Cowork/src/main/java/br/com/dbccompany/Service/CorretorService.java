package br.com.dbccompany.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Repository.CorretorRepository;

@Service
public class CorretorService {
	@Autowired
	public CorretorRepository corretorRepository;
	

	public Corretor buscarPorCodigo(long id) {
		return corretorRepository.findById(id).get();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Corretor salvar(Corretor servicos) {
		
		return corretorRepository.save(servicos);
		
	}
}
