package br.com.dbccompany.Entity1;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Pacote {
    @Id
    @SequenceGenerator(name="PACOTE_SEQ",sequenceName="PACOTE_SEQ")
    @GeneratedValue(generator="PACOTE_SEQ",strategy= GenerationType.SEQUENCE)
    private long id;

    @Column(name = "VALOR")
    private double valor;

    @OneToMany (mappedBy = "pacote", cascade = CascadeType.ALL)
    private List<ClientePacote> clientePacotes = new ArrayList<>();

    @OneToMany (mappedBy = "pacote", cascade = CascadeType.ALL)
    private List<EspacoPacote> espacoPacotes = new ArrayList<>();
}
