package br.com.dbccompany.Repository1;

import br.com.dbccompany.Entity1.Pacote;
import org.springframework.data.repository.CrudRepository;

public interface PacoteRepository  extends CrudRepository<Pacote, Long> {
}
