package br.com.dbccompany.Repository1;

import br.com.dbccompany.Entity1.Espaco;
import org.springframework.data.repository.CrudRepository;

public interface EspacoRepository extends CrudRepository<Espaco, Long> {
}
