package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Segurados extends Pessoas {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	
	@Column(name="QTD_SERVICOS")
	private long qtdServicos;
	
	@OneToMany (mappedBy = "pessoaSegurada", cascade = CascadeType.ALL)
	private List<ServicoContratado> servicosContrados = new ArrayList<>();


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getQtdServicos() {
		return qtdServicos;
	}

	public void setQtdServicos(long qtdServicos) {
		this.qtdServicos = qtdServicos;
	}

	public List<ServicoContratado> getServicosContrados() {
		return servicosContrados;
	}

	public void setServicosContrados(List<ServicoContratado> servicosContrados) {
		this.servicosContrados = servicosContrados;
	}
}
