package br.com.dbccompany.Entity1;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class SaldoCliente {
    @Id
    @SequenceGenerator(name="SALDO_CLIENTE_SEQ",sequenceName="SALDO_CLIENTE_SEQ")
    @GeneratedValue(generator="SALDO_CLIENTE_SEQ",strategy= GenerationType.SEQUENCE)
    private long id;

    @Column(name="QUANTIDADE")
    private long quantidade;

    @Column(name = "VENCIMENTO")
    private long vencimento;

    @ManyToOne
    @JoinColumn(name="id_Espaco")
    private Espaco espaco;

    @ManyToOne
    @JoinColumn(name="id_cliente")
    private Cliente cliente;
}
