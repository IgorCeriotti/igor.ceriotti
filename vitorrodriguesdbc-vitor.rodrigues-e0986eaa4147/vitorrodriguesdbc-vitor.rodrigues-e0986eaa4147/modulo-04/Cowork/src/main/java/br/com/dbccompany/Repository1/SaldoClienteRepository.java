package br.com.dbccompany.Repository1;

import br.com.dbccompany.Entity1.SaldoCliente;
import org.springframework.data.repository.CrudRepository;

public interface SaldoClienteRepository extends CrudRepository<SaldoCliente, Long> {
}
