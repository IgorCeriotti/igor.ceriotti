package br.com.dbccompany.Repository1;

import br.com.dbccompany.Entity1.EspacoPacote;
import org.springframework.data.repository.CrudRepository;

public interface EspacoPacoteRepository extends CrudRepository<EspacoPacote, Long> {
}
