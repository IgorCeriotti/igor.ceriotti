package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Corretor;

public interface  CorretorRepository extends CrudRepository<Corretor, Long>{
	Corretor findByCargo(String cargo);
}
