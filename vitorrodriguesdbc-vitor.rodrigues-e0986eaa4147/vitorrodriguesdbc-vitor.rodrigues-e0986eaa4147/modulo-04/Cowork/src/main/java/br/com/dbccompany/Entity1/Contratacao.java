package br.com.dbccompany.Entity1;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Contratacao {
    @Id
    @SequenceGenerator(name="CONTRATACAO_SEQ",sequenceName="CONTRATACAO_SEQ")
    @GeneratedValue(generator="CONTRATACAO_SEQ",strategy= GenerationType.SEQUENCE)
    private long id;

    @Column(name = "QUANTIDADE")
    private long quantidade;

    @Column(name = "DESCONTO")
    private double desconto;

    @Column(name = "PRAZO")
    private long prazo;

    @OneToMany (mappedBy = "contratacao", cascade = CascadeType.ALL)
    private List<Pagamento> pagamentos = new ArrayList<>();
}
