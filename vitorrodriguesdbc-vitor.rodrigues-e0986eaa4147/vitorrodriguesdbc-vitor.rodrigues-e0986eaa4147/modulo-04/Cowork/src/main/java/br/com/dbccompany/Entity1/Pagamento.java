package br.com.dbccompany.Entity1;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Pagamento {
    @Id
    @SequenceGenerator(name="PAGAMENTO_SEQ",sequenceName="PAGAMENTO_SEQ")
    @GeneratedValue(generator="PAGAMENTO_SEQ",strategy= GenerationType.SEQUENCE)
    private long id;

    @ManyToOne
    @JoinColumn(name="id_cliente_pacote")
    private ClientePacote clientePacote;

    @ManyToOne
    @JoinColumn(name="id_contratacao")
    private Contratacao contratacao;
}
