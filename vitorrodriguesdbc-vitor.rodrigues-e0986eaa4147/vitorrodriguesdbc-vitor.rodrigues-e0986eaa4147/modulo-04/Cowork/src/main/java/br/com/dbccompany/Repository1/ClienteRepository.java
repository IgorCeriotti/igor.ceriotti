package br.com.dbccompany.Repository1;

import br.com.dbccompany.Entity1.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {
}
