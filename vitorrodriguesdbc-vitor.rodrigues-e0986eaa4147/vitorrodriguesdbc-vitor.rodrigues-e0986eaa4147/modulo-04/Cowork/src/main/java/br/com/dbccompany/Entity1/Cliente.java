package br.com.dbccompany.Entity1;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Cliente {
    @Id
    @SequenceGenerator(name="CLIENTE_SEQ",sequenceName="CLIENTE_SEQ")
    @GeneratedValue(generator="CLIENTE_SEQ",strategy= GenerationType.SEQUENCE)
    private long id;

    @Column(name="NOME")
    private String nome;

    @Column(name="CPF")
    private long cpf;

    @Column(name = "DATA_NASCIMENTO")
    private long data;

    @OneToMany (mappedBy = "cliente", cascade = CascadeType.ALL)
    private List<ClientePacote> clientePacotes = new ArrayList<>();

    @OneToMany (mappedBy = "cliente", cascade = CascadeType.ALL)
    private List<SaldoCliente> saldoClientes = new ArrayList<>();
}
