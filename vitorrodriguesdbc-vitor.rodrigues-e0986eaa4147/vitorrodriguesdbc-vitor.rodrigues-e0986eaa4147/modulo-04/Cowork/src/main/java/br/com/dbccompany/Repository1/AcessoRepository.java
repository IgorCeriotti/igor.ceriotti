package br.com.dbccompany.Repository1;

import br.com.dbccompany.Entity1.Acesso;
import org.springframework.data.repository.CrudRepository;

public interface AcessoRepository extends CrudRepository<Acesso, Long> {

    Acesso findByNome(String nome);
}
