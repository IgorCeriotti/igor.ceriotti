package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Servicos {
	@Id
	@SequenceGenerator(name="SERVICOS_SEQ",sequenceName="SERVICOS_SEQ")
	@GeneratedValue(generator="SERVICOS_SEQ",strategy=GenerationType.SEQUENCE)
	private long id;
	
	@Column(name="NOME")
	private String nome;
	
	@Column(name="DESCRICAO")
	private String descricao;
	
	@Column(name="VALOR_PADRAO")
	private boolean valorPadrao;
	
	@ManyToMany(mappedBy = "servicos")
    private List<Seguradora> seguradoras = new ArrayList<>();
	
	@OneToMany (mappedBy = "servico", cascade = CascadeType.ALL)
	private List<ServicoContratado> servicosContrados = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isValorPadrao() {
		return valorPadrao;
	}

	public void setValorPadrao(boolean valorPadrao) {
		this.valorPadrao = valorPadrao;
	}

	public List<Seguradora> getSeguradoras() {
		return seguradoras;
	}

	public void setSeguradoras(List<Seguradora> seguradoras) {
		this.seguradoras = seguradoras;
	}

	public List<ServicoContratado> getServicosContrados() {
		return servicosContrados;
	}

	public void setServicosContrados(List<ServicoContratado> servicosContrados) {
		this.servicosContrados = servicosContrados;
	}

}
