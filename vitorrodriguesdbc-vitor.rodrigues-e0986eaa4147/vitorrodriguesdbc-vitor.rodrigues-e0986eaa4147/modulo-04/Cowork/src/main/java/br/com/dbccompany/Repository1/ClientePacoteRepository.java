package br.com.dbccompany.Repository1;

import br.com.dbccompany.Entity1.ClientePacote;
import org.springframework.data.repository.CrudRepository;

public interface ClientePacoteRepository extends CrudRepository<ClientePacote, Long> {
}
