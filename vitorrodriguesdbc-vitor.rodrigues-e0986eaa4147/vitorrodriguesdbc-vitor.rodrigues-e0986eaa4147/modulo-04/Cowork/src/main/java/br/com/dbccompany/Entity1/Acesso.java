package br.com.dbccompany.Entity1;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Acesso {
    @Id
    @SequenceGenerator(name="ACESSO_SEQ",sequenceName="ACESSO_SEQ")
    @GeneratedValue(generator="ACESSO_SEQ",strategy= GenerationType.SEQUENCE)
    private long id;

    @Column(name = "DATA")
    private long data;
}
