/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jogos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 *
 * @author tiago
 */
@Entity
@PrimaryKeyJoinColumn(name = "id_conta")
public class ContaCorrente extends Conta {

    @Column(nullable = false, precision = 10, scale = 2)
    private Double limite;
    
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private ContaType tipo = ContaType.CORRENTE;

    public Double getLimite() {
        return limite;
    }

    public void setLimite(Double limite) {
        this.limite = limite;
    }

}
