package br.com.dbccompany.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Service.AgenciaService;


@Controller
@RequestMapping("/api/agencia")
public class AgenciaController {
	
	@Autowired
	 public AgenciaService agenciaService;
	
	@PostMapping(value="/nova")
	@ResponseBody
	public Agencia novaAgencia(@RequestBody Agencia agencia) {
		return agenciaService.salvar(agencia);
	}
}
