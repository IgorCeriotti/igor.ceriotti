package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;


@Entity
public class Conta {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	
	@Column(name="NUMERO")
	private long numero;
	
	@Column(name="TIPO")
	private String tipo;
	
	@ManyToOne
	@JoinColumn(name="id_agencia")
	private Agencia agencia;
	
	@ManyToOne
	@JoinColumn(name="id_tipo_conta")
	private TipoConta tipoConta;
	
	@ManyToMany(mappedBy = "contas")
    private List<Cliente> clientes = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getNumero() {
		return numero;
	}

	public void setNumero(long numero) {
		this.numero = numero;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
}
