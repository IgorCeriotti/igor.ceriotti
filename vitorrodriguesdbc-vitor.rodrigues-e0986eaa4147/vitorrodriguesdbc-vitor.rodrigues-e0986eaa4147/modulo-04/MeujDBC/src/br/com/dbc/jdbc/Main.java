/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vitor.rodrigues
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Connection conn = Connector.connect();
        try {
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'USUARIO'").executeQuery();
            if (!rs.next()) {
                conn.prepareStatement("CREATE TABLE USUARIO(\n"
                        + "ID NUMBER NOT NULL PRIMARY KEY,"
                        + "NOME VARCHAR(100) NOT NULL,"
                        + "APELIDO VARCHAR(15) NOT NULL,"
                        + "SENHA VARCHAR(15) NOT NULL,"
                        + "CPF NUMBER NOT NULL"
                        + ")").execute();
            }

            PreparedStatement pst = conn.prepareStatement("insert into usuario (id,nome,apelido,senha,cpf)"
                    + "values(usuario_seq.nextval,?,?,?,?)");
            pst.setString(1, "joao");
            pst.setString(2, "joaozinho");
            pst.setString(3, "123");
            pst.setInt(4, 1234);
            pst.executeUpdate();

            rs = conn.prepareStatement("select $from usuario").executeQuery();

            while (rs.next()) {
                System.out.println("Id do usuaurio" + rs.getInt("id"));
                System.out.println("Nome do usuaurio" + rs.getString("nome"));
                System.out.println(String.format("apelido do usuario: %s", rs.getString("apelido")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Erro na consulta main", ex);
        }
    }

}
