/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author vitor.rodrigues
 */
@Entity
@SequenceGenerator(name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
public class Cliente {
    @Id
    @GeneratedValue(generator = "CLIENTE_SEQ")
    
    private Integer id;
    private String nome;
    private Integer cpf;
    
}
