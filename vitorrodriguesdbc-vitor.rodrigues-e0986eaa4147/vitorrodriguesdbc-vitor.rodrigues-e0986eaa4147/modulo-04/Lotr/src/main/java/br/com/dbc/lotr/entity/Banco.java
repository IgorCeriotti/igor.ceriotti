/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author vitor.rodrigues
 */
@Entity
@SequenceGenerator(name = "BANCO_SEQ",sequenceName = "BANCO_SEQ")
public class Banco {
    @Id
    @GeneratedValue(generator = "BANCO_SEQ")
    
    private Integer id;
    private String nome;
    private Integer codigo;
    
}
