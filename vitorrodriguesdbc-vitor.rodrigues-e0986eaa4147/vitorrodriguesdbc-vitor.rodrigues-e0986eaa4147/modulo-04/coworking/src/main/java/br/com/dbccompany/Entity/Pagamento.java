package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id",
        scope = Pagamento.class)
@Table( name = "PAGAMENTOS" )
public class Pagamento {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "PAGAMENTOS_SEQ",
            sequenceName = "PAGAMENTOS_SEQ")
    @GeneratedValue(generator = "PAGAMENTOS_SEQ",
            strategy = GenerationType.SEQUENCE)
    private long id;

    @ManyToOne( cascade = CascadeType.MERGE )
    @JoinColumn( name = "id_Clientes_Pacotes" )
    private ClientePacote clientePacote;

    @ManyToOne (cascade = CascadeType.MERGE)
    @JoinColumn( name = "id_Contratacao" )
    private Contratacao contratacao;

    @Column( name = "TIPO_CONTRATACAO" )
    @Enumerated( EnumType.STRING )
    private TipoPagamento tipoPagamento;

    public ClientePacote getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientePacote clientePacote) {
        this.clientePacote = clientePacote;
    }

    public Contratacao getContratacao() {
        return contratacao;
    }

    public void setContratacao(Contratacao contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
