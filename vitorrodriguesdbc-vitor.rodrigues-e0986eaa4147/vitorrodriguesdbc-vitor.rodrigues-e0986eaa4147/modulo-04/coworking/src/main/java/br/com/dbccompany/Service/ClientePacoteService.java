package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.ClientePacote;
import br.com.dbccompany.Entity.Pacote;
import br.com.dbccompany.Repository.ClientePacoteRepository;
import br.com.dbccompany.Repository.ClienteRepository;
import br.com.dbccompany.Repository.PacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClientePacoteService {

    @Autowired
    ClientePacoteRepository clientePacoteRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    PacoteRepository pacoteRepository;

    @Transactional( rollbackFor = Exception.class )
    public ClientePacote salvar(ClientePacote clientePacote) {

        Cliente cliente = clienteRepository.findById(clientePacote.getCliente().getId()).get();
        clientePacote.setCliente(cliente);

        Pacote pacote = pacoteRepository.findById(clientePacote.getPacote().getId()).get();
        clientePacote.setPacote(pacote);

        return clientePacoteRepository.save(clientePacote);

    }

    @Transactional( rollbackFor = Exception.class )
    public ClientePacote editarCliente( long id, ClientePacote clientePacote ) {
        clientePacote.setId( id );
        return clientePacoteRepository.save( clientePacote );
    }
}
