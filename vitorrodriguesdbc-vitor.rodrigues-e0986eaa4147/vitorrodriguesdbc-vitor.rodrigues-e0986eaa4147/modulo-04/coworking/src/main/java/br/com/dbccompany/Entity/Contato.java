package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id",
        scope = Contato.class)
@Table( name = "CONTATO" )
public class Contato {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "CONTATO_SEQ",
            sequenceName = "CONTATO_SEQ")
    @GeneratedValue(generator = "CONTATO_SEQ",
            strategy = GenerationType.SEQUENCE)
    private long id;

    @Column( name = "VALOR" )
    private String valor;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn( name = "id_Tipo_contato" )
    private TipoContato tipoContato;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn( name = "id_Cliente" )
    private Cliente cliente;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public TipoContato getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContato tipoContato) {
        this.tipoContato = tipoContato;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
