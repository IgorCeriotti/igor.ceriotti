package br.com.dbccompany.Service;


import br.com.dbccompany.Entity.Pacote;
import br.com.dbccompany.Repository.PacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PacoteService {
    @Autowired
    PacoteRepository pacoteRepository;


    @Transactional( rollbackFor = Exception.class )
    public Pacote salvar(Pacote pacote) {

        return pacoteRepository.save(pacote);

    }
}
