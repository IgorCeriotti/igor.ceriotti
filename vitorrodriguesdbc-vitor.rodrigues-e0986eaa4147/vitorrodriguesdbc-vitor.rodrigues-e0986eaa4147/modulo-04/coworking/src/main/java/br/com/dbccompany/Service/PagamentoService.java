package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.*;
import br.com.dbccompany.Repository.ClientePacoteRepository;
import br.com.dbccompany.Repository.ContratacaoRepository;
import br.com.dbccompany.Repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    ClientePacoteRepository clientePacoteRepository;

    @Autowired
    ContratacaoRepository contratacaoRepository;

    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    SaldoClienteService saldoClienteService;

    @Autowired
    EspacoPacoteService espacoPacoteService;


    @Transactional(rollbackFor = Exception.class)
    public Pagamento salvar(Pagamento pagamento, boolean tipo) {

        if (tipo) {
            geraSaldoContratacao(pagamento);

            return pagamentoRepository.save(pagamento);
        } else {

            geraSaldoPacote(pagamento);
            return pagamentoRepository.save(pagamento);
        }
    }


    @Transactional(rollbackFor = Exception.class)
    public Pagamento editarPagamento(long id, Pagamento pagamento) {
        // SaldoCliente saldo =  pagamento.getClientePacote().getCliente().getSaldosClientes();
        pagamento.setId(id);
        return pagamentoRepository.save(pagamento);
    }

    @Transactional(rollbackFor = Exception.class)
    public Pagamento SaveOrUpdate(Pagamento pagamento) {

        /*if (pagamento.getClientePacote()!= null) {
            return editarPagamento(pagamento.getId(), pagamento);
        } else if (pagamento.getContratacao()!= null) {
            return editarPagamento(pagamento.getId(), pagamento);} else*/
         if (pagamento.getClientePacote() == null && pagamento.getContratacao() != null) {
             Contratacao contratacao = contratacaoRepository.findById(pagamento.getContratacao().getId()).get();
             pagamento.setContratacao(contratacao);
            return salvar(pagamento, true);
        } else {
             ClientePacote clientePacote = clientePacoteRepository.findById(pagamento.getClientePacote().getId()).get();
             pagamento.setClientePacote(clientePacote);
             return salvar(pagamento, false);
        }
    }

    public List<Pagamento> allPagamentos() {
        return (List<Pagamento>) pagamentoRepository.findAll();
    }

    public Optional<Pagamento> buscarPagamento(long id) {
        return pagamentoRepository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public void geraSaldoPacote(Pagamento pagamento) {
        ClientePacote clientePacote = pagamento.getClientePacote();

        List<EspacoPacote> listaEspacosPacotes = clientePacote.getPacote().getEspacosPacotes();
        for (int i = 0; i < listaEspacosPacotes.size(); i++) {
            SaldoCliente saldo = new SaldoCliente();
            saldo.setTipoContratacao(listaEspacosPacotes.get(i).getTipoContratacao());
            saldo.setQuantidade(listaEspacosPacotes.get(i).getQuantidade());

            SaldoClienteId id = new SaldoClienteId();

            id.setCliente(pagamento.getClientePacote().getCliente());
            id.setEspaco(pagamento.getClientePacote().getPacote().getEspacosPacotes().get(i).getEspaco());


            long result = listaEspacosPacotes.get(i).getPrazo();
            LocalDateTime now = LocalDateTime.now();
            LocalDateTime vencimento = now.plus(result, ChronoUnit.DAYS);


            saldo.setVencimento(vencimento);
            saldo.setTipoContratacao(pagamento.getClientePacote().getPacote().getEspacosPacotes().get(i).getTipoContratacao());
            saldo.setQuantidade(pagamento.getClientePacote().getPacote().getEspacosPacotes().get(i).getQuantidade());
            saldo.setId(id);

            listaEspacosPacotes.get(i).getEspaco().getSaldosClientes().add(saldo);
            clientePacote.getCliente().pushSaldosClientes(saldo);

            saldoClienteService.salvar(saldo);
            espacoPacoteService.salvar(listaEspacosPacotes.get(i));
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void geraSaldoContratacao(Pagamento pagamento){
        Contratacao contratacao = pagamento.getContratacao();
        SaldoCliente saldoCliente = new SaldoCliente();
        SaldoClienteId id = new SaldoClienteId();

        id.setCliente(pagamento.getContratacao().getCliente());
        id.setEspaco(pagamento.getContratacao().getEspaco());

        long result = pagamento.getContratacao().getPrazo();
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime vencimento = now.plus(result, ChronoUnit.DAYS);

        saldoCliente.setQuantidade(contratacao.getQuantidade());
        saldoCliente.setTipoContratacao(contratacao.getTipoContratacao());
        saldoCliente.setId(id);
        saldoCliente.setVencimento(vencimento);
        saldoCliente.setTipoContratacao(pagamento.getContratacao().getTipoContratacao());

        saldoClienteService.salvar(saldoCliente);
    }
}