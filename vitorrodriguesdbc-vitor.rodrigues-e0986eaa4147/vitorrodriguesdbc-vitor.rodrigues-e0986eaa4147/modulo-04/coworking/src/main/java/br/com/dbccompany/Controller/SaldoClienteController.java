package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.SaldoCliente;
import br.com.dbccompany.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/saldoCliente")
public class SaldoClienteController {
    @Autowired
    SaldoClienteService saldoClienteService;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<SaldoCliente> listSaldoClientes(){
        return saldoClienteService.allSaldoClientes();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Optional<SaldoCliente> SaldoClienteEspecifico(@PathVariable long id) {
        return saldoClienteService.buscarSaldoCliente(id);
    }
}
