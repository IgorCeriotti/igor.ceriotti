package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id",
        scope = SaldoCliente.class)
@Table( name = "SALDO_CLIENTE" )
public class SaldoCliente implements Serializable {

    @EmbeddedId
    private SaldoClienteId id;

    @OneToMany( mappedBy = "clientesSaldoCliente")
    private List<Acessos> acessos = new ArrayList<>();

    @Column( name = "TIPO_CONTRATACAO" )
    @Enumerated( EnumType.STRING )
    private TipoContratacao tipoContratacao;

    @Column( name = "QUANTIDADE", nullable = false )
    private long quantidade;

    @Column( name = "VENCIMENTO", nullable = false )
    private LocalDateTime vencimento;

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(long quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDateTime getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDateTime vencimento) {
        this.vencimento = vencimento;
    }

    public List<Acessos> getAcessos() {
        return acessos;
    }

    public void pushAcessos(Acessos... acessos) {
        this.acessos.addAll(Arrays.asList(acessos));
    }

}
