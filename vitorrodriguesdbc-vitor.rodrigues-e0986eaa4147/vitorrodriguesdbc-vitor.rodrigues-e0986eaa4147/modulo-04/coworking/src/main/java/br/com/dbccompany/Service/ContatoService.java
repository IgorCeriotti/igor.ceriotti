package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Repository.ClienteRepository;
import br.com.dbccompany.Repository.ContatoRepository;
import br.com.dbccompany.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ContatoService {

    @Autowired
    ContatoRepository contatoRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    TipoContatoRepository tipoContatoRepository;

    @Transactional( rollbackFor = Exception.class )
    public Contato salvar(Contato contato) {

        TipoContato tipoContato = tipoContatoRepository.findById(contato.getTipoContato().getId()).get();
        contato.setTipoContato(tipoContato);

        Cliente cliente = clienteRepository.findById(contato.getCliente().getId()).get();
        contato.setCliente(cliente);

        return contatoRepository.save(contato);

    }
}
