package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Acessos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AcessosRepository extends CrudRepository<Acessos, Long> {


}
