package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id",
        scope = Pacote.class)
@Table( name = "PACOTES" )
public class Pacote {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "PACOTES_SEQ",
            sequenceName = "PACOTES_SEQ")
    @GeneratedValue(generator = "PACOTES_SEQ",
            strategy = GenerationType.SEQUENCE)
    private long id;

    @Column( name = "VALOR", nullable = false )
    private double valor;

    @OneToMany( mappedBy = "pacote" , cascade = CascadeType.MERGE)
    private List<EspacoPacote> espacosPacotes = new ArrayList<>();

    @OneToMany( mappedBy = "pacote", cascade = CascadeType.MERGE)
    private List<ClientePacote> pacotesClientes = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<EspacoPacote> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void pushEspacosPacotes(EspacoPacote... espacosPacotes) {
        this.espacosPacotes.addAll(Arrays.asList(espacosPacotes));
    }

    public List<ClientePacote> getPacotesClientes() {
        return pacotesClientes;
    }

    public void pushPacotesClientes(ClientePacote... pacotesClientes) {
        this.pacotesClientes.addAll(Arrays.asList(pacotesClientes));
    }
}
