package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContratacaoRepository extends CrudRepository<Contratacao, Long> {
    List<Contratacao> findByEspaco(Espaco espaco);
    List<Contratacao> findByCliente(Cliente cliente);
    Contratacao findByTipoContratacao(TipoContratacao tipoContratacao);
    Contratacao findByQuantidade(long quantidade);
    Contratacao findByDesconto(double desconto);
    Contratacao findByPrazo(long prazo);
}
