package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Usuario;
import br.com.dbccompany.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/usuario")
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Usuario> listUsuarios(){
        return usuarioService.allUsuarios();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Usuario novoUsuario(@RequestBody Usuario usuario ) {

        return usuarioService.salvar(usuario);
    }
}
