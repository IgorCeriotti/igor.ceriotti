package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("api/tipoContato")
public class TipoContatoController {

    @Autowired
    TipoContatoService tipoContatoService;

    @PostMapping( value = "/novo" )
    @ResponseBody
    public TipoContato novo(@RequestBody TipoContato tipoContato ) {
        return tipoContatoService.salvar(tipoContato);
    }
}
