package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Repository.EspacoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EspacoService {
    @Autowired
    EspacoRepository espacoRepository;


    @Transactional( rollbackFor = Exception.class )
    public Espaco salvar(Espaco espaco) throws Exception{
        if(espacoRepository.findByNome(espaco.getNome())!=null){
            throw new Exception("Nome já existe");
        }
        if(espaco.getNome()==null){
            throw new Exception("Nome é obrigatorio");
        }
        if(espaco.getValor()<0){
            throw new Exception("Valor é obrigatorio");
        }
        if(espaco.getQtdPessoas()<0){
            throw new Exception("Quantidade de pessoas é obrigatorio");
        }
        return espacoRepository.save(espaco);
    }
}
