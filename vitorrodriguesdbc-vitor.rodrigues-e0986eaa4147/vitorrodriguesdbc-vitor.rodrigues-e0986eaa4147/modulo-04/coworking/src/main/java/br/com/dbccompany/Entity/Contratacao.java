package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id",
        scope = Contratacao.class)
@Table( name = "CONTRATACAO" )
public class Contratacao {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CONTRATACAO_SEQ",
            sequenceName = "CONTRATACAO_SEQ")
    @GeneratedValue(generator = "CONTRATACAO_SEQ",
            strategy = GenerationType.SEQUENCE)
    private long id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn( name = "id_Espacos" )
    private Espaco espaco;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn( name = "id_Clientes" )
    private Cliente cliente;

    @Column( name = "TIPO_CONTRATACAO" )
    @Enumerated( EnumType.STRING )
    private TipoContratacao tipoContratacao;

    @Column( name = "QUANTIDADE" )
    private long quantidade;

    @Column( name = "DESCONTO" )
    private double desconto;

    @Column( name = "PRAZO" )
    private long prazo;

    @OneToMany( mappedBy = "contratacao", cascade = CascadeType.MERGE )
    private List<Pagamento> pagamentos = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Espaco getEspaco() {
        return espaco;
    }

    public void setEspaco(Espaco espaco) {
        this.espaco = espaco;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(long quantidade) {
        this.quantidade = quantidade;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    public long getPrazo() {
        return prazo;
    }

    public void setPrazo(long prazo) {
        this.prazo = prazo;
    }

    public List<Pagamento> getPagamentos() {
        return pagamentos;
    }

    public void pushPagamentos(Pagamento... pagamentos) {
        this.pagamentos.addAll(Arrays.asList(pagamentos));
    }
}
