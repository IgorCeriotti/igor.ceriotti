package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.*;
import br.com.dbccompany.Repository.AcessosRepository;
import br.com.dbccompany.Repository.ClienteRepository;
import br.com.dbccompany.Repository.EspacoRepository;
import br.com.dbccompany.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class AcessosService {

    @Autowired
    private AcessosRepository acessosRepository;

    @Autowired
    SaldoClienteRepository saldoClienteRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    EspacoRepository espacoRepository;

    @Transactional( rollbackFor = Exception.class )
    public String salvar( Acessos acessos ) {

        SaldoCliente saldoCliente = saldoClienteRepository.findById(new SaldoClienteId(acessos.getClientesSaldoCliente().getId().getCliente(), acessos.getClientesSaldoCliente().getId().getEspaco()));

        acessos.setClientesSaldoCliente(saldoCliente);


        LocalDateTime now = LocalDateTime.now();


        if(acessos.getIsEntrada()) {
            if (acessos.getClientesSaldoCliente() == null) {
                return ("Poxa, não acredito que você não possui saldo :(");

            } else if (acessos.getClientesSaldoCliente().getQuantidade() > 0) {
                if (acessos.getData() == null) {
                    acessos.setData(now);
                }
                return String.format("Você tem %d %s(s)", acessos.getClientesSaldoCliente().getQuantidade(), acessos.getClientesSaldoCliente().getTipoContratacao());
            } else if (acessos.getClientesSaldoCliente().getQuantidade() == 0) {
                return ("Saldo insuficiente! Contratar novo pacote.");
            } else if (acessos.getClientesSaldoCliente().getQuantidade() > 0 && now.isAfter(acessos.getClientesSaldoCliente().getVencimento())) {
                acessos.getClientesSaldoCliente().setQuantidade(0);
                saldoClienteRepository.save(acessos.getClientesSaldoCliente());
                return "Saldo Vencido";
            }else{return "";}
        }

            else{
                LocalDateTime data = acessos.getData();
                long quantidadeSaldo = acessos.getClientesSaldoCliente().getQuantidade();
                long tempoNoEspaco = 0;
                switch (acessos.getClientesSaldoCliente().getTipoContratacao()) {
                    case MINUTO:
                        tempoNoEspaco = Math.round(now.getMinute() - data.getMinute());
                        break;
                    case HORA:
                        tempoNoEspaco = Math.round(now.getHour() - data.getHour());
                        break;
                    case TURNO:
                        tempoNoEspaco = ((now.getHour() - data.getHour()) / 5);
                        break;
                    case DIARIA:
                        tempoNoEspaco = Math.round(now.getDayOfMonth() - data.getDayOfMonth());
                        break;
                    case SEMANA:
                        tempoNoEspaco = Math.round((now.getDayOfMonth() - data.getDayOfMonth()) / 7);
                        break;
                    case MES:
                        tempoNoEspaco = Math.round(now.getMonthValue() - data.getMonthValue());
                        break;
                }
                acessos.getClientesSaldoCliente().setQuantidade(quantidadeSaldo - tempoNoEspaco);
                acessosRepository.save(acessos);
                return "Saida";
            }
    }

    @Transactional( rollbackFor = Exception.class )
    public Acessos editarAcesso(long id, Acessos acessos ) {
        acessos.setId(id);
        return acessosRepository.save( acessos );
    }

    public List<Acessos> allAcessos() {
        return (List<Acessos>) acessosRepository.findAll();
    }

    public Optional<Acessos> buscarAcesso(long id ){
        return acessosRepository.findById(id);
    }
}
