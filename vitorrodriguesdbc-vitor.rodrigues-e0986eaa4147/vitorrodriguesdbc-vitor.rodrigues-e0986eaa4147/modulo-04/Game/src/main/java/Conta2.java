
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author vitor.rodrigues
 */
@Entity
@SequenceGenerator(allocationSize = 1, name = "CONTA2_SEQ", sequenceName = "CONTA2_SEQ")
public class Conta2 {

    @Id
    @GeneratedValue(generator = "CONTA2_SEQ", strategy = GenerationType.SEQUENCE)

    private Integer id;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "conta_produto",
            joinColumns = {
                @JoinColumn(name = "id_conta")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_produto")})
    private List<Produto> produtos = new ArrayList();
}
