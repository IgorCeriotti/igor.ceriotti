import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class DwarfTest
{
    @Test
    public void dwarfNasceCom110(){
        Dwarf umDwarf = new Dwarf("Legolas");
        assertEquals(110.0,umDwarf.getVida(),.00001);
    
    }
    
    @Test
    public void dwarfFicaCom100DeVidaQuandoPerdeVida(){
    
        Dwarf dwarf = new Dwarf ("umDwarf");
        dwarf.sofrerDano();
        assertEquals(100,dwarf.getVida(),.00001);
    }
    @Test
    public void dwarfFicaCom90DeVidaQuandoPerdeVidaDuasVezes(){
    
        Dwarf dwarf = new Dwarf ("umDwarf");
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        assertEquals(90,dwarf.getVida(),.00001);
    } 
    @Test
    public void dwarfFicaCom90DeVidaQuandoPerdeVidaDozeVezes(){
    
        Dwarf dwarf = new Dwarf ("umDwarf");
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        assertEquals(0,dwarf.getVida(),.00001);
    } 
    @Test
    public void ElfoNasceStatusRecemCriado(){
        Dwarf dwarf = new Dwarf("dwarf");
        assertEquals(Status.RECEM_CRIADO,dwarf.getStatus());
        }
    @Test
    public void dwarfFicaCom90DeVidaQuandoPerdeVidaDozeVezesDeveEstarMorto(){
    
        Dwarf dwarf = new Dwarf ("umDwarf");
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        assertEquals(0,dwarf.getVida(),.00001);
        assertEquals(Status.MORTO,dwarf.getStatus());
    } 
    @Test
    public void dwarfSofreDanoMudaStatus(){
    
        Dwarf dwarf = new Dwarf ("umDwarf");
        dwarf.sofrerDano();
        assertEquals(Status.SOFREU_DANO,dwarf.getStatus());
    } 
    
}
