import java.util.*;
public class Inventario
{
     ArrayList<Item>itens;
     ArrayList<Item>itensInvertidos;
     private TipoOrdenacao ord;
    public Inventario(){
        this.itens=new ArrayList<>();
       
    }
   
    public int getTamanho(Inventario invent){
    
        return itens.size();
    }
    
    public void adicionarItemAoInventario(Item item){
        itens.add(item);
    }

    public Item retornarItemComMaiorQuantidade(){
        Item itemMaiorQuant=null;
        for(int i=0; i<itens.size();i++){
            if (itens.get(i) != null && itens.get(i).getQuantidade()>itemMaiorQuant.getQuantidade()){
                itemMaiorQuant=itens.get(i);
            }
        
        }
        return itemMaiorQuant;
    
    }

    public String listarNomesDosItens(){
        String s= "";
        int cont=0;
        for(int i=0; i<itens.size();i++){
            if (itens.get(i) != null&& cont!=0){
           
                s= s+", "+ itens.get(i).getDescricao();
            }
            if (itens.get(i) != null&& cont==0){
           
                s=itens.get(i).getDescricao();
                cont++;
            }
        }
        return s;   
    }
    
    public Item obterItemNaPosicao(int posicao){
        if(posicao >= itens.size()){
            return null;
        }
        else{
            return itens.get(posicao);
        }
    }
    public Item obterItemNaPosicaoInvert(int posicao){
        
        return itensInvertidos.get(posicao);
   
    }
    public void excluirItem(int posicao){
        itens.remove(posicao);
    }
    
    public void gerarItensIniciaisDeElfos(){
        Item flecha = new Item (2,"flechas");
        adicionarItemAoInventario(flecha);
        Item arco = new Item(1,"arco");
        adicionarItemAoInventario(arco);
    }
    
    public Item buscar(String descri){
        for(Item itemAtual: this.itens){
         if(itemAtual.getDescricao().equals(descri))
         return itemAtual;
        }
        
        return null;
        
    }
    
    public ArrayList inverterArray(){
        
        new ArrayList<>();
        for(int i= itens.size()-1;i>=0;i--){
            itensInvertidos.add(itens.get(i));
        
        }
        return itensInvertidos;
    
    }

    public void ordenarItens(){
        int maiorQuant= retornarItemComMaiorQuantidade().getQuantidade();
        
        ArrayList<Item>itensBackup =new ArrayList<>();
        for(int i=0; i<itens.size();i++){
            if(obterItemNaPosicao(i).getQuantidade()==maiorQuant){
            
                itensBackup.add(obterItemNaPosicao(i));
                excluirItem(i);
           }
        }
        for(int j=0; j<itens.size();j++){
            adicionarItemAoInventario(obterItemNaPosicao(j));
        
        }        
    }
    public void ordenar(TipoOrdenacao orden){
        if(orden==TipoOrdenacao.ASC){ordenarItens();
        }
        if(orden==TipoOrdenacao.DESC){inverterArray();   
        }
    }
    public String estatisticasInventario(){
        double media=0;
        int quantItensAcimaDaMedia=0;
        double mediana=0;
        for(int i=0; i<itens.size();i++){
            media += obterItemNaPosicao(i).getQuantidade();
        }        
        for(int i=0; i<itens.size();i++){
            if(media/itens.size()<obterItemNaPosicao(i).getQuantidade()){
                quantItensAcimaDaMedia++;
            }
        
        }
        for(int i=0; i<itens.size();i++){
            inverterArray();
            ordenarItens();
            if(itens.size()%2!=0&&obterItemNaPosicao(i)==obterItemNaPosicaoInvert(i)){
                mediana=obterItemNaPosicao(i).getQuantidade();
            }
            if(itens.size()%2==0){
                mediana =( obterItemNaPosicao(i/2).getQuantidade() + obterItemNaPosicao((i/2)-1).getQuantidade())/2;
            
            }
        }        
        String s;
        s= "Media: "+media+" Mediana: "+mediana+" Quantidade de valores acima da media: "+quantItensAcimaDaMedia+".";
        return s;
        
    }
    public String paginacaoItens(int pag, int lim){
        StringBuilder descricoes = new StringBuilder();
        String descricao = "";
        for(int i=pag; i< lim+i && i < itens.size();i++){
            
            descricao = descricao + obterItemNaPosicao(i).getDescricao();
            
        }
        return descricao;
        
    }
}
