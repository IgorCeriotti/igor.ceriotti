
public class Dwarf
{
   private String nome;
   private double vida=110.0;
   private Status status;
    
    public  Dwarf(String nome){
    
       this.setNome(nome);
       status = Status.RECEM_CRIADO;
    }
    
    public String getNome(){return nome;}
    public double getVida(){return vida;}
    public void setNome(String nome){this.nome=nome;}
    public Status getStatus(){return status;}
    
    
    
    private boolean podeSofrerDano()
    {
        return vida>0;
    }
    
    public void sofrerDano(){
       if(podeSofrerDano()){
            vida= vida-10;
            status= Status.SOFREU_DANO;
       }else{
            status = Status.MORTO;
       }
    }
    
    
}
