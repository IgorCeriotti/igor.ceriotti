import java.util.*;

public class ComparadorDePerformance {
    public void comparar() {
        // popular arraylist e hashmap
        ArrayList<Elfo> arrayElfos = new ArrayList<>();
        HashMap<String, Elfo> hashMapElfos = new HashMap<>();
        int qtdElfos = 1500000;
        for (int i = 0; i < qtdElfos; i++) {
            String nome = "Elfo " + i;
            Elfo elfo = new Elfo(nome);
            arrayElfos.add(elfo);
            hashMapElfos.put(nome, elfo);
        }

        String nomeBusca = "Elfo 1000000";

        long mSeqInicio = System.currentTimeMillis();
        Elfo elfoSeq = buscarSequencial(arrayElfos, nomeBusca);
        long mSeqFim = System.currentTimeMillis();

        long mMapInicio = System.currentTimeMillis();
        Elfo elfoMap = buscarMapa(hashMapElfos, nomeBusca);
        long mMapFim = System.currentTimeMillis();

        String tempoSeq = String.format("%.10f", (mSeqFim - mSeqInicio) / 1000.0 );
        String tempoMap = String.format("%.10f", (mMapFim - mMapInicio) / 1000.0 );

        System.out.println("ArrayList: " + tempoSeq);
        System.out.println("HashMap: " + tempoMap);
        
        
        
        
    }
    private Elfo buscarSequencial(ArrayList<Elfo> lista, String nome) {
        for (Elfo elfo : lista) {
            if (elfo.getNome().equals(nome)) {
                return elfo;
            }
        }
        return null;
    }

    private Elfo buscarMapa(HashMap<String, Elfo> mapa, String nome) {
        return mapa.get(nome);
    }
}
