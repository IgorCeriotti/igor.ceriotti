import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest {
    @Test
    public void calcularMediaInventarioVazio() {
        Inventario inventario = new Inventario(1);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertTrue(Double.isNaN(resultado));
    }

    @Test
    public void calcularMediaComUmItem() {
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(2, "Escudo de madeira"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertEquals(2, resultado, 1e-8); 
    }

    @Test
    public void calcularMediaQtdIguais() {
        Inventario inventario = new Inventario(2);
        inventario.adicionar(new Item(2, "Escudo de madeira"));
        inventario.adicionar(new Item(2, "Flecha de gelo"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertEquals(2, resultado, 1e-8); 
    }

    @Test
    public void calcularMediaQtdsDiferentes() {
        Inventario inventario = new Inventario(2);
        inventario.adicionar(new Item(4, "Escudo de madeira"));
        inventario.adicionar(new Item(4, "Flecha de gelo"));
        inventario.adicionar(new Item(1, "Botas"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertEquals(3, resultado, 1e-8); 
    }

    @Test
    public void calcularMedianaComInventarioVazio() {
        Inventario inventario = new Inventario(1);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertTrue(Double.isNaN(resultado));
    }

    @Test
    public void calcularMedianaComApenasUmItem() {
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(5, "Lembas"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals(5, resultado, 1e-8);
    }

    @Test
    public void calcularMedianaQtdImpar() {
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(5, "Lembas"));
        inventario.adicionar(new Item(10, "Adaga"));
        inventario.adicionar(new Item(20, "Poção MP"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals(10, resultado, 1e-8);
    }

    @Test
    public void calcularMedianaQtdPar() {
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(5, "Lembas"));
        inventario.adicionar(new Item(10, "Adaga"));
        inventario.adicionar(new Item(20, "Poção MP"));
        inventario.adicionar(new Item(30, "Poção MP"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals(15, resultado, 1e-8);
    }

    @Test
    public void qtdItensAcimaDaMediaInventarioVazio() {
        Inventario inventario = new Inventario(1);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(0, estatisticas.qtdItensAcimaDaMedia());
    }

    @Test
    public void qtdItensAcimaDaMediaComApenasUmItem() {
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(5, "Lembas"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        int resultado = estatisticas.qtdItensAcimaDaMedia();
        assertEquals(0, resultado);
    }

    @Test
    public void qtdItensAcimaDaMediaVariosItens() {
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(5, "Lembas"));
        inventario.adicionar(new Item(10, "Adaga"));
        inventario.adicionar(new Item(20, "Poção MP"));
        inventario.adicionar(new Item(30, "Poção MP"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        // media == 16.25
        assertEquals(2, estatisticas.qtdItensAcimaDaMedia());
    }

    @Test
    public void qtdItensAcimaDaMediaComItemIgualAMedia() {
        Inventario inventario = new Inventario(0);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        inventario.adicionar(new Item(1, "Escudo"));
        inventario.adicionar(new Item(2, "Escudo"));
        inventario.adicionar(new Item(3, "Escudo"));
        inventario.adicionar(new Item(4, "Escudo"));
        inventario.adicionar(new Item(5, "Escudo"));
        assertEquals(2, estatisticas.qtdItensAcimaDaMedia());
    }
}