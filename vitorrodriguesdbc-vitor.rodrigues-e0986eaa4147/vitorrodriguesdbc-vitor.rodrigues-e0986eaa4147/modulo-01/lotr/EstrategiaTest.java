import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class EstrategiaTest
{
    @Test
    public void listaElfosVivos() {
        Elfo elfo1 = new ElfoVerde("Galadriel");
        Elfo elfo2 = new ElfoNoturno("Galadriel");
        Estrategia estrategia = new Estrategia();
        ArrayList<Elfo> atacantes = new ArrayList<>();
        atacantes.add(elfo1);
        atacantes.add(elfo2);
        ArrayList<Elfo> resposta =estrategia.getOrdemDeAtaque(atacantes);
        assertEquals(elfo1,resposta.get(0));
    }
    
    @Test
    public void listaElfosVivosNaPosicao() {
        Elfo elfo1 = new ElfoNoturno ("Galadriel");
        Elfo elfo2 = new ElfoVerde("Galadriel");
        Elfo elfo3 = new ElfoNoturno ("Galadriel");
        Elfo elfo4 = new ElfoVerde("Galadriel");
        Elfo elfo5 = new ElfoNoturno ("Galadriel");
        Elfo elfo6 = new ElfoNoturno ("Galadriel");
        Elfo elfo7 = new ElfoVerde("Galadriel");
        Estrategia estrategia = new Estrategia();
        ArrayList<Elfo> atacantes = new ArrayList<>();
        atacantes.add(elfo1);
        atacantes.add(elfo2);
        atacantes.add(elfo3);
        atacantes.add(elfo4);
        atacantes.add(elfo5);
        atacantes.add(elfo6);
        atacantes.add(elfo7);
        ArrayList<Elfo> resposta =estrategia.getOrdemDeAtaque(atacantes);
        assertEquals(elfo2,resposta.get(0));
        assertEquals(elfo4,resposta.get(1));
        assertEquals(elfo7,resposta.get(2)); 
    }
    
    @Test
    public void listaElfosVivosNaPosicaoAlternadaElfoNoturno() {
        Elfo elfo1 = new ElfoNoturno ("Galadriel");
        Elfo elfo2 = new ElfoVerde("Galadriel");
        Elfo elfo3 = new ElfoVerde ("Galadriel");
        Elfo elfo4 = new ElfoNoturno("Galadriel");
        Elfo elfo5 = new ElfoNoturno ("Galadriel");
        Elfo elfo6 = new ElfoVerde("Galadriel");
        Estrategia estrategia = new Estrategia();
        ArrayList<Elfo> atacantes = new ArrayList<>();
        atacantes.add(elfo1);
        atacantes.add(elfo2);
        atacantes.add(elfo3);
        atacantes.add(elfo4);
        atacantes.add(elfo5);
        atacantes.add(elfo6);
        ArrayList<Elfo> resposta = estrategia.getOrdemAlternadaDeAtaque(atacantes);
        assertEquals(elfo1,resposta.get(0));
        assertEquals(elfo2,resposta.get(1));
        assertEquals(elfo4,resposta.get(2));
    }
    
    @Test
    public void listaElfosVivosNaPosicaoAlternadaIniciaElfoVerde() {
        Elfo elfo1 = new ElfoNoturno ("Galadriel");
        Elfo elfo2 = new ElfoVerde("Galadriel");
        Elfo elfo3 = new ElfoVerde ("Galadriel");
        Elfo elfo4 = new ElfoNoturno("Galadriel");
        Elfo elfo5 = new ElfoNoturno ("Galadriel");
        Elfo elfo6 = new ElfoVerde("Galadriel");
        Estrategia estrategia = new Estrategia();
        ArrayList<Elfo> atacantes = new ArrayList<>();
        atacantes.add(elfo1);
        atacantes.add(elfo2);
        atacantes.add(elfo3);
        atacantes.add(elfo4);
        atacantes.add(elfo5);
        atacantes.add(elfo6);
        ArrayList<Elfo> resposta = estrategia.getOrdemAlternadaDeAtaque(atacantes);
        assertEquals(elfo1,resposta.get(0));
        assertEquals(elfo1,resposta.get(1));
        assertEquals(elfo3,resposta.get(2));
    }
}
