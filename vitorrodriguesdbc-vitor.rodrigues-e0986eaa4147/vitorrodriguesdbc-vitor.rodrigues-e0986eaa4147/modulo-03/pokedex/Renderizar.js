class Renderizar { // eslint-disable-line no-unused-vars
  gerarRandom() {
    const min = 1
    const max = 802
    return Math.floor( Math.random() * ( +max - +min ) ) + +min;
  }

  renderizarPokemonNaTela( pokemon ) {

    function retornaElementId( nome ) {
      return document.getElementById( nome )
    }

    const dadosPokemon = retornaElementId( 'dadosPokemon' )

    function retornaQuery( nome ) {
      return dadosPokemon.querySelector( nome )
    }

    const nome = retornaQuery( '.nome' )
    nome.innerText = pokemon.nome.toUpperCase()

    const imgPokemon = retornaQuery( '.thumb' )
    imgPokemon.src = pokemon.thumbUrl

    const heightPokemon = retornaQuery( '.height' )
    heightPokemon.innerText = pokemon.alturaCm

    const weightPokemon = retornaQuery( '.weight' )
    weightPokemon.innerText = 'Peso: ' + pokemon.weight + 'kg'

    function jsUcfirst( string )
    {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    const listTipos = retornaElementId( 'tipos' )
    listTipos.innerHTML = ''
    const arraySoComTiposString = pokemon.tipos
    for (let i = 0; i < arraySoComTiposString.length; i += 1 ) {
      const tipoLI = document.createElement( 'LI' )
      const conteudoLinha = document.createTextNode( jsUcfirst(arraySoComTiposString[i]) )
      tipoLI.appendChild( conteudoLinha )
      listTipos.appendChild( tipoLI )
    }


    const listStats = retornaElementId( 'stats' )
    listStats.innerHTML = ''
    const arraySoComStatsString = pokemon.stats
    for (let i = 0; i < arraySoComStatsString.length; i += 2 ) {
      const statLI = document.createElement( 'LI' )
      const conteudoLinha = document.createTextNode( jsUcfirst(arraySoComStatsString[i]) + ':' + ' ' + arraySoComStatsString[i + 1] + "%" )
      statLI.appendChild( conteudoLinha )
      listStats.appendChild( statLI )
    }
  }
}
