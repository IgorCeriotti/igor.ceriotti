import '../utils/number-prototypes'
import EpisodiosApi from './episodiosApi'

export default class DetalheEpisodio{
    constructor( { id,episodioId,notaImdb,sinopse,dataEstreia } ){
        this.id=id
        this.episodioId=episodioId
        this.notaImdb=notaImdb
        this.sinopse=sinopse
        this.dataEstreia=dataEstreia
        this.episodiosApi = new EpisodiosApi()
    }
}