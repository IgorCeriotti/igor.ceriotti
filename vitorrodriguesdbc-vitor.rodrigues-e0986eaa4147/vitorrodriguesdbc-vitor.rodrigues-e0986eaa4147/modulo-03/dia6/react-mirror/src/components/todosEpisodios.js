import React from 'react'
import './todosEpisodios.css'
import ListaEpisodiosUi from './shared/listaEpisodiosUi'
import MeuBotao from './shared/meuBotao'

const TodosEpisodios = props => {
  const { listaEpisodios } = props.location.state
  return (
    <React.Fragment>
      <ListaEpisodiosUi listaEpisodios={ listaEpisodios.todos } />
      <MeuBotao cor="azul"  texto="Ordenar"/>
    </React.Fragment>
  )
}
export default TodosEpisodios