package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id",
        scope = EspacoPacote.class)
@Table( name = "ESPACOS_PACOTES" )
public class EspacoPacote {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESPACOS_PACOTES_SEQ",
            sequenceName = "ESPACOS_PACOTES_SEQ")
    @GeneratedValue(generator = "ESPACOS_PACOTES_SEQ",
            strategy = GenerationType.SEQUENCE)
    private long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn( name = "id_Espacos" )
    private Espaco espaco;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn( name = "id_Pacotes" )
    private Pacote pacote;

    @Column( name = "TIPO_CONTRATACAO" )
    @Enumerated( EnumType.STRING )
    private TipoContratacao tipoContratacao;

    @Column( name = "QUANTIDADE" )
    private long quantidade;

    @Column( name = "PRAZO" )
    private long prazo;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Espaco getEspaco() {
        return espaco;
    }

    public void setEspaco(Espaco espaco) {
        this.espaco = espaco;
    }

    public Pacote getPacote() {
        return pacote;
    }

    public void setPacote(Pacote pacote) {
        this.pacote = pacote;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(long quantidade) {
        this.quantidade = quantidade;
    }

    public long getPrazo() {
        return prazo;
    }

    public void setPrazo(long prazo) {
        this.prazo = prazo;
    }
}
