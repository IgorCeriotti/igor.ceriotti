package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Acessos;
import br.com.dbccompany.Entity.SaldoCliente;
import br.com.dbccompany.Entity.TipoContratacao;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;

public interface SaldoClienteRepository extends CrudRepository<SaldoCliente, Long> {
    SaldoCliente findByAcessos(Acessos acessos);
    SaldoCliente findByTipoContratacao(TipoContratacao tipoContratacao);
    SaldoCliente findByQuantidade(long quantidade);
    SaldoCliente findByVencimento(Date vencimento);
}
