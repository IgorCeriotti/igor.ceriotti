package br.com.dbccompany.Entity;

public enum TipoPagamento {

    Débito, Crédito, Dinheiro, Transferencia;

}
