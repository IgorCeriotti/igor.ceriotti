package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id",
        scope = Cliente.class)
@Table( name = "CLIENTES" )
public class Cliente {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTES_SEQ",
            sequenceName = "CLIENTES_SEQ")
    @GeneratedValue(generator = "CLIENTES_SEQ",
            strategy = GenerationType.SEQUENCE)
    private long id;

    @Column( name = "NOME", nullable = false )
    private String nome;

    @Column( name = "CPF", nullable = false, unique = true )
    private String cpf;

    @Column( name = "DATA_NASCIMENTO", nullable = false )
    private Date dataNascimento;

    @OneToMany( mappedBy = "cliente", cascade = CascadeType.ALL )
    private List<Contato> contatos = new ArrayList<>();

    @OneToMany( mappedBy = "cliente" , cascade = CascadeType.ALL)
    private List<ClientePacote> clientePacotes = new ArrayList<>();

    @OneToMany( mappedBy = "cliente" , cascade = CascadeType.ALL)
    private List<Contratacao> contratacoes = new ArrayList<>();

    @OneToMany(mappedBy = "id.cliente", cascade = CascadeType.ALL)
    private List<SaldoCliente> saldosClientes = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<Contato> getContatos() {
        return contatos;
    }

    public void pushContatos(Contato... contatos) {
        this.contatos.addAll(Arrays.asList(contatos));
    }

    public List<ClientePacote> getClientePacotes() {
        return clientePacotes;
    }

    public void pushClientePacotes(ClientePacote... clientePacotes) {
        this.clientePacotes.addAll(Arrays.asList(clientePacotes));
    }

    public List<Contratacao> getContratacoes() {
        return contratacoes;
    }

    public void pushContratacoes(Contratacao... contratacoes) {
        this.contratacoes.addAll(Arrays.asList(contratacoes));
    }

    public List<SaldoCliente> getSaldosClientes() {
        return saldosClientes;
    }

    public void pushSaldosClientes(SaldoCliente... saldosClientes) {
        this.saldosClientes.addAll(Arrays.asList(saldosClientes));
    }
}
