package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id",
        scope = Acessos.class)
public class Acessos {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ACESSOS_SEQ",
            sequenceName = "ACESSOS_SEQ")
    @GeneratedValue(generator = "ACESSOS_SEQ",
            strategy = GenerationType.SEQUENCE)
    private long id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumns({
            @JoinColumn( name = "id_Clientes_Saldo_Cliente", referencedColumnName = "id_cliente" ),
            @JoinColumn( name = "id_Espacos_Saldo_Cliente", referencedColumnName = "id_espaco")
    })
    private SaldoCliente ClientesSaldoCliente;

    @Column( name = "is_Entrada", nullable = false )
    private boolean isEntrada;

    @Column( name = "is_Excecao" )
    private boolean isExcecao;

    @Column( name = "Data" )
    private LocalDateTime data;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean getIsEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public boolean getIsExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public SaldoCliente getClientesSaldoCliente() {
        return ClientesSaldoCliente;
    }

    public void setClientesSaldoCliente(SaldoCliente clientesSaldoCliente) {
        ClientesSaldoCliente = clientesSaldoCliente;
    }
}
