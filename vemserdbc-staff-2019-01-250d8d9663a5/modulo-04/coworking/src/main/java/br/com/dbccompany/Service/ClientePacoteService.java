package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.ClientePacote;
import br.com.dbccompany.Repository.ClientePacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClientePacoteService {

    @Autowired
    ClientePacoteRepository clientePacoteRepository;

    @Transactional( rollbackFor = Exception.class )
    public ClientePacote salvar(ClientePacote clientePacote) {


        return clientePacoteRepository.save(clientePacote);

    }

    @Transactional( rollbackFor = Exception.class )
    public ClientePacote editarCliente( long id, ClientePacote clientePacote ) {
        clientePacote.setId( id );
        return clientePacoteRepository.save( clientePacote );
    }
}
