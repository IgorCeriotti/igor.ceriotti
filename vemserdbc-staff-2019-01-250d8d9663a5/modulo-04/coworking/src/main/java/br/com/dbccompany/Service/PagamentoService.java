package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.*;
import br.com.dbccompany.Repository.ClienteRepository;
import br.com.dbccompany.Repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    SaldoClienteService saldoClienteService;

    @Autowired
    EspacoPacoteService espacoPacoteService;


    @Transactional( rollbackFor = Exception.class )
    public Pagamento salvar(Pagamento pagamento) {
        ClientePacote clientePacote = pagamento.getClientePacote();
        Contratacao contratacao = pagamento.getContratacao();
        if(clientePacote != null && contratacao !=null){
            return null;
        } else if (clientePacote==null && contratacao!=null ){
            SaldoCliente saldoCliente = new SaldoCliente();
            saldoCliente.setQuantidade(contratacao.getQuantidade());
            saldoCliente.setTipoContratacao(contratacao.getTipoContratacao());
            saldoClienteService.salvar(saldoCliente);
            return pagamentoRepository.save(pagamento);
        } else if(clientePacote!=null && contratacao==null){
            List<EspacoPacote> listaEspacosPacotes = clientePacote.getPacote().getEspacosPacotes();
            for(int i= 0; i<listaEspacosPacotes.size(); i++){
                //TODO update caso ja exista :(
                SaldoCliente saldo = new SaldoCliente();
                saldo.setTipoContratacao(listaEspacosPacotes.get(i).getTipoContratacao());
                saldo.setQuantidade(listaEspacosPacotes.get(i).getQuantidade());

                long result = listaEspacosPacotes.get(i).getPrazo();
                LocalDateTime now = LocalDateTime.now();
                LocalDateTime vencimento = now.plus(result, ChronoUnit.DAYS);
                saldo.setVencimento(vencimento);

                listaEspacosPacotes.get(i).getEspaco().getSaldosClientes().add(saldo);
                clientePacote.getCliente().pushSaldosClientes(saldo);

                saldoClienteService.salvar(saldo);
                espacoPacoteService.salvar(listaEspacosPacotes.get(i));

            }
            return  pagamentoRepository.save(pagamento);
        }
        return pagamentoRepository.save(pagamento);

    }

    @Transactional( rollbackFor = Exception.class )
    public Pagamento editarPagamento( long id, Pagamento pagamento ) {
       // SaldoCliente saldo =  pagamento.getClientePacote().getCliente().getSaldosClientes();
        pagamento.setId( id );
        return pagamentoRepository.save( pagamento );
    }

    @Transactional( rollbackFor =  Exception.class )
    public Pagamento SaveOrUpdate( Pagamento pagamento ){
        if( pagamento.getClientePacote().getCliente().getSaldosClientes() != null  ){
            return editarPagamento(pagamento.getId(), pagamento);
        } else if ( pagamento.getContratacao().getEspaco().getSaldosClientes() != null ){
           return editarPagamento(pagamento.getId(), pagamento);
        } else {
           return salvar( pagamento );
        }
    }

    public List<Pagamento> allPagamentos() {
        return (List<Pagamento>) pagamentoRepository.findAll();
    }

    public Optional<Pagamento> buscarPagamento(long id ){
        return pagamentoRepository.findById(id);
    }


}
