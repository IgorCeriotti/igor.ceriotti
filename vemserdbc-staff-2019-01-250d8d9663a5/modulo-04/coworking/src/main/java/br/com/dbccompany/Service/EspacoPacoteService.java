package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Entity.EspacoPacote;
import br.com.dbccompany.Repository.EspacoPacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EspacoPacoteService {

    @Autowired
    EspacoPacoteRepository espacoPacoteRepository;

    @Transactional(rollbackFor = Exception.class)
    public EspacoPacote salvar(EspacoPacote espacoPacote ) {
        return espacoPacoteRepository.save(espacoPacote);
    }
}

