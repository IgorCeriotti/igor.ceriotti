package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Entity.EspacoPacote;
import br.com.dbccompany.Entity.Pacote;
import br.com.dbccompany.Entity.TipoContratacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EspacoPacoteRepository extends CrudRepository<EspacoPacote, Long> {
    List<EspacoPacote> findByEspaco(Espaco espaco);
    List<EspacoPacote> findByPacote(Pacote pacote);
    EspacoPacote findByTipoContratacao(TipoContratacao tipoContratacao);
    EspacoPacote findByQuantidade(long quantidade);
    EspacoPacote findByPrazo(long prazo);
}
