package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Repository.ContratacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContratacaoService {
    @Autowired
    ContratacaoRepository contratacaoRepository;

    @Transactional( rollbackFor = Exception.class )
    public String salvar(Contratacao contratacao ) {
        Espaco espaco = contratacao.getEspaco();
        long qtd = contratacao.getQuantidade();
        double valor = espaco.getValor();

        contratacaoRepository.save(contratacao);

        return String.format("R$: %f", qtd*valor);
    }

    @Transactional( rollbackFor = Exception.class )
    public Contratacao editarContratacao( long id, Contratacao contratacao ) {
        contratacao.setId(id);
        return contratacaoRepository.save( contratacao );
    }

    public List<Contratacao> allContratacoes() {
        return (List<Contratacao>) contratacaoRepository.findAll();
    }

    public Optional<Contratacao> buscarContratacao(long id ){
        return contratacaoRepository.findById(id);
    }
}
