package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.SaldoCliente;
import br.com.dbccompany.Entity.Usuario;
import br.com.dbccompany.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UsuarioService {
    @Autowired
    UsuarioRepository usuarioRepository;

    @Transactional
    public Usuario salvar(Usuario obj) {
        String senhaDescrip= obj.getSenha();
        obj.setSenha(new BCryptPasswordEncoder(6).encode(senhaDescrip));
        return usuarioRepository.save(obj);
    }

    public List<Usuario> allUsuarios() {
        return (List<Usuario>) usuarioRepository.findAll();
    }





}
