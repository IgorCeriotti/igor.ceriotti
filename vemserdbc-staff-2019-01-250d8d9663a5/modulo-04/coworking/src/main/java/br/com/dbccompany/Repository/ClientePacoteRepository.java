package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.ClientePacote;
import br.com.dbccompany.Entity.Pacote;
import br.com.dbccompany.Entity.Pagamento;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientePacoteRepository extends CrudRepository<ClientePacote, Long> {
    List<ClientePacote> findByCliente(Cliente cliente);
    List<ClientePacote> findByPacote(Pacote pacote);
    ClientePacote findByQuantidade(long quantidade);
    ClientePacote findByPagamentos(Pagamento pagamento);
}
