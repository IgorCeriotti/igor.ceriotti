package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/cliente")
public class ClienteController {
    @Autowired
    ClienteService clienteService;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Cliente> listClientes(){
        return clienteService.allClientes();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Cliente novoCliente(@RequestBody Cliente cliente) {

        return clienteService.salvar(cliente);
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Optional<Cliente> ClienteEspecifico(@PathVariable long id) {
        return clienteService.buscarCliente(id);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Cliente editarCliente( @PathVariable long id, @RequestBody Cliente cliente ) {
        return clienteService.editarCliente(id, cliente);
    }

}
