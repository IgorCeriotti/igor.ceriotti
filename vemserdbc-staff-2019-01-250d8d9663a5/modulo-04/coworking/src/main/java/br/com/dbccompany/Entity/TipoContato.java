package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id",
        scope = TipoContato.class)
@Table( name = "TIPO_CONTATO" )
public class TipoContato {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "TIPOCONT_SEQ",
            sequenceName = "TIPOCONT_SEQ")
    @GeneratedValue(generator = "TIPOCONT_SEQ",
            strategy = GenerationType.SEQUENCE)
    private long id;

    @Column( name = "NOME", nullable = false )
    private String nome;

    @OneToMany( mappedBy = "tipoContato" , cascade = CascadeType.MERGE)
    private List<Contato> contatos = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Contato> getContatos() {
        return contatos;
    }

    public void pushContatos(Contato... contatos) {
        this.contatos.addAll(Arrays.asList(contatos));
    }
}
