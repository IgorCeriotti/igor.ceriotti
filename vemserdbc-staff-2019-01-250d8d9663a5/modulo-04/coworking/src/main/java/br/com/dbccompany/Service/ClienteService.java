package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Repository.ClienteRepository;
import br.com.dbccompany.Repository.ContatoRepository;
import br.com.dbccompany.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {
    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    ContatoRepository contatoRepository;

    @Autowired
    TipoContatoRepository tipoContatoRepository;

    @Transactional( rollbackFor = Exception.class )
    public Cliente salvar( Cliente cliente ) {
        for ( int i = 0; i<cliente.getContatos().size(); i++ ) {
            Contato contato = contatoRepository.findById(cliente.getContatos().get(i).getId()).get();
            cliente.pushContatos(contato);
            /*if( contato.getTipoContato().getNome().equals("EMAIL") ){
                cliente.pushContatos(contato);
            } else if ( contato.getTipoContato().getNome().equals("TELEFONE") ){
                cliente.pushContatos(contato);
            }*/
        }


        /*TipoContato tipoEmail = new TipoContato();

        tipoEmail.setNome("EMAIL");
        tipoContatoRepository.save(tipoEmail);

        TipoContato tipoTelefone = new TipoContato();
        tipoTelefone.setNome("TELEFONE");

        email.setTipoContato(tipoEmail);
        telefone.setTipoContato(tipoTelefone);
        tipoContatoRepository.save(tipoTelefone);

        cliente.pushContatos(email);
        cliente.pushContatos(telefone);*/

        return clienteRepository.save( cliente );
    }

    @Transactional( rollbackFor = Exception.class )
    public Cliente editarCliente( long id, Cliente cliente ) {
        cliente.setId( id );
        return clienteRepository.save( cliente );
    }

    public List<Cliente> allClientes() {
        return (List<Cliente>) clienteRepository.findAll();
    }

    public Optional<Cliente> buscarCliente(long id ){
        return clienteRepository.findById(id);
    }

}
