package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Pagamento;
import br.com.dbccompany.Service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/pagamento")
public class PagamentoController {

    @Autowired
    PagamentoService pagamentoService;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Pagamento> listPagamentos(){
        return pagamentoService.allPagamentos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Pagamento novoPagamento( @RequestBody Pagamento pagamento ) {
        return pagamentoService.salvar(pagamento);
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Optional<Pagamento> PagamentoEspecifico(@PathVariable long id) {
        return pagamentoService.buscarPagamento(id);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Pagamento editarServicoContratado( @PathVariable long id, @RequestBody Pagamento pagamento ) {
        return pagamentoService.editarPagamento(id, pagamento);
    }
}
