package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Acessos;
import br.com.dbccompany.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/acessos")
public class AcessosController {

    @Autowired
    AcessosService acessosService;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Acessos> listAcessos(){
        return acessosService.allAcessos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public String novoAcesso( @RequestBody Acessos acesso ) {
        return acessosService.salvar(acesso);
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Optional<Acessos> AcessoEspecifico(@PathVariable long id) {
        return acessosService.buscarAcesso(id);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Acessos editarAcesso( @PathVariable long id, @RequestBody Acessos acesso ) {
        return acessosService.editarAcesso(id, acesso);
    }
}
