package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.EnderecoSeguradoras;
import br.com.dbccompany.Entity.Seguradora;

public interface EnderecoSeguradorasRepository extends CrudRepository<EnderecoSeguradoras, Long> {

	EnderecoSeguradoras findBySeguradora( Seguradora seguradora );

	
	
}
