package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@PrimaryKeyJoinColumn( name = "id_pessoas" )
public class Segurados extends Pessoas {
	
	@Column( name = "QTD_SERVICOS", nullable = false )
	private Integer qtdServicos;
	
	@OneToMany( mappedBy = "segurado" )
	private List<ServicoContratado> servicosContratados = new ArrayList<>(); 

	public Integer getQtdServicos() {
		return qtdServicos;
	}

	public void setQtdServicos(Integer qtdServicos) {
		this.qtdServicos = qtdServicos;
	}

	public List<ServicoContratado> getServicosContratados() {
		return servicosContratados;
	}

	public void pushServicosContratados(ServicoContratado... servicosContratados) {
		this.servicosContratados.addAll(Arrays.asList(servicosContratados));
	}
	
	
		
}
