package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Entity.Cidade;
import br.com.dbccompany.Entity.Enderecos;

public interface BairroRepository extends CrudRepository<Bairro, Long>{

	Bairro findByNome( String nome ); 
	List<Bairro> findByCidade( Cidade cidade ); 
	Bairro findByEnderecos( Enderecos endereco ); 
	
}
