package br.com.dbccompany.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Estado;
import br.com.dbccompany.Service.EstadoService;

@Controller
@RequestMapping("/api/estado")

public class EstadoController {

	@Autowired
	EstadoService estadoService; 
	
	@GetMapping( value = "/" )
	@ResponseBody
	public List<Estado> listEstados(){
		return estadoService.allEstados();
	}
	
	@PostMapping( value = "/novo" )
	@ResponseBody
	public Estado novoEstado( @RequestBody Estado estado ) {
		return estadoService.salvar(estado);
	}
	
	@GetMapping( value = "/{id}" )
	@ResponseBody
	public Optional<Estado> estadoEspecifico( @PathVariable long id) {
		return estadoService.buscarEstado(id);
	}
	
	@PutMapping( value = "/editar/{id}" )
	@ResponseBody
	public Estado editarEstado( @PathVariable long id, @RequestBody Estado estado ) {
		return estadoService.editarEstado(id, estado);
	}

	
}
