package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Entity.Cidade;
import br.com.dbccompany.Entity.Enderecos;
import br.com.dbccompany.Repository.BairroRepository;



@Service
public class BairroService {
	@Autowired
	private BairroRepository bairroRepository; 
	
	@Transactional( rollbackFor = Exception.class )
	public Bairro salvar( Bairro bairro ) {
		return bairroRepository.save( bairro ); 
	}
	
	@Transactional( rollbackFor = Exception.class )
	public void deletar( long Id ) {
		bairroRepository.deleteById(Id);
	}
	
	@Transactional( rollbackFor = Exception.class )
	public Bairro editarBairro( long Id, Bairro bairro ) {
		bairro.setId( Id );
		return bairroRepository.save( bairro );
	}
	
	public Optional<Bairro> buscarBairro( long id ){
		return bairroRepository.findById(id); 
	}
	
	public List<Bairro> allBairros() {
		return (List<Bairro>) bairroRepository.findAll();
	}
	
	public Bairro buscarPorNome( String nome ) {
		return bairroRepository.findByNome(nome);
	}
	
	public List<Bairro> buscarPorCidade( Cidade cidade ){
		return bairroRepository.findByCidade(cidade);
	}
	
	public Bairro buscarPorEndereco( Enderecos endereco ) {
		return bairroRepository.findByEnderecos(endereco);
	}
	
}
