package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Repository.CorretorRepository;

@Service
public class CorretorService {

	@Autowired
	private CorretorRepository corretorRepository; 
	
	@Transactional( rollbackFor = Exception.class )
	public Corretor salvar( Corretor corretor ) {
		return corretorRepository.save( corretor ); 
	}
	
	@Transactional( rollbackFor = Exception.class )
	public void deletar( long Id ) {
		corretorRepository.deleteById(Id);
	}
	
	@Transactional( rollbackFor = Exception.class )
	public Corretor editarCorretor( long Id, Corretor corretor ) {
		corretor.setId( Id );
		return corretorRepository.save( corretor );
	}
	
	public Optional<Corretor> buscarCorretor( long id ){
		return corretorRepository.findById(id); 
	}
	
	public List<Corretor> allCorretores() {
		return (List<Corretor>) corretorRepository.findAll();
	}
	
	public Corretor buscarPorCargo( String cargo ) {
		return corretorRepository.findByCargo(cargo);
	}
	
	public Corretor buscarPorComissao( long comissao ) {
		return corretorRepository.findByComissao(comissao);
	}
	
	public Corretor buscarPorServicoContratado( ServicoContratado servicoContratado ) {
		return corretorRepository.findByServicosContratados(servicoContratado);
	}

	
}
