package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Enderecos;
import br.com.dbccompany.Entity.Pessoas;

public interface PessoasRepository extends CrudRepository<Pessoas, Long> {

	Pessoas findByNome( String nome );
	Pessoas findByCpf( long cpf );
	Pessoas findByPai( String pai );
	Pessoas findByMae( String mae );
	Pessoas findByTelefone( long telefone );
	Pessoas findByEmail( String email ); 
	List<Pessoas> findByEnderecos( Enderecos endereco ); 
	
	
}
