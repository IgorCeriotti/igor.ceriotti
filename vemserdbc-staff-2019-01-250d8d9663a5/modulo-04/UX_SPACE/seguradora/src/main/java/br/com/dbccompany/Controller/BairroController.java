package br.com.dbccompany.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Service.BairroService;

@Controller
@RequestMapping("/api/bairro")

public class BairroController {
	@Autowired
	BairroService bairroContratadoService; 
	
	@GetMapping( value = "/" )
	@ResponseBody
	public List<Bairro> listBairros(){
		return bairroContratadoService.allBairros();
	}
	
	@PostMapping( value = "/novo" )
	@ResponseBody
	public Bairro novoBairro( @RequestBody Bairro bairro ) {
		return bairroContratadoService.salvar(bairro);
	}
	
	@GetMapping( value = "/{id}" )
	@ResponseBody
	public Optional<Bairro> bairroEspecifico( @PathVariable long id) {
		return bairroContratadoService.buscarBairro(id);
	}
	
	@PutMapping( value = "/editar/{id}" )
	@ResponseBody
	public Bairro editarBairro( @PathVariable long id, @RequestBody Bairro bairro ) {
		return bairroContratadoService.editarBairro(id, bairro);
	}
}
