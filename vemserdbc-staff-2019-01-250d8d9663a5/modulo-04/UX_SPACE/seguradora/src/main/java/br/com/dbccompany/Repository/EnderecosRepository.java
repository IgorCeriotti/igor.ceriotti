package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Entity.Enderecos;
import br.com.dbccompany.Entity.Pessoas;

public interface EnderecosRepository extends CrudRepository<Enderecos, Long>{

	Enderecos findByLogradouro( String logradouro );
	Enderecos findByNumero( long numero );
	Enderecos findByComplemento( String complemento );
	List<Enderecos> findByBairro( Bairro bairro );
	List<Enderecos> findByPessoas( Pessoas pessoa );
	
	
}
