package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.EnderecoSeguradoras;
import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Repository.EnderecoSeguradorasRepository;

@Service
public class EnderecoSeguradorasService {

	@Autowired
	private EnderecoSeguradorasRepository enderecoSeguradorasRepository; 
	
	@Transactional( rollbackFor = Exception.class )
	public EnderecoSeguradoras salvar( EnderecoSeguradoras enderecoSeguradoras ) {
		return enderecoSeguradorasRepository.save( enderecoSeguradoras ); 
	}
	
	@Transactional( rollbackFor = Exception.class )
	public void deletar( long Id ) {
		enderecoSeguradorasRepository.deleteById(Id);
	}
	
	@Transactional( rollbackFor = Exception.class )
	public EnderecoSeguradoras editarEnderecoSeguradoras( long Id, EnderecoSeguradoras enderecoSeguradoras ) {
		enderecoSeguradoras.setId( Id );
		return enderecoSeguradorasRepository.save( enderecoSeguradoras );
	}
	
	public Optional<EnderecoSeguradoras> buscarEnderecoSeguradoras( long id ){
		return enderecoSeguradorasRepository.findById(id); 
	}
	
	public List<EnderecoSeguradoras> allEnderecoSeguradoras() {
		return (List<EnderecoSeguradoras>) enderecoSeguradorasRepository.findAll();
	}
	
	public EnderecoSeguradoras buscarPorSeguradora( Seguradora seguradora ) {
		return enderecoSeguradorasRepository.findBySeguradora(seguradora);
	}
	

	
}
