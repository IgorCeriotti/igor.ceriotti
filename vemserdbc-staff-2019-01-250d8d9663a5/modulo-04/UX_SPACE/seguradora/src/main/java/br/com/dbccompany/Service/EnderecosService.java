package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Entity.Enderecos;
import br.com.dbccompany.Entity.Pessoas;
import br.com.dbccompany.Repository.EnderecosRepository;

@Service
public class EnderecosService {

	@Autowired
	private EnderecosRepository enderecoRepository; 
	
	@Transactional( rollbackFor = Exception.class )
	public Enderecos salvar( Enderecos endereco ) {
		return enderecoRepository.save( endereco ); 
	}
	
	@Transactional( rollbackFor = Exception.class )
	public void deletar( long Id ) {
		enderecoRepository.deleteById(Id);
	}
	
	@Transactional( rollbackFor = Exception.class )
	public Enderecos editarEndereco( long Id, Enderecos endereco ) {
		endereco.setId( Id );
		return enderecoRepository.save( endereco );
	}
	
	public Optional<Enderecos> buscarEndereco( long id ){
		return enderecoRepository.findById( id ); 
	}
	
	public List<Enderecos> allEnderecos() {
		return (List<Enderecos>) enderecoRepository.findAll();
	}
	
	public Enderecos buscarPorLogradouro( String logradouro ) {
		return enderecoRepository.findByLogradouro(logradouro);
	}
	
	public Enderecos buscarPorNumero( long numero ) {
		return enderecoRepository.findByNumero(numero);
	}
	
	public Enderecos buscarPorComplemento( String complemento ) {
		return enderecoRepository.findByComplemento(complemento);
	}
	
	public List<Enderecos> buscarPorBairro( Bairro bairro ){
		return enderecoRepository.findByBairro(bairro);
	}
	
	public List<Enderecos> buscarPorPessoa( Pessoas pessoa ){
		return enderecoRepository.findByPessoas(pessoa);
	}

	
}
