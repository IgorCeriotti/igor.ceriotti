package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Entity.Segurados;
import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Entity.Servicos;
import br.com.dbccompany.Repository.CorretorRepository;
import br.com.dbccompany.Repository.SeguradosRepository;
import br.com.dbccompany.Repository.ServicoContratadoRepository;

@Service
public class ServicoContratadoService {

	@Autowired
	private ServicoContratadoRepository servicoContratadoRepository;
	
	@Autowired
	private CorretorRepository corretorRepository;
	
	@Autowired
	private SeguradosRepository seguradosRepository;
	
	@Transactional( rollbackFor = Exception.class )
	public ServicoContratado salvar( ServicoContratado servicoContratado ) {
		Corretor corretor = corretorRepository.findById( servicoContratado.getCorretor().getId() ).get();
		Segurados segurados = seguradosRepository.findById( servicoContratado.getSegurado().getId() ).get();
	
		
		Integer qtdAtual = segurados.getQtdServicos();
		segurados.setQtdServicos( qtdAtual + 1 );
		servicoContratado.setSegurado( segurados );
		
		double fatiaComissao = servicoContratado.getValor()*0.1;
		corretor.setComissao( fatiaComissao + corretor.getComissao() );
		servicoContratado.setCorretor( corretor );

		return servicoContratadoRepository.save( servicoContratado ); 
	}
	
	@Transactional( rollbackFor = Exception.class )
	public void deletar( long Id ) {
		servicoContratadoRepository.deleteById(Id);
	}
	
	@Transactional( rollbackFor = Exception.class )
	public ServicoContratado editarServicoContratado( long Id, ServicoContratado servicoContratado ) {
		servicoContratado.setId( Id );
		return servicoContratadoRepository.save( servicoContratado );
	}
	
	public Optional<ServicoContratado> buscarServicoContratado( long id ){
		return servicoContratadoRepository.findById(id); 
	}
	
	public List<ServicoContratado> allServicosContratados() {
		return (List<ServicoContratado>) servicoContratadoRepository.findAll();
	}
	
	public ServicoContratado buscarPorDescricao( String descricao ) {
		return servicoContratadoRepository.findByDescricao(descricao);
	}
	
	public ServicoContratado buscarPorValor( long valor ) {
		return servicoContratadoRepository.findByValor(valor);
	}
	
	public List<ServicoContratado> buscarPorServico( Servicos servico ){
		return servicoContratadoRepository.findByServicos(servico);
	}
	
	public List<ServicoContratado> buscarPorSeguradora( Seguradora seguradora ){
		return servicoContratadoRepository.findBySeguradoras(seguradora);
	}
	
	public List<ServicoContratado> buscarPorSegurado( Segurados segurado ){
		return servicoContratadoRepository.findBySegurado(segurado);
	}
	
	public List<ServicoContratado> buscarPorCorretor( Corretor corretor ){
		return servicoContratadoRepository.findByCorretor(corretor);
	}
	
	
	
}
