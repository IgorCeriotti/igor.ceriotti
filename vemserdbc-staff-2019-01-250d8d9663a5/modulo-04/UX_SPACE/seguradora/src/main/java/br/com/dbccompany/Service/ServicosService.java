package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Entity.Servicos;
import br.com.dbccompany.Repository.ServicosRepository;

@Service
public class ServicosService {

	@Autowired
	private ServicosRepository servicosRepository; 
	
	@Transactional( rollbackFor = Exception.class )
	public Servicos salvar( Servicos servico ) {
		return servicosRepository.save( servico ); 
	}
	
	@Transactional( rollbackFor = Exception.class )
	public void deletar( long Id ) {
		servicosRepository.deleteById(Id);
	}
	
	@Transactional( rollbackFor = Exception.class )
	public Servicos editarServico( long Id, Servicos servico ) {
		servico.setId( Id );
		return servicosRepository.save( servico );
	}
	
	public Optional<Servicos> buscarServico( long id ){
		return servicosRepository.findById(id); 
	}
	
	public List<Servicos> allServicos() {
		return (List<Servicos>) servicosRepository.findAll();
	}
	
	public Servicos buscarPorNome( String nome ) {
		return servicosRepository.findByNome(nome);
	}

	public Servicos buscarPorDescricao( String descricao ) {
		return servicosRepository.findByDescricao(descricao);
	}
	
	public Servicos buscarPorValorPadrao( long valorPadrao ) {
		return servicosRepository.findByValorPadrao(valorPadrao);
	}
	
	public List<Servicos> buscarPorSeguradora( Seguradora seguradora ){
		return servicosRepository.findBySeguradoras(seguradora);
	}
	
	public Servicos buscarPorServicoContratado( ServicoContratado servicoContratado ) {
		return servicosRepository.findByServicosContratados(servicoContratado);
	}
}
