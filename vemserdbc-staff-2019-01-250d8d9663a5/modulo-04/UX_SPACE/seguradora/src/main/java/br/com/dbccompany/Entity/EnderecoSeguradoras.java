package br.com.dbccompany.Entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class, 
		  property = "id")
@PrimaryKeyJoinColumn( name = "ID_ENDERECOS" )
public class EnderecoSeguradoras extends Enderecos {

	@OneToOne
	@JoinColumn( name = "ID_SEGURADORA" )
	private Seguradora seguradora; 
}
