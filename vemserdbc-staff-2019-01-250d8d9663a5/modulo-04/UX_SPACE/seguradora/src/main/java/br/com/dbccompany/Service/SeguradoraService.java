package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.EnderecoSeguradoras;
import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Entity.Servicos;
import br.com.dbccompany.Repository.SeguradoraRepository;

@Service
public class SeguradoraService {

	@Autowired
	private SeguradoraRepository seguradoraRepository; 
	
	@Transactional( rollbackFor = Exception.class )
	public Seguradora salvar( Seguradora seguradora ) {
		return seguradoraRepository.save( seguradora ); 
	}
	
	@Transactional( rollbackFor = Exception.class )
	public void deletar( long Id ) {
		seguradoraRepository.deleteById(Id);
	}
	
	@Transactional( rollbackFor = Exception.class )
	public Seguradora editarSeguradora( long Id, Seguradora seguradora ) {
		seguradora.setId( Id );
		return seguradoraRepository.save( seguradora );
	}
	
	public Optional<Seguradora> buscarSeguradora( long id ){
		return seguradoraRepository.findById(id); 
	}
	
	public List<Seguradora> allSeguradoras() {
		return (List<Seguradora>) seguradoraRepository.findAll();
	}
	
	public Seguradora buscarPorNome( String nome ) {
		return seguradoraRepository.findByNome(nome);
	}
	
	public Seguradora buscarPorCnpj( long cnpj ) {
		return seguradoraRepository.findByCnpj(cnpj);
	}
	
	public List<Seguradora> buscarPorServico( Servicos servico ){
		return seguradoraRepository.findByServicos(servico);
	}
	
	public Seguradora buscarPorServicoContratado( ServicoContratado servicoContratado ) {
		return seguradoraRepository.findByServicosContratados(servicoContratado);
	}
	
	public Seguradora buscarPorEndereco( EnderecoSeguradoras enderecoSeguradora ) {
		return seguradoraRepository.findByEndereco(enderecoSeguradora);
	}

	
	
}
