package br.com.dbccompany.Entity;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class, 
		  property = "id")
public class Seguradora {
	@Id
	@GeneratedValue( strategy = GenerationType.SEQUENCE )
	private long id; 
	
	@Column( name = "NOME", nullable = false )
	private String nome; 
	
	@Column( name = "CNPJ", nullable = false, unique = true )
	private long cnpj;
	
	@OneToOne ( mappedBy = "seguradora" )
	private EnderecoSeguradoras endereco; 
	
    @ManyToMany()
    @JoinTable(name = "SEGURADORAS_SERVICOS",
            joinColumns = {
            	@JoinColumn(name = "id_seguradora")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_servicos")})
    private List<Servicos> servicos = new ArrayList<>();
    
    @OneToMany( mappedBy = "seguradoras" )
    private List<ServicoContratado> servicosContratados = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getCnpj() {
		return cnpj;
	}

	public void setCnpj(long cpnj) {
		this.cnpj = cpnj;
	}

	public EnderecoSeguradoras getEndereco() {
		return endereco;
	}

	public void setEndereco(EnderecoSeguradoras endereco) {
		this.endereco = endereco;
	}

	public List<Servicos> getServicos() {
		return servicos;
	}

	public void pushServicos(Servicos... servicos) {
		this.servicos.addAll(Arrays.asList(servicos));
	}

	public List<ServicoContratado> getServicosContratados() {
		return servicosContratados;
	}

	public void pushServicosContratados(ServicoContratado... servicosContratados) {
		this.servicosContratados.addAll(Arrays.asList(servicosContratados));
	}
    
    

}
