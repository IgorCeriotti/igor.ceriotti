package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Segurados;
import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Repository.SeguradosRepository;

@Service
public class SeguradosService {

	@Autowired
	private SeguradosRepository seguradosRepository; 
	
	@Transactional( rollbackFor = Exception.class )
	public Segurados salvar( Segurados segurado ) {
		return seguradosRepository.save( segurado ); 
	}
	
	@Transactional( rollbackFor = Exception.class )
	public void deletar( long Id ) {
		seguradosRepository.deleteById(Id);
	}
	
	@Transactional( rollbackFor = Exception.class )
	public Segurados editarSegurado( long Id, Segurados segurado ) {
		segurado.setId( Id );
		return seguradosRepository.save( segurado );
	}
	
	public Optional<Segurados> buscarSegurados( long id ){
		return seguradosRepository.findById(id); 
	}
	
	public List<Segurados> allSegurados() {
		return (List<Segurados>) seguradosRepository.findAll();
	}
	
	public Segurados buscarPorQtdServicos( Integer qtdServicos ) {
		return seguradosRepository.findByQtdServicos(qtdServicos);
	}
	
	public Segurados buscarPorServicoContratado( ServicoContratado servicoContratado ) {
		return seguradosRepository.findByServicosContratados(servicoContratado);
	}

}
