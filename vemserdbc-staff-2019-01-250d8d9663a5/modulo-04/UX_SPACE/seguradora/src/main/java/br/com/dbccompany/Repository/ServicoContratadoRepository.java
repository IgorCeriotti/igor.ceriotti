package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Entity.Segurados;
import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Entity.Servicos;

public interface ServicoContratadoRepository extends CrudRepository<ServicoContratado, Long>{

	ServicoContratado findByDescricao( String descricao );
	ServicoContratado findByValor( long valor );
	List<ServicoContratado> findByServicos( Servicos servico );
	List<ServicoContratado> findBySeguradoras( Seguradora seguradora );
	List<ServicoContratado> findBySegurado( Segurados segurado );
	List<ServicoContratado> findByCorretor( Corretor corretor ); 
	
	
}
