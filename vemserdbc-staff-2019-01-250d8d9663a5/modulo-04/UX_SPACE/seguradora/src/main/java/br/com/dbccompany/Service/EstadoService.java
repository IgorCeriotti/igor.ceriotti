package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Cidade;
import br.com.dbccompany.Entity.Estado;
import br.com.dbccompany.Repository.EstadoRepository;

@Service
public class EstadoService {
	
	@Autowired
	private EstadoRepository estadoRepository; 
	
	@Transactional( rollbackFor = Exception.class )
	public Estado salvar( Estado estado ) {
		return estadoRepository.save( estado ); 
	}
	
	@Transactional( rollbackFor = Exception.class )
	public void deletar( long Id ) {
		estadoRepository.deleteById(Id);
	}
	
	@Transactional( rollbackFor = Exception.class )
	public Estado editarEstado( long Id, Estado estado ) {
		estado.setId( Id );
		return estadoRepository.save( estado );
	}
	
	public Optional<Estado> buscarEstado( long id ){
		return estadoRepository.findById(id); 
	}
	
	public List<Estado> allEstados() {
		return (List<Estado>) estadoRepository.findAll();
	}
	
	public Estado buscarPorNome( String nome ) {
		return estadoRepository.findByNome(nome);
	}
	
	public Estado buscarPorCidade( Cidade cidade ) {
		return estadoRepository.findByCidades(cidade);
	}

}
