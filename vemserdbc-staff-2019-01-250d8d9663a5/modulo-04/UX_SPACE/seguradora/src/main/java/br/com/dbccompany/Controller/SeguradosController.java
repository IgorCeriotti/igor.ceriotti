package br.com.dbccompany.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Segurados;
import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Service.SeguradosService;

@Controller
@RequestMapping("/api/segurados")

public class SeguradosController {

	@Autowired
	SeguradosService seguradosService; 
	
	@GetMapping( value = "/" )
	@ResponseBody
	public List<Segurados> listSegurados(){
		return seguradosService.allSegurados();
	}
	
	@PostMapping( value = "/novo" )
	@ResponseBody
	public Segurados novoSegurado( @RequestBody Segurados segurado ) {
		return seguradosService.salvar(segurado);
	}
	
	@GetMapping( value = "/{id}" )
	@ResponseBody
	public Optional<Segurados> seguradoEspecifico( @PathVariable long id) {
		return seguradosService.buscarSegurados(id);
	}
	
	@PutMapping( value = "/editar/{id}" )
	@ResponseBody
	public Segurados editarSegurado( @PathVariable long id, @RequestBody Segurados segurado ) {
		return seguradosService.editarSegurado(id, segurado);
	}
	
	@PutMapping( value = "/contratarServico/{id}" )
	@ResponseBody
	public Segurados contratarServico( @PathVariable long id, @RequestBody ServicoContratado servicoContratado) {
		Optional<Segurados> seguradoOpcional = seguradosService.buscarSegurados( id );
		Segurados segurado = seguradoOpcional.get();
		
		segurado.pushServicosContratados( servicoContratado );
		
		Integer qtdServicosAtual = segurado.getQtdServicos();
		segurado.setQtdServicos( qtdServicosAtual + 1 );
				
		return seguradosService.editarSegurado( id, segurado );
	}
	
	
}
