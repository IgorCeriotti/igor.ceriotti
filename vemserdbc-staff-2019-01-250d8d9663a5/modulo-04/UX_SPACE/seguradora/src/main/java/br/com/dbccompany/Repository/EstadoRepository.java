package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Cidade;
import br.com.dbccompany.Entity.Estado;

import java.util.List;

public interface EstadoRepository extends CrudRepository<Estado, Long>{

	Estado findByNome(String nome);
	Estado findByCidades(Cidade cidade);
	List<Estado> findAll();
	
}
