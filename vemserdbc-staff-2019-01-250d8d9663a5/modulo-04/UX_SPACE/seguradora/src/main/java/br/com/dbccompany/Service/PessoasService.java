package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Enderecos;
import br.com.dbccompany.Entity.Pessoas;
import br.com.dbccompany.Repository.PessoasRepository;

@Service
public class PessoasService {

	@Autowired
	private PessoasRepository pessoasRepository; 
	
	@Transactional( rollbackFor = Exception.class )
	public Pessoas salvar( Pessoas pessoas ) {
		return pessoasRepository.save( pessoas ); 
	}
	
	@Transactional( rollbackFor = Exception.class )
	public void deletar( long Id ) {
		pessoasRepository.deleteById(Id);
	}
	
	@Transactional( rollbackFor = Exception.class )
	public Pessoas editarPessoas( long Id, Pessoas pessoas ) {
		pessoas.setId( Id );
		return pessoasRepository.save( pessoas );
	}
	
	public Optional<Pessoas> buscarPessoas( long id ){
		return pessoasRepository.findById(id); 
	}
	
	public List<Pessoas> allPessoas() {
		return (List<Pessoas>) pessoasRepository.findAll();
	}
	
	public Pessoas buscarPorNome( String nome ) {
		return pessoasRepository.findByNome(nome);
	}
	
	public Pessoas buscarPorCpf( long cpf ) {
		return pessoasRepository.findByCpf(cpf);
	}
	
	public Pessoas buscarPorPai( String pai ) {
		return pessoasRepository.findByPai(pai);
	}
	
	public Pessoas buscarPorMae( String mae ) {
		return pessoasRepository.findByMae(mae);
	}
	
	public Pessoas buscarPorTelegone( long telefone ) {
		return pessoasRepository.findByTelefone(telefone);
	}
	
	public Pessoas buscarPorEmail( String email ) {
		return pessoasRepository.findByEmail(email);
	}
	
	public List<Pessoas> buscarPorEndereco( Enderecos endereco ){
		return pessoasRepository.findByEnderecos(endereco);
	}

	
}
