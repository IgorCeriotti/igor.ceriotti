package br.com.dbccompany.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Service.SeguradoraService;

@Controller
@RequestMapping("/api/seguradora")

public class SeguradoraController {

	@Autowired
	SeguradoraService seguradoraService; 
	
	@GetMapping( value = "/" )
	@ResponseBody
	public List<Seguradora> listSeguradoras(){
		return seguradoraService.allSeguradoras();
	}
	
	@PostMapping( value = "/novo" )
	@ResponseBody
	public Seguradora novaSeguradora( @RequestBody Seguradora seguradora ) {
		return seguradoraService.salvar(seguradora);
	}
	
	@GetMapping( value = "/{id}" )
	@ResponseBody
	public Optional<Seguradora> seguradoraEspecifica( @PathVariable long id) {
		return seguradoraService.buscarSeguradora(id);
	}
	
	@PutMapping( value = "/editar/{id}" )
	@ResponseBody
	public Seguradora editarSeguradora( @PathVariable long id, @RequestBody Seguradora seguradora ) {
		return seguradoraService.editarSeguradora(id, seguradora);
	}
	
	

}
