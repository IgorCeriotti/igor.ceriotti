package br.com.dbccompany.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.EnderecoSeguradoras;
import br.com.dbccompany.Service.EnderecoSeguradorasService;

@Controller
@RequestMapping("/api/enderecoSeguradoras")

public class EnderecoSeguradorasController {

	@Autowired
	EnderecoSeguradorasService enderecoSeguradorasService; 
	
	@GetMapping( value = "/" )
	@ResponseBody
	public List<EnderecoSeguradoras> listEnderecosSeguradoras(){
		return enderecoSeguradorasService.allEnderecoSeguradoras();
	}
	
	@PostMapping( value = "/novo" )
	@ResponseBody
	public EnderecoSeguradoras novoEnderecoSeguradoras( @RequestBody EnderecoSeguradoras enderecoSeguradoras ) {
		return enderecoSeguradorasService.salvar(enderecoSeguradoras);
	}
	
	@GetMapping( value = "/{id}" )
	@ResponseBody
	public Optional<EnderecoSeguradoras> enderecoSeguradorasEspecifico( @PathVariable long id) {
		return enderecoSeguradorasService.buscarEnderecoSeguradoras(id);	
	}
	
	@PutMapping( value = "/editar/{id}" )
	@ResponseBody
	public EnderecoSeguradoras editarEnderecoSeguradoras( @PathVariable long id, @RequestBody EnderecoSeguradoras enderecoSeguradoras ) {
		return enderecoSeguradorasService.editarEnderecoSeguradoras(id, enderecoSeguradoras);
	}

}
