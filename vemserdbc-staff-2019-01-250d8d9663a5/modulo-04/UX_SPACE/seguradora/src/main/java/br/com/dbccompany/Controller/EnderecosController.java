package br.com.dbccompany.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Enderecos;
import br.com.dbccompany.Service.EnderecosService;

@Controller
@RequestMapping("/api/enderecos")

public class EnderecosController {
	@Autowired
	EnderecosService enderecosService; 
	
	@GetMapping( value = "/" )
	@ResponseBody
	public List<Enderecos> listEnderecos(){
		return enderecosService.allEnderecos();
	}
	
	@PostMapping( value = "/novo" )
	@ResponseBody
	public Enderecos novoEndereco( @RequestBody Enderecos endereco ) {
		return enderecosService.salvar(endereco);
	}
	
	@GetMapping( value = "/{id}" )
	@ResponseBody
	public Optional<Enderecos> enderecoEspecifico( @PathVariable long id) {
		return enderecosService.buscarEndereco(id);
	}
	
	@PutMapping( value = "/editar/{id}" )
	@ResponseBody
	public Enderecos editarEndereco( @PathVariable long id, @RequestBody Enderecos endereco ) {
		return enderecosService.editarEndereco(id, endereco);
	}

}
