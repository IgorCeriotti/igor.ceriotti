package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.EnderecoSeguradoras;
import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Entity.Servicos;

public interface SeguradoraRepository extends CrudRepository<Seguradora, Long>{

	Seguradora findByNome( String nome );
	Seguradora findByCnpj( long cnpj );
	Seguradora findByEndereco( EnderecoSeguradoras endereco );
	List<Seguradora> findByServicos( Servicos servico ); 
	Seguradora findByServicosContratados( ServicoContratado servicoContratado );
	
	
}
