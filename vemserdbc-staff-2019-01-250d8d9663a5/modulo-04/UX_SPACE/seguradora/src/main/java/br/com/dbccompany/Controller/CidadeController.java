package br.com.dbccompany.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Cidade;
import br.com.dbccompany.Service.CIdadeService;

@Controller
@RequestMapping("/api/cidade")

public class CidadeController {

	@Autowired
	CIdadeService cidadeService; 
	
	@GetMapping( value = "/" )
	@ResponseBody
	public List<Cidade> listCidades(){
		return cidadeService.allCidades();
	}
	
	@PostMapping( value = "/novo" )
	@ResponseBody
	public Cidade novaCidade( @RequestBody Cidade cidade ) {
		return cidadeService.salvar(cidade);
	}
	
	@GetMapping( value = "/{id}" )
	@ResponseBody
	public Optional<Cidade> cidadeEspecifica( @PathVariable long id) {
		return cidadeService.buscarCIdade(id);
	}
	
	@PutMapping( value = "/editar/{id}" )
	@ResponseBody
	public Cidade editarCidade( @PathVariable long id, @RequestBody Cidade cidade ) {
		return cidadeService.editarCidade(id, cidade);
	}
}
