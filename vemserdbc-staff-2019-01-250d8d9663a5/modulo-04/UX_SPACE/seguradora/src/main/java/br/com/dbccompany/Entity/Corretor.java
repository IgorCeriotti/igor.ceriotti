package br.com.dbccompany.Entity;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "id_pessoas")
public class Corretor extends Pessoas { 
	
	@Column( name = "CARGO", nullable = false )
	private String cargo; 
	
	@Column( name = "COMISSAO", nullable = false )
	private double comissao;
	
	@OneToMany( mappedBy = "corretor" )
	private List<ServicoContratado> servicosContratados = new ArrayList<>();

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public double getComissao() {
		return comissao;
	}

	public void setComissao(double comissao) {
		this.comissao = comissao;
	}

	public List<ServicoContratado> getServicosContratados() {
		return servicosContratados;
	}

	public void pushServicosContratados(ServicoContratado... servicosContratados) {
		this.servicosContratados.addAll(Arrays.asList(servicosContratados));
	}
}
