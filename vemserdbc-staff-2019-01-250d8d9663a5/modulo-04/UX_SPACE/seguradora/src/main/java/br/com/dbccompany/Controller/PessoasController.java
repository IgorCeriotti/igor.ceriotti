package br.com.dbccompany.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Pessoas;
import br.com.dbccompany.Service.PessoasService;

@Controller
@RequestMapping("/api/pessoas")

public class PessoasController {

	@Autowired
	PessoasService pessoasService; 
	
	@GetMapping( value = "/" )
	@ResponseBody
	public List<Pessoas> listPessoas(){
		return pessoasService.allPessoas();
	}
	
	@PostMapping( value = "/novo" )
	@ResponseBody
	public Pessoas novaPessoa( @RequestBody Pessoas pessoa ) {
		return pessoasService.salvar(pessoa);
	}
	
	@GetMapping( value = "/{id}" )
	@ResponseBody
	public Optional<Pessoas> pessoaEspecifica( @PathVariable long id) {
		return pessoasService.buscarPessoas(id);
	}
	
	@PutMapping( value = "/editar/{id}" )
	@ResponseBody
	public Pessoas editarPessoa( @PathVariable long id, @RequestBody Pessoas pessoa ) {
		return pessoasService.editarPessoas(id, pessoa);
	}

}
