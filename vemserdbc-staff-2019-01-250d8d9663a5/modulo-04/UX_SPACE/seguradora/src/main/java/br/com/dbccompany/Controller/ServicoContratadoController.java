package br.com.dbccompany.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Service.ServicoContratadoService;

@Controller
@RequestMapping("/api/servicoContratado")
public class ServicoContratadoController {

	@Autowired
	ServicoContratadoService servicoContratadoService; 
	
	@GetMapping( value = "/" )
	@ResponseBody
	public List<ServicoContratado> listServicosContratados(){
		return servicoContratadoService.allServicosContratados();
	}
	
	@PostMapping( value = "/novo" )
	@ResponseBody
	public ServicoContratado novoServicoContratado( @RequestBody ServicoContratado servicoContratado ) {
		return servicoContratadoService.salvar(servicoContratado);
	}
	
	@GetMapping( value = "/{id}" )
	@ResponseBody
	public Optional<ServicoContratado> servicoContratadoEspecifico( @PathVariable long id) {
		return servicoContratadoService.buscarServicoContratado(id);
	}
	
	@PutMapping( value = "/editar/{id}" )
	@ResponseBody
	public ServicoContratado editarServicoContratado( @PathVariable long id, @RequestBody ServicoContratado servicoContratado ) {
		return servicoContratadoService.editarServicoContratado(id, servicoContratado);
	}
	
//	@GetMapping( value = "/relatorio/{id}" )
//	@ResponseBody
//	public Relatorio buscarRelatorio( @RequestBody ServicoContratado servicoContratado ) {
//		Servicos servico = servicoContratado.getServicos();
//		double valorPadraoDoServico = servico.getValorPadrao();
//		
//		Relatorio relatorio = new Relatorio();		
//		
//		relatorio.setValorPadrao( valorPadraoDoServico );
//		relatorio.setServicoContratado( servicoContratado );
//		
//		return relatorio; 
//	}
	
	
}
