package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Segurados;
import br.com.dbccompany.Entity.ServicoContratado;

public interface SeguradosRepository extends CrudRepository<Segurados, Long>{

	Segurados findByQtdServicos( Integer qtdServicos );
	Segurados findByServicosContratados( ServicoContratado servicoContratado ); 
	
	
}
