package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class, 
		  property = "id")
public class Bairro {
	@Id
	@GeneratedValue( strategy = GenerationType.SEQUENCE )
	private long id; 
	
	@Column(name = "NOME", nullable = false )
	private String nome;
	
	@ManyToOne
	@JoinColumn( name = "id_cidade" )
	private Cidade cidade;
	
	@OneToMany( mappedBy = "bairro" )
	private List<Enderecos> enderecos = new ArrayList<>(); 
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public List<Enderecos> getEnderecos() {
		return enderecos;
	}

	public void pushEnderecos(Enderecos... enderecos) {
		this.enderecos.addAll(Arrays.asList(enderecos));
	} 
	
	
	
	
}
