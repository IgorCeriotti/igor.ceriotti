package br.com.dbccompany.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Servicos;
import br.com.dbccompany.Service.ServicosService;

@Controller
@RequestMapping("/api/servicos")

public class ServicosController {

	@Autowired
	ServicosService servicosService; 
	
	@GetMapping( value = "/" )
	@ResponseBody
	public List<Servicos> listServicos(){
		return servicosService.allServicos();
	}
	
	@PostMapping( value = "/novo" )
	@ResponseBody
	public Servicos novoServico( @RequestBody Servicos servico ) {
		return servicosService.salvar(servico);
	}
	
	@GetMapping( value = "/{id}" )
	@ResponseBody
	public Optional<Servicos> servicoEspecifico( @PathVariable long id) {
		return servicosService.buscarServico(id);
	}
	
	@PutMapping( value = "/editar/{id}" )
	@ResponseBody
	public Servicos editarServico( @PathVariable long id, @RequestBody Servicos servico ) {
		return servicosService.editarServico(id, servico);
	}

}
