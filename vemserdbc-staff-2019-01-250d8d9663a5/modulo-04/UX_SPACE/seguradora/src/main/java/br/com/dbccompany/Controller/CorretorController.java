package br.com.dbccompany.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Service.CorretorService;

@Controller
@RequestMapping("/api/corretor")

public class CorretorController {

	@Autowired
	CorretorService corretorService; 
	
	@GetMapping( value = "/" )
	@ResponseBody
	public List<Corretor> listCorretores(){
		return corretorService.allCorretores();
	}
	
	@PostMapping( value = "/novo" )
	@ResponseBody
	public Corretor novoCorretor( @RequestBody Corretor corretor ) {
		return corretorService.salvar(corretor);
	}
	
	@GetMapping( value = "/{id}" )
	@ResponseBody
	public Optional<Corretor> corretorEspecifico( @PathVariable long id) {
		return corretorService.buscarCorretor(id);
	}
	
	@PutMapping( value = "/editar/{id}" )
	@ResponseBody
	public Corretor editarCorretor( @PathVariable long id, @RequestBody Corretor corretor ) {
		return corretorService.editarCorretor(id, corretor);
	}
	
	@PutMapping( value = "/contratarServico/{id}" )
	@ResponseBody
	public Corretor contratarServico( @PathVariable long id, @RequestBody ServicoContratado servicoContratado ) {
		Optional<Corretor> corretorOpcional = corretorService.buscarCorretor(id);
		Corretor corretor = corretorOpcional.get();
		
		double valorDoServicoContratado = servicoContratado.getValor();
		double comissaoDoCorretor = corretor.getComissao();
		double porcentagemValorServicoParaComissao = valorDoServicoContratado*0.1;
		double comissaoIncrementada = comissaoDoCorretor + porcentagemValorServicoParaComissao; 
		
		corretor.setComissao( comissaoIncrementada );
		corretor.pushServicosContratados( servicoContratado );
		
		return corretorService.editarCorretor( id, corretor );
	}
	
}
