package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class, 
		  property = "id")
public class Servicos {

	@Id
	@GeneratedValue( strategy = GenerationType.SEQUENCE )
	private long id; 
	
	@Column( name = "NOME", nullable = false )
	private String nome; 
	
	@Column( name = "DESCRICAO", nullable = false )
	private String descricao; 
	
	@Column( name = "VALOR_PADRAO", nullable = false )
	private double valorPadrao;
	
	@ManyToMany( mappedBy = "servicos" )
    private List<Seguradora> seguradoras = new ArrayList<>();
	
	@OneToMany( mappedBy = "servicos" )
	private List<ServicoContratado> servicosContratados = new ArrayList<>(); 


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValorPadrao() {
		return valorPadrao;
	}

	public void setValorPadrao(double valorPadrao) {
		this.valorPadrao = valorPadrao;
	}

	public List<Seguradora> getSeguradoras() {
		return seguradoras;
	}

	public void pushSeguradoras(Seguradora... seguradoras) {
		this.seguradoras.addAll(Arrays.asList(seguradoras));
	}

	public List<ServicoContratado> getServicosContratados() {
		return servicosContratados;
	}

	public void pushServicosContratados(ServicoContratado... servicosContratados) {
		this.servicosContratados.addAll(Arrays.asList(servicosContratados));
	} 
	
	
	
	
}
