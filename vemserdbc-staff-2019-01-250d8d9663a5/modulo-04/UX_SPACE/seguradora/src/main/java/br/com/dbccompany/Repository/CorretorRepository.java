package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Entity.ServicoContratado;

public interface CorretorRepository extends CrudRepository<Corretor, Long>{

	Corretor findByCargo( String cargo );
	Corretor findByComissao( long comissao );
	Corretor findByServicosContratados( ServicoContratado servicoContratado );
	
	
}
