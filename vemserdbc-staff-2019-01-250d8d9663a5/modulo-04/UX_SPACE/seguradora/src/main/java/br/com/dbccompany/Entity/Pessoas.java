package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Pessoas {
	@Id
	@GeneratedValue( strategy = GenerationType.SEQUENCE )
	private long id; 
	
	@Column( name = "nome", nullable = false )
	private String nome; 
	
	@Column( name = "cpf", nullable = false, unique = true )
	private long cpf;
	
	@Column( name = "pai", nullable = false )
	private String pai;
	
	@Column( name = "mae", nullable = false )
	private String mae; 
	
	@Column( name = "telefone", nullable = false )
	private long telefone; 
	
	@Column( name = "email", nullable = false, unique = true )
	private String email;
	
	@ManyToMany()
    @JoinTable(name = "enderecos_pessoas",
            joinColumns = {
            	@JoinColumn(name = "id_Pessoas")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_Enderecos")})
    private List<Enderecos> enderecos = new ArrayList<>();
	


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getCpf() {
		return cpf;
	}

	public void setCpf(long cpf) {
		this.cpf = cpf;
	}

	public String getPai() {
		return pai;
	}

	public void setPai(String pai) {
		this.pai = pai;
	}

	public String getMae() {
		return mae;
	}

	public void setMae(String mae) {
		this.mae = mae;
	}

	public long getTelefone() {
		return telefone;
	}

	public void setTelefone(long telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Enderecos> getEnderecos() {
		return enderecos;
	}

	public void pushEnderecos(Enderecos... enderecos) {
		this.enderecos.addAll(Arrays.asList(enderecos));
	}
}
