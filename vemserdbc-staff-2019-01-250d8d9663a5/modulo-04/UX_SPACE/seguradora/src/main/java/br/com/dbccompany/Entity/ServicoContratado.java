package br.com.dbccompany.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class, 
		  property = "id")
public class ServicoContratado {

	@Id
	@GeneratedValue( strategy = GenerationType.SEQUENCE )
	private long id;
	
	@Column( name = "DESCRICAO", nullable = false )
	private String descricao;
	
	@Column( name = "VALOR", nullable = false )
	private double valor; 
	
	@ManyToOne
	@JoinColumn( name = "ID_SERVICOS" )
	private Servicos servicos; 
	
	@ManyToOne
	@JoinColumn( name = "ID_SEGURADORA" )
	private Seguradora seguradoras; 
	
	@ManyToOne
	@JoinColumn( name = "ID_PESSOAS_SEGURADOS", nullable = false )
	private Segurados segurado; 
	
	@ManyToOne
	@JoinColumn( name = "ID_PESSOAS_CORRETOR", nullable = false )
	private Corretor corretor;
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public Servicos getServicos() {
		return servicos;
	}

	public void setServicos(Servicos servicos) {
		this.servicos = servicos;
	}

	public Seguradora getSeguradoras() {
		return seguradoras;
	}

	public void setSeguradoras(Seguradora seguradoras) {
		this.seguradoras = seguradoras;
	}

	public Segurados getSegurado() {
		return segurado;
	}

	public void setSegurado(Segurados segurado) {
		this.segurado = segurado;
	}

	public Corretor getCorretor() {
		return corretor;
	}

	public void setCorretor(Corretor corretor) {
		this.corretor = corretor;
	}

	
	
	
	
	
}
