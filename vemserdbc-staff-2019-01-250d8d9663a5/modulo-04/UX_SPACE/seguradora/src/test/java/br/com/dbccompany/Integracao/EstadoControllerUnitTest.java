package br.com.dbccompany.Integracao;

import br.com.dbccompany.Controller.EstadoController;
import br.com.dbccompany.Entity.Estado;
import br.com.dbccompany.Repository.EstadoRepository;
import org.junit.Before;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class EstadoControllerUnitTest {

    private MockMvc mockMvc;

    @Mock
    private EstadoRepository estadoRepository;

    @InjectMocks
    private EstadoController estadoController;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(estadoController)
                .build();

        Estado estado1 = new Estado();
        estado1.setNome( "Pará" );

        Estado estado2 = new Estado();
        estado2.setNome( "Bahia" );
    }




}
