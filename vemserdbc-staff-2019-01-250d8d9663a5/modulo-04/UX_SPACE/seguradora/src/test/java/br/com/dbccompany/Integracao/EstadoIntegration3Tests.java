package br.com.dbccompany.Integracao;

import br.com.dbccompany.Controller.EstadoController;
import br.com.dbccompany.Entity.Estado;
import br.com.dbccompany.Repository.EstadoRepository;
import br.com.dbccompany.SeguradoraApplicationTests;
import br.com.dbccompany.Service.EstadoService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static com.sun.org.apache.xerces.internal.util.PropertyState.is;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class EstadoIntegration3Tests extends SeguradoraApplicationTests {

	private MockMvc mvc; 
	
	@Autowired
	private EstadoController controller;

	@Autowired
	private EstadoRepository estadoRepository;

	@Autowired
	private EstadoService estadoService;

	@Autowired
	ApplicationContext context;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mvc = MockMvcBuilders.standaloneSetup(controller).build();

		Estado estado = new Estado();
		estado.setNome( "Pará" );
	}
	
	@Test
	public void statusOk() throws Exception {
		this.mvc.perform(get("/api/estado/")).andExpect(status().isOk()); // ok = status 200
	}

	/*@Test
	public void getEstadoByIdAPI() throws Exception
	{
		mvc.perform( MockMvcRequestBuilders
				.get("/estado/{id}", 1))
				.andExpect(MockMvcResultMatchers.jsonPath("$.estadoId").value(1));
	}*/



	@Test
	public void test_get_all_success() throws Exception {
		Estado estado1 = new Estado();
		estado1.setNome( "Pará" );
		Estado estado2 = new Estado();
		estado2.setNome( "Bahia" );
		List<Estado> estados = Arrays.asList( estado1 ,estado2);

		EstadoService mock = org.mockito.Mockito.mock(EstadoService.class);
		Mockito.when(mock.allEstados()).thenReturn(estados);

		mvc.perform(get("/api/estado/"))
				.andExpect( status().isOk())
				.andExpect( content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
				//.andExpect((ResultMatcher) jsonPath("$.size()", is(2)));
		Mockito.verify(mock, times(1)).allEstados();
		Mockito.verifyNoMoreInteractions(mock);
	}

}
