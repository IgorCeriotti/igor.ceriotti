package br.com.dbccompany.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Cargo;
import br.com.dbccompany.Entity.Decisor;
import br.com.dbccompany.Entity.SolicitacaoEmprestimo;
import br.com.dbccompany.Repository.DecisorRepository;

@Service
public class DecisorService {

	@Autowired
	private DecisorRepository decisorRepository; 
	
	@Transactional(rollbackFor = Exception.class)
	public Decisor salvar(Decisor decisor) {
		return decisorRepository.save(decisor); 
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(long Id) {
		decisorRepository.deleteById(Id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Decisor editarDecisor(long Id, Decisor decisor) {
		decisor.setId(Id);
		return decisorRepository.save(decisor);
	}
	
	public Optional<Decisor> buscarDecisor(long id){
		return decisorRepository.findById(id); 
	}
	
	public Decisor buscarPorRegras(String regras) {
		return decisorRepository.findByRegras(regras);
	}
	
	public Decisor buscarPorCargo(Cargo cargo) {
		return decisorRepository.findByCargo(cargo);
	}
	
	public Decisor buscarPorSolicitacao(SolicitacaoEmprestimo solicitacaoEmprestimo) {
		return decisorRepository.findBySolicitacoesEmprestimos(solicitacaoEmprestimo);
	}
	
}
