package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Service.BancoService;

@Controller
@RequestMapping("/api/banco")
public class BancoController {
	
	@Autowired
	BancoService bancoService;
	
	@GetMapping(value="/") //nao to passando dados, apenas quero pegar algo: usar get
	@ResponseBody     //quero a resposta no corpo do retorno 
	public List<Banco> lstBancos(){
		return bancoService.allBancos();
	}
	
	@PostMapping(value="/novo")
	@ResponseBody
	public Banco novoBanco(@RequestBody Banco banco) {
		return bancoService.salvar(banco); 
	}
	
	@GetMapping(value="/{codigo}")
	@ResponseBody
	public Banco bancoEspecifico(@PathVariable long codigo) { //path: pq o codigo entre chaves é dinamico!
		return bancoService.buscarBancoPorCodigo(codigo); 
	}
	
	@PutMapping(value="/editar/{id}") //para dar update no banco, tem os mesmos paramentos de get e post.
	@ResponseBody
	public Banco editarBanco(@PathVariable long id, @RequestBody Banco banco) {
		return bancoService.editarBanco(id, banco);
	}
}

