package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;


@Entity
public class Cliente {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id; 
	
	@Column(name = "NOME", nullable
			= false)
	private String nome;
	
	@Column(name = "CPF", nullable = false )
	private long cpf; 
	
	@Column(name = "RG", nullable = false )
	private long rg;
	
	@Column(name = "SENHA", nullable = false )
	private long senha;
	
    @ManyToMany()
    @JoinTable(name = "lista_enderecos",
            joinColumns = {
            	@JoinColumn(name = "id_cliente")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_endereco")})
    private List<Endereco> enderecos = new ArrayList<>();

    @ManyToMany()
    @JoinTable(name = "lista_contatos",
            joinColumns = {
            	@JoinColumn(name = "id_cliente")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_contato_cliente")})
    private List<ContatoCliente> contatos = new ArrayList<>();
    
    @ManyToMany(mappedBy = "clientes")
    private List<Conta> contas = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getCpf() {
		return cpf;
	}

	public void setCpf(long cpf) {
		this.cpf = cpf;
	}

	public long getRg() {
		return rg;
	}

	public void setRg(long rg) {
		this.rg = rg;
	}

	public long getSenha() {
		return senha;
	}

	public void setSenha(long senha) {
		this.senha = senha;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void pushEnderecos(Endereco... enderecos) {
		this.enderecos.addAll(Arrays.asList(enderecos));
	}

	public List<ContatoCliente> getContatos() {
		return contatos;
	}

	public void pushContatos(ContatoCliente... contatos) {
		this.contatos.addAll(Arrays.asList(contatos));
	}

	public List<Conta> getContas() {
		return contas;
	}

	public void pushContas(Conta... contas) {
		this.contas.addAll(Arrays.asList(contas));
	}
	
	
	
	
}
