package br.com.dbccompany.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.EmprestimoPreAprovado;
import br.com.dbccompany.Entity.SolicitacaoEmprestimo;
import br.com.dbccompany.Entity.Transacoes;
import br.com.dbccompany.Repository.TransacoesRepository;

@Service
public class TransacoesService {
	@Autowired //para injetar repository no service
	private TransacoesRepository transacoesRepository; 
	
	@Transactional(rollbackFor =  Exception.class)  //procura e ve erro
	public Transacoes salvar(Transacoes transacao) {
		return transacoesRepository.save(transacao) ;
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(long Id) {
		transacoesRepository.deleteById(Id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Transacoes editarTransacao(long Id, Transacoes transacao) {
		transacao.setId(Id);
		return transacoesRepository.save(transacao);
	}
	
	public Optional<Transacoes> buscarTransacao(long id) {
		return transacoesRepository.findById(id); 
	}
	
	public Transacoes buscarPorValor(double valor) {
		return transacoesRepository.findByValor(valor);
	}
	
	public Transacoes buscarPorPeriodo(Date periodo) {
		return transacoesRepository.findByPeriodo(periodo);
	}

	public List<Transacoes> buscarPorContaOrigem(Conta contaOrigem){
		return transacoesRepository.findByContaOrigem(contaOrigem);
	}
	
	public List<Transacoes> buscarPorContaDestino(Conta contaDestino){
		return transacoesRepository.findByContaDestino(contaDestino);
	}
	
	public Transacoes buscarPorSolicitacao(SolicitacaoEmprestimo solicitacaoEmprestimo) {
		return transacoesRepository.findBySolicitacaoEmprestimo(solicitacaoEmprestimo);
	}
	
	public Transacoes buscarPorEmprestimoPreAprovado(EmprestimoPreAprovado emprestimoPreAprovado) {
		return transacoesRepository.findByEmprestimoPreAprovado(emprestimoPreAprovado);
	}
	
	
	
}
