package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Agencia;

import br.com.dbccompany.Service.AgenciaService;

@Controller
@RequestMapping("/api/agencia")
public class AgenciaController {
	
	@Autowired
	AgenciaService agenciaService; 
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Agencia novaAgencia(@RequestBody Agencia agencia) {
		return agenciaService.salvar(agencia); 
	}
	
	@GetMapping(value="/{codigo}")
	@ResponseBody
	public Agencia agenciaEspecifica(@PathVariable long codigo) { //path: pq o codigo entre chaves é dinamico!
		return agenciaService.buscarPorCodigo(codigo); 
	}
	
	@GetMapping(value="/") 
	@ResponseBody     
	public List<Agencia> lstAgencias(){
		return agenciaService.allAgencias();
	}
	
	
	
}
