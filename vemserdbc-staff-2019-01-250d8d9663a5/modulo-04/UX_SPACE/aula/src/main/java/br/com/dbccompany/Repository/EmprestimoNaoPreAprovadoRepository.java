package br.com.dbccompany.Repository;

import java.text.DecimalFormat;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.EmprestimoNaoPreAprovado;
import br.com.dbccompany.Entity.SolicitacaoEmprestimo;

public interface EmprestimoNaoPreAprovadoRepository extends CrudRepository<EmprestimoNaoPreAprovado, Long>{

	EmprestimoNaoPreAprovado findByValor(double valor); 
	List<EmprestimoNaoPreAprovado> findByContas(Conta conta);
	EmprestimoNaoPreAprovado findBySolicitacoesEmprestimos(SolicitacaoEmprestimo solicitacaoEmprestimo);
	
	
}
