package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import br.com.dbccompany.Entity.Conta;

import br.com.dbccompany.Entity.EmprestimoPreAprovado;
import br.com.dbccompany.Entity.Transacoes;

import br.com.dbccompany.Repository.EmprestimoPreAprovadoRepository;

@Service
public class EmprestimoPreAProvadoService {
	@Autowired //para injetar repository no service
	private EmprestimoPreAprovadoRepository emprestimoPreAprovadoRepository; 
	
	@Transactional(rollbackFor =  Exception.class)  //procura e ve erro
	public EmprestimoPreAprovado salvar(EmprestimoPreAprovado emprestimoPreAprovado) {
		return emprestimoPreAprovadoRepository.save(emprestimoPreAprovado) ;
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(long Id) {
		emprestimoPreAprovadoRepository.deleteById(Id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public EmprestimoPreAprovado editarEmprestimo(long Id, EmprestimoPreAprovado emprestimoPreAprovado) {
		emprestimoPreAprovado.setId(Id);
		return emprestimoPreAprovadoRepository.save(emprestimoPreAprovado);
	}
	
	public Optional<EmprestimoPreAprovado> buscarEmprestimo(long id) {
		return emprestimoPreAprovadoRepository.findById(id); 
	}
	
	public EmprestimoPreAprovado buscarPorValorCredito(double valorCredito) {
		return emprestimoPreAprovadoRepository.findByValorCredito(valorCredito); 
	}
	
	public EmprestimoPreAprovado buscarPorTipoCredito(String tipoCredito) {
		return emprestimoPreAprovadoRepository.findByTipoCredito(tipoCredito);
	}
	
	public List<EmprestimoPreAprovado> buscarPorConta(Conta conta){
		return emprestimoPreAprovadoRepository.findByContas(conta);
	}
	
	public EmprestimoPreAprovado buscarPorTransacao(Transacoes transacao) {
		return emprestimoPreAprovadoRepository.findByTransacao(transacao); 
	}
	
	
}
