package br.com.dbccompany.Repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.EmprestimoPreAprovado;
import br.com.dbccompany.Entity.SolicitacaoEmprestimo;
import br.com.dbccompany.Entity.Transacoes;

public interface TransacoesRepository extends CrudRepository<Transacoes, Long> {

	Transacoes findByValor(double valor); 
	Transacoes findByPeriodo(Date periodo); 
	List<Transacoes> findByContaOrigem(Conta contaOrigem); 
	List<Transacoes> findByContaDestino(Conta contaDestino); 
	Transacoes findBySolicitacaoEmprestimo(SolicitacaoEmprestimo solicitacaoEmprestimo);
	Transacoes findByEmprestimoPreAprovado(EmprestimoPreAprovado emprestimoPreAprovado);
	
	
	
}
