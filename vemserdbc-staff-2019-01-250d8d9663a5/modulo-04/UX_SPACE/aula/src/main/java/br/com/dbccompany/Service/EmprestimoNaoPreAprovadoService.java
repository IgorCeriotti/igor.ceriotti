package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.EmprestimoNaoPreAprovado;
import br.com.dbccompany.Entity.SolicitacaoEmprestimo;
import br.com.dbccompany.Repository.EmprestimoNaoPreAprovadoRepository;


@Service
public class EmprestimoNaoPreAprovadoService {

	@Autowired
	private EmprestimoNaoPreAprovadoRepository emprestimoNaoPreAprovadoRepository; 
	
	@Transactional(rollbackFor = Exception.class)
	public EmprestimoNaoPreAprovado salvar(EmprestimoNaoPreAprovado emprestimoNaoPreAprovado) {
		return emprestimoNaoPreAprovadoRepository.save(emprestimoNaoPreAprovado); 
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(long Id) {
		emprestimoNaoPreAprovadoRepository.deleteById(Id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public EmprestimoNaoPreAprovado editarEmprestimo(long Id, EmprestimoNaoPreAprovado emprestimoNaoPreAprovado) {
		emprestimoNaoPreAprovado.setId(Id);
		return emprestimoNaoPreAprovadoRepository.save(emprestimoNaoPreAprovado);
	}
	
	public Optional<EmprestimoNaoPreAprovado> buscarEmprestimo(long id){
		return emprestimoNaoPreAprovadoRepository.findById(id); 
	}
	
	public EmprestimoNaoPreAprovado buscarPorValor(double valor) {
		return emprestimoNaoPreAprovadoRepository.findByValor(valor);
	}
	
	public List<EmprestimoNaoPreAprovado> buscarPorConta(Conta conta){
		return emprestimoNaoPreAprovadoRepository.findByContas(conta);
	}
	
	public EmprestimoNaoPreAprovado buscarPorSolicitacao(SolicitacaoEmprestimo solicitacaoEmprestimo) {
		return emprestimoNaoPreAprovadoRepository.findBySolicitacoesEmprestimos(solicitacaoEmprestimo);
	}
	
	
	
}
