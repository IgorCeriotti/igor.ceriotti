package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Decisor;
import br.com.dbccompany.Entity.EmprestimoNaoPreAprovado;
import br.com.dbccompany.Entity.SolicitacaoEmprestimo;
import br.com.dbccompany.Entity.Transacoes;

public interface SolicitacaoEmprestimoRepository extends CrudRepository<SolicitacaoEmprestimo, Long>{

	SolicitacaoEmprestimo findByEmprestimoNaoPreAprovado(EmprestimoNaoPreAprovado emprestimoNaoPreAprovado); 
	List<SolicitacaoEmprestimo> findByDecisor(Decisor decisor); 
	SolicitacaoEmprestimo findByDecisao(String decisao); 
	SolicitacaoEmprestimo findByValorAprovado(double valorAprovado);
	SolicitacaoEmprestimo findByTransacao(Transacoes transacao); 
	
	
}
