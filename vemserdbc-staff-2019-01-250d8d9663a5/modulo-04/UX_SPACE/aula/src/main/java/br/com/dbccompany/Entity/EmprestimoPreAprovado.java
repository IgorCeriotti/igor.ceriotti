package br.com.dbccompany.Entity;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class EmprestimoPreAprovado {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column (name = "VALOR_CREDITO", nullable = false )
	private double valorCredito; 
	
	@Column (name = "TIPO_CREDITO", nullable = false )
	private String tipoCredito; 
	
	@ManyToOne
	@JoinColumn ( name = "id_lista_contas_do_cliente", nullable = false )
	private Conta contas; 
	
	@OneToOne ( mappedBy = "emprestimoPreAprovado" )
	private Transacoes transacao;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getValorCredito() {
		return valorCredito;
	}

	public void setValorCredito(double valorCredito) {
		this.valorCredito = valorCredito;
	}

	public String getTipoCredito() {
		return tipoCredito;
	}

	public void setTipoCredito(String tipoCredito) {
		this.tipoCredito = tipoCredito;
	}

	public Conta getContas() {
		return contas;
	}

	public void setContas(Conta contas) {
		this.contas = contas;
	}

	public Transacoes getTransacao() {
		return transacao;
	}

	public void setTransacao(Transacoes transacao) {
		this.transacao = transacao;
	} 
	
	
	
}
