package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.ContatoCliente;

public interface ContatoClienteRepository extends CrudRepository<ContatoCliente, Long> {
	
	ContatoCliente findByTelefoneResidencial(long telefoneResidencial); 
	ContatoCliente findByCelular(long celular);
	ContatoCliente findByEmail(String email); 
	List<ContatoCliente> findByClientes(Cliente cliente); 
	
	
}
