package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.ContatoCliente;
import br.com.dbccompany.Repository.ContatoClienteRepository;


@Service
public class ContatoClienteService {

	@Autowired
	private ContatoClienteRepository contatoClienteRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public ContatoCliente salvar(ContatoCliente contatoCliente) {
		return contatoClienteRepository.save(contatoCliente);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(long Id) {
		contatoClienteRepository.deleteById(Id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public ContatoCliente editarContato(long Id, ContatoCliente contatoCliente) {
		contatoCliente.setId(Id);
		return contatoClienteRepository.save(contatoCliente);
	}
	
	public Optional<ContatoCliente> buscarContato(long id){
		return contatoClienteRepository.findById(id);
	}
	
	public ContatoCliente buscarPorTelefoneResidencial(long telefoneResidencial) {
		return contatoClienteRepository.findByTelefoneResidencial(telefoneResidencial);
	}
	
	public ContatoCliente buscarPorCelular(long celular) {
		return contatoClienteRepository.findByCelular(celular);
	}
	
	public ContatoCliente buscarPorEmail(String email) {
		return contatoClienteRepository.findByEmail(email);
	}
	
	public List<ContatoCliente> buscarPorCliente(Cliente cliente){
		return contatoClienteRepository.findByClientes(cliente);
	}
	
	
	
}
