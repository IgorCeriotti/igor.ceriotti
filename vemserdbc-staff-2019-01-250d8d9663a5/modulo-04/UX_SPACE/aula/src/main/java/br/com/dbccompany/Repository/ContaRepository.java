package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.CreditoPreAprovado;
import br.com.dbccompany.Entity.EmprestimoNaoPreAprovado;
import br.com.dbccompany.Entity.EmprestimoPreAprovado;
import br.com.dbccompany.Entity.TipoConta;
import br.com.dbccompany.Entity.Transacoes;

public interface ContaRepository extends CrudRepository<Conta, Long> {
	
	Conta findByNumero(Integer numero); 
	List<Conta> findByTipoConta(TipoConta tipoConta);
	Conta findByCreditosPreAprovados(CreditoPreAprovado creditoPreAprovado); 
	List<Conta> findByClientes(Cliente cliente);
	Conta findByEmprestimosPreAprovados(EmprestimoPreAprovado emprestimoPreAprovado); 
	Conta findByEmprestimosNaoPreAprovados(EmprestimoNaoPreAprovado emprestimoNaoPreAprovado); 
	Conta findByTransacaoOrigem(Transacoes transacaoOrigem); 
	Conta findByTransacaoDestino(Transacoes transacaoDestino); 
	
	
	
}
