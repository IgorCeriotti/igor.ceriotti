package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


@Entity
public class Cargo {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id; 
	
	private String nome;
	
	
	@OneToMany (mappedBy = "cargo")
	private java.util.List<Usuario> usuarios = new ArrayList<>();
	
	@OneToOne ( mappedBy = "cargo" )
	private Decisor decisor; 

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public java.util.List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void pushUsuarios(Usuario... usuarios) {
		this.usuarios.addAll(Arrays.asList(usuarios));
	}

	public Decisor getDecisor() {
		return decisor;
	}

	public void setDecisor(Decisor decisor) {
		this.decisor = decisor;
	} 
	
	
	
	
}
