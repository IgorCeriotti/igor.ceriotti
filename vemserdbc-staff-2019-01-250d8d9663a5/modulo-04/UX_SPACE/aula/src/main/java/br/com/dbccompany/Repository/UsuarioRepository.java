package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Cargo;
import br.com.dbccompany.Entity.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

	Usuario findByNome(String nome); 
	List<Usuario> findByCargo(Cargo cargo);
	
	
}
