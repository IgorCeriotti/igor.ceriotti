package br.com.dbccompany.Repository;

import java.text.DecimalFormat;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.CreditoPreAprovado;

public interface CreditoPreAprovadoRepository extends CrudRepository<CreditoPreAprovado, Long> {

	 
	CreditoPreAprovado findByValor(double valor); 
	List<CreditoPreAprovado> findByConta(Conta conta); 
	
	
}
