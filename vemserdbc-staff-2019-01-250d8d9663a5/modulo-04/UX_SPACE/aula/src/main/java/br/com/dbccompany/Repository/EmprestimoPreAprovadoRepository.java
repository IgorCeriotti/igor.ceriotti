package br.com.dbccompany.Repository;


import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.EmprestimoPreAprovado;
import br.com.dbccompany.Entity.Transacoes;

public interface EmprestimoPreAprovadoRepository extends CrudRepository<EmprestimoPreAprovado, Long> {

	EmprestimoPreAprovado findByValorCredito(double valorCredito); 
	EmprestimoPreAprovado findByTipoCredito(String tipoCredito); 
	List<EmprestimoPreAprovado> findByContas(Conta conta); 
	EmprestimoPreAprovado findByTransacao(Transacoes transacao); 
	
	
}
