package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Decisor;
import br.com.dbccompany.Entity.EmprestimoNaoPreAprovado;

import br.com.dbccompany.Entity.SolicitacaoEmprestimo;
import br.com.dbccompany.Entity.Transacoes;

import br.com.dbccompany.Repository.SolicitacaoEmprestimoRepository;

@Service
public class SolicitacaoEmprestimoService {
	@Autowired //para injetar repository no service
	private SolicitacaoEmprestimoRepository solicitacaoEmprestimoRepository; 
	
	@Transactional(rollbackFor =  Exception.class)  //procura e ve erro
	public SolicitacaoEmprestimo salvar(SolicitacaoEmprestimo solicitacaoEmprestimo) {
		return solicitacaoEmprestimoRepository.save(solicitacaoEmprestimo) ;
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(long Id) {
		solicitacaoEmprestimoRepository.deleteById(Id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public SolicitacaoEmprestimo editarDecisor(long Id, SolicitacaoEmprestimo solicitacaoEmprestimo) {
		solicitacaoEmprestimo.setId(Id);
		return solicitacaoEmprestimoRepository.save(solicitacaoEmprestimo);
	}
	
	public Optional<SolicitacaoEmprestimo> buscarSolicitacao(long id) {
		return solicitacaoEmprestimoRepository.findById(id); 
	}
	
	public SolicitacaoEmprestimo buscarPorEmprestimoNaoAprovado(EmprestimoNaoPreAprovado emprestimoNaoPreAprovado) {
		return solicitacaoEmprestimoRepository.findByEmprestimoNaoPreAprovado(emprestimoNaoPreAprovado);
	}
	
	public List<SolicitacaoEmprestimo> buscarPorDecisor(Decisor decisor){
		return solicitacaoEmprestimoRepository.findByDecisor(decisor);
	}
	
	public SolicitacaoEmprestimo buscarPorDecisao(String decisao) {
		return solicitacaoEmprestimoRepository.findByDecisao(decisao);
	}
	
	public SolicitacaoEmprestimo buscarPorValorAprovado(double valorAprovado) {
		return solicitacaoEmprestimoRepository.findByValorAprovado(valorAprovado);
	}
	
	public SolicitacaoEmprestimo buscarPorTransacao(Transacoes transacao) {
		return solicitacaoEmprestimoRepository.findByTransacao(transacao);
	}
	
	
}
