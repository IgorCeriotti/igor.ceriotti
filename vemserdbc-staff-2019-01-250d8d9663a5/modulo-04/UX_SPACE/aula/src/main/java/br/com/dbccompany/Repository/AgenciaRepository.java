package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Entity.Endereco;

public interface AgenciaRepository extends CrudRepository<Agencia, Long> {
	
	Agencia findByCodigo(long codigo);
	Agencia findByNome(String nome); 
	Agencia findByEndereco(Endereco endereco); 
	List<Agencia> findByBanco(Banco banco); 
	
	
	
	
}
