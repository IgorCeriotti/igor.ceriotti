package br.com.dbccompany.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.TipoConta;

import br.com.dbccompany.Repository.TipoContaRepository;


@Service
public class TipoContaService {
	@Autowired //para injetar repository no service
	private TipoContaRepository tipoContaRepository; 
	
	@Transactional(rollbackFor =  Exception.class)  //procura e ve erro
	public TipoConta salvar(TipoConta tipoConta) {
		return tipoContaRepository.save(tipoConta) ;
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(long Id) {
		tipoContaRepository.deleteById(Id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public TipoConta editarConta(long Id, TipoConta tipoConta) {
		tipoConta.setId(Id);
		return tipoContaRepository.save(tipoConta);
	}
	
	public Optional<TipoConta> buscarConta(long id) {
		return tipoContaRepository.findById(id); 
	}
	
	public TipoConta buscarPorTipo(String tipo) {
		return tipoContaRepository.findByTipo(tipo);
	}
	
	public TipoConta buscarPorConta(Conta conta) {
		return tipoContaRepository.findByContas(conta);
	}
	
	
}
