package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.ContatoCliente;
import br.com.dbccompany.Entity.Endereco;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {
	
	Cliente findByNome(String nome); 
	Cliente findByCpf(long cpf); 
	Cliente findByRg(long rg);
	Cliente findBySenha(long senha); 
	List<Cliente> findByEnderecos(Endereco endereco);
	List<Cliente> findByContatos(ContatoCliente contato);
	List<Cliente> findByContas(Conta conta); 
}
