package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Cargo;

import br.com.dbccompany.Entity.Usuario;

import br.com.dbccompany.Repository.UsuarioRepository;

@Service
public class UsuarioService {
	@Autowired //para injetar repository no service
	private UsuarioRepository usuarioRepository; 
	
	@Transactional(rollbackFor =  Exception.class)  //procura e ve erro
	public Usuario salvar(Usuario usuario) {
		return usuarioRepository.save(usuario) ;
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(long Id) {
		usuarioRepository.deleteById(Id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Usuario editarUsuario(long Id, Usuario usuario) {
		usuario.setId(Id);
		return usuarioRepository.save(usuario);
	}
	
	public Optional<Usuario> buscarUsuario(long id) {
		return usuarioRepository.findById(id); 
	}
	
	public Usuario buscarPorNome(String nome) {
		return usuarioRepository.findByNome(nome);
	}
	
	public List<Usuario> buscarPorCargo(Cargo cargo){
		return usuarioRepository.findByCargo(cargo);
	}
}
