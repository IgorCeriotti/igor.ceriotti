package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Cargo;
import br.com.dbccompany.Entity.Decisor;
import br.com.dbccompany.Entity.Usuario;

public interface CargoRepository extends CrudRepository<Cargo, Long> {

	Cargo findByNome(String nome); 
	
	Cargo findByUsuarios(Usuario usuario); 
	
	Cargo findByDecisor(Decisor decisor); 
	
}
