package br.com.dbccompany.Entity;

import java.sql.Date;
import java.text.DecimalFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Transacoes {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column( name = "VALOR", nullable = false )
	private double valor; 
	
	@Column( name = "PERIODO", nullable = false )
	private Date periodo; 
	
	@ManyToOne
	@JoinColumn( name = "id_conta_origem", nullable = false )
	private Conta contaOrigem; 
	
	@ManyToOne
	@JoinColumn( name = "id_conta_destino", nullable = false )
	private Conta contaDestino;
	
	@OneToOne 
	@JoinColumn( name = "id_solicitacao_emprestimo" )
	private SolicitacaoEmprestimo solicitacaoEmprestimo; 
	
	@OneToOne 
	@JoinColumn( name = "id_emprestimo_pre_aprovado" )
	private EmprestimoPreAprovado emprestimoPreAprovado;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public Date getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Date periodo) {
		this.periodo = periodo;
	}

	public Conta getContaOrigem() {
		return contaOrigem;
	}

	public void setContaOrigem(Conta contaOrigem) {
		this.contaOrigem = contaOrigem;
	}

	public Conta getContaDestino() {
		return contaDestino;
	}

	public void setContaDestino(Conta contaDestino) {
		this.contaDestino = contaDestino;
	}

	public SolicitacaoEmprestimo getSolicitacaoEmprestimo() {
		return solicitacaoEmprestimo;
	}

	public void setSolicitacaoEmprestimo(SolicitacaoEmprestimo solicitacaoEmprestimo) {
		this.solicitacaoEmprestimo = solicitacaoEmprestimo;
	}

	public EmprestimoPreAprovado getEmprestimoPreAprovado() {
		return emprestimoPreAprovado;
	}

	public void setEmprestimoPreAprovado(EmprestimoPreAprovado emprestimoPreAprovado) {
		this.emprestimoPreAprovado = emprestimoPreAprovado;
	} 
	
	

}
