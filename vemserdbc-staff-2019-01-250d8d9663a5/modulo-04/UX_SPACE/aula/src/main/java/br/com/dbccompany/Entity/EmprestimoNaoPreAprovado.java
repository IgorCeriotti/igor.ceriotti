package br.com.dbccompany.Entity;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class EmprestimoNaoPreAprovado {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column (name = "VALOR", nullable = false )
	private double valor; 
	
	@ManyToOne
	@JoinColumn ( name = "id_lista_contas_do_cliente", nullable = false )
	private Conta contas;
	
	@OneToMany ( mappedBy = "emprestimoNaoPreAprovado" )
	private List<SolicitacaoEmprestimo> solicitacoesEmprestimos = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public List<SolicitacaoEmprestimo> getSolicitacoesEmprestimos() {
		return solicitacoesEmprestimos;
	}

	public void pushSolicitacoesEmprestimos(SolicitacaoEmprestimo... solicitacoesEmprestimos) {
		this.solicitacoesEmprestimos.addAll(Arrays.asList(solicitacoesEmprestimos));
	}
	
	
}
