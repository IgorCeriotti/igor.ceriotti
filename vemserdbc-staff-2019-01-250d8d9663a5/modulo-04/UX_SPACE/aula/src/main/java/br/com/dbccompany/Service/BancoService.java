package br.com.dbccompany.Service;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Repository.BancoRepository;

@Service
public class BancoService {

	@Autowired //para injetar repository no service
	private BancoRepository bancoRepository; 
	
	@Transactional(rollbackFor =  Exception.class)  //procura e ve erro
	public Banco salvar(Banco banco) {
		return bancoRepository.save(banco); 
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(long Id) {
		 bancoRepository.deleteById(Id);
	}
	
	public Optional<Banco> buscarBanco(long id) {
		return bancoRepository.findById(id); 
	}
	
	public List<Banco> allBancos(){
		return (List<Banco>) bancoRepository.findAll();
	}
	
	public Banco buscarBancoPorCodigo(long codigo) {
		return bancoRepository.findByCodigo(codigo); 
	}
	
	public Banco buscarBancoPorNome(String nome) {
		return bancoRepository.findByNome(nome);
	}
	
	public Banco buscarBancoPorAgencia(Agencia agencia) {
		return bancoRepository.findByAgencias(agencia); 
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Banco editarBanco(long Id, Banco banco) {
		banco.setId(Id);
		return bancoRepository.save(banco); 
	}
}
