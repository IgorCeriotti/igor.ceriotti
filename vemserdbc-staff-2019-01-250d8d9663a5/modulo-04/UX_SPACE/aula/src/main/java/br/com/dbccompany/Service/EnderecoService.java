package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.Cliente;

import br.com.dbccompany.Entity.Endereco;

import br.com.dbccompany.Repository.EnderecoRepository;

@Service
public class EnderecoService {

	@Autowired //para injetar repository no service
	private EnderecoRepository enderecoRepository; 
	
	@Transactional(rollbackFor =  Exception.class)  //procura e ve erro
	public Endereco salvar(Endereco endereco) {
		return enderecoRepository.save(endereco) ;
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(long Id) {
		enderecoRepository.deleteById(Id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Endereco editarDecisor(long Id, Endereco endereco) {
		endereco.setId(Id);
		return enderecoRepository.save(endereco);
	}
	
	public Optional<Endereco> buscarEmprestimo(long id) {
		return enderecoRepository.findById(id); 
	}
	
	public Endereco buscarPorLogradouro(String logradouro) {
		return enderecoRepository.findByLogradouro(logradouro);
	}
	
	public Endereco buscarPorNumero(Integer numero) {
		return enderecoRepository.findByNumero(numero);
	}
	
	public Endereco buscarPorComplemento(String complemento) {
		return enderecoRepository.findByComplemento(complemento);
	}
	
	public Endereco buscarPorBairro(String bairro) {
		return enderecoRepository.findByBairro(bairro);
	}
	
	public Endereco buscarPorCidade(String cidade) {
		return enderecoRepository.findByCidade(cidade);
	}
	
	public Endereco buscarPorAgencia(Agencia agencia) {
		return enderecoRepository.findByAgencia(agencia);
	}
	
	public List<Endereco> buscarPorCliente(Cliente cliente){
		return enderecoRepository.findByClientes(cliente);
	}
}
