package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Conta {
	@Id
	@GeneratedValue( strategy = GenerationType.SEQUENCE )
	private long id; 
	
	@Column( name = "NUMERO", nullable = false )
	private Integer numero; 
	
	
	@ManyToOne
	@JoinColumn ( name = "tipo_conta" )
	private TipoConta tipoConta;

	@OneToMany ( mappedBy = "conta" )
	private List<CreditoPreAprovado> creditosPreAprovados = new ArrayList<>();
	
	@ManyToMany()
    @JoinTable(name = "lista_contas_do_cliente",
            joinColumns = {
            	@JoinColumn(name = "id_conta")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_cliente")})
    private List<Cliente> clientes = new ArrayList<>();
	
	@OneToMany ( mappedBy = "contas" )
	private List<EmprestimoPreAprovado> emprestimosPreAprovados = new ArrayList<>();
	
	@OneToMany ( mappedBy = "contas" )
	private List<EmprestimoNaoPreAprovado> emprestimosNaoPreAprovados = new ArrayList<>();
	
	@OneToMany ( mappedBy = "contaOrigem" )
	private List<Transacoes> transacaoOrigem = new ArrayList<>();
	
	@OneToMany ( mappedBy = "contaDestino" )
	private List<Transacoes> transacaoDestino = new ArrayList<>();
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public TipoConta getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(TipoConta tipoConta) {
		this.tipoConta = tipoConta;
	}

	public List<CreditoPreAprovado> getCreditosPreAprovados() {
		return creditosPreAprovados;
	}

	public void pushCreditosPreAprovados(CreditoPreAprovado... creditosPreAprovados) {
		this.creditosPreAprovados.addAll(Arrays.asList(creditosPreAprovados));
	}

	public List<Cliente> getClientes() {
		return clientes;
	}

	public void pushClientes(Cliente... clientes) {
		this.clientes.addAll(Arrays.asList(clientes));
	}

	public List<EmprestimoPreAprovado> getEmprestimosPreAprovados() {
		return emprestimosPreAprovados;
	}

	public void pushEmprestimosPreAprovados(EmprestimoPreAprovado... emprestimosPreAprovados) {
		this.emprestimosPreAprovados.addAll(Arrays.asList(emprestimosPreAprovados));
	}

	public List<EmprestimoNaoPreAprovado> getEmprestimosNaoPreAprovados() {
		return emprestimosNaoPreAprovados;
	}

	public void pushEmprestimosNaoPreAprovados(EmprestimoNaoPreAprovado... emprestimosNaoPreAprovados) {
		this.emprestimosNaoPreAprovados.addAll(Arrays.asList(emprestimosNaoPreAprovados));
	}

	public List<Transacoes> geTransacaoOrigem() {
		return transacaoOrigem;
	}

	public void pushTransacaoOrigem(Transacoes... transacaoOrigem) {
		this.transacaoOrigem.addAll(Arrays.asList(transacaoOrigem));
	}

	public List<Transacoes> getTransacaoDestino() {
		return transacaoDestino;
	}

	public void pushTransacaoDestino(Transacoes... transacaoDestino) {
		this.transacaoDestino.addAll(Arrays.asList(transacaoDestino));
	} 
	
	
	
	
}
