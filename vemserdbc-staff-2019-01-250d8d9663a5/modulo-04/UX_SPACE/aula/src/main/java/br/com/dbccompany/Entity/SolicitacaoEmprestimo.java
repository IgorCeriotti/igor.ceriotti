package br.com.dbccompany.Entity;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class SolicitacaoEmprestimo {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@ManyToOne 
	@JoinColumn ( name = "id_emprestimo_nao_pre_aprovado", nullable = false )
	private EmprestimoNaoPreAprovado emprestimoNaoPreAprovado; 
	
	@ManyToOne
	@JoinColumn ( name = "id_decisor", nullable = false )
	private Decisor decisor; 
	
	@Column( name = "DECISAO", nullable = false )
	private String decisao; 
	
	@Column( name = "VALOR_APROVADO", nullable = false )
	private double valorAprovado; 
	
	@OneToOne ( mappedBy = "solicitacaoEmprestimo" )
	private Transacoes transacao;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public EmprestimoNaoPreAprovado getEmprestimoNaoPreAprovado() {
		return emprestimoNaoPreAprovado;
	}

	public void setEmprestimoNaoPreAprovado(EmprestimoNaoPreAprovado emprestimoNaoPreAprovado) {
		this.emprestimoNaoPreAprovado = emprestimoNaoPreAprovado;
	}

	public Decisor getDecisor() {
		return decisor;
	}

	public void setDecisor(Decisor decisor) {
		this.decisor = decisor;
	}

	public String getDecisao() {
		return decisao;
	}

	public void setDecisao(String decisao) {
		this.decisao = decisao;
	}

	public double getValorAprovado() {
		return valorAprovado;
	}

	public void setValorAprovado(double valorAprovado) {
		this.valorAprovado = valorAprovado;
	}

	public Transacoes getTransacao() {
		return transacao;
	}

	public void setTransacao(Transacoes transacao) {
		this.transacao = transacao;
	} 
	
	
	
	
}
