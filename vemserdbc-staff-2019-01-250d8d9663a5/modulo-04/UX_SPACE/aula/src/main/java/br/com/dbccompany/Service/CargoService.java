package br.com.dbccompany.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Cargo;
import br.com.dbccompany.Entity.Decisor;
import br.com.dbccompany.Entity.Usuario;
import br.com.dbccompany.Repository.CargoRepository;

@Service
public class CargoService {

	@Autowired
	private CargoRepository cargoRepository; 
	
	@Transactional
	public Cargo salvar(Cargo cargo) {
		return cargoRepository.save(cargo);
	}
	
	@Transactional
	public void deletar(long Id) {
		cargoRepository.deleteById(Id);
	}
	
	@Transactional
	public Cargo editarCargo(long Id, Cargo cargo) {
		cargo.setId(Id);
		return cargoRepository.save(cargo);
	}
	
	public Optional<Cargo> buscarCargo(long id){
		return cargoRepository.findById(id);
	}
	
	public Cargo buscarPorNome(String nome) {
		return cargoRepository.findByNome(nome);
	}
	
	public Cargo buscarPorUsuario(Usuario usuario) {
		return cargoRepository.findByUsuarios(usuario);
	}
	
	public Cargo buscarPorDecisor(Decisor decisor) {
		return cargoRepository.findByDecisor(decisor); 
	}
}
