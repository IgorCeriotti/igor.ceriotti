package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.ContatoCliente;
import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Repository.ClienteRepository;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository; 
	
	@Transactional(rollbackFor = Exception.class)
	public Cliente salvar(Cliente cliente) {
		return clienteRepository.save(cliente);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(long Id) {
		clienteRepository.deleteById(Id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Cliente editarCliente(long Id, Cliente cliente) {
		cliente.setId(Id);
		return clienteRepository.save(cliente); 
	}
	
	public Optional<Cliente> buscarCliente(long id){
		return clienteRepository.findById(id);
	}
	
	public Cliente buscarPorNome(String nome) {
		return clienteRepository.findByNome(nome);
	}
	
	public Cliente buscarPorCpf(long cpf) {
		return clienteRepository.findByCpf(cpf);
	}
	
	public Cliente buscarPorRg(long rg) {
		return clienteRepository.findByRg(rg);
	}
	
	public Cliente buscarPorSenha(long senha) {
		return clienteRepository.findBySenha(senha);
	}
	
	public List<Cliente> buscarPorEndereco(Endereco endereco) {
		return clienteRepository.findByEnderecos(endereco);
	}
	
	public List<Cliente> buscarPorContato(ContatoCliente contato){
		return clienteRepository.findByContatos(contato); 
	}
	
	public List<Cliente> buscarPorContas(Conta conta){
		return clienteRepository.findByContas(conta);
	}
	
	
	
	
}
