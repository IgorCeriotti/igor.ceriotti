package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class, 
		  property = "id")
public class Banco {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id; 
	
	@Column(name = "CODIGO", unique = true, nullable = false)
	private long codigo;
	
	@Column(name = "NOME")
	private String nome;
	
	@OneToMany( mappedBy = "banco" )
	private List<Agencia> agencias = new ArrayList<>(); 

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCodigo() {
		return codigo;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Agencia> getAgencias() {
		return agencias;
	}

	public void pushAgencias(Agencia... agencias) {
		this.agencias.addAll(Arrays.asList(agencias));
	}
	
	
	
	
}


