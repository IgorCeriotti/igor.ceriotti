package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.CreditoPreAprovado;
import br.com.dbccompany.Entity.EmprestimoNaoPreAprovado;
import br.com.dbccompany.Entity.EmprestimoPreAprovado;
import br.com.dbccompany.Entity.TipoConta;
import br.com.dbccompany.Entity.Transacoes;
import br.com.dbccompany.Repository.ContaRepository;

@Service
public class ContaService {

	@Autowired
	private ContaRepository contaRepository; 
	
	@Transactional(rollbackFor = Exception.class)
	public Conta salvar(Conta conta) {
		return contaRepository.save(conta);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(long Id) {
		contaRepository.deleteById(Id);
	}
	
	@Transactional(rollbackFor = Exception.class)
		public Conta editarConta(long Id, Conta conta) {
			conta.setId(Id);
			return contaRepository.save(conta); 
		}
	
	public Optional<Conta> buscarConta(long id){
		return contaRepository.findById(id); 
	}
	
	public Conta buscarPorNumero(Integer numero) {
		return contaRepository.findByNumero(numero);
	}
	
	public List<Conta> buscarPorTipo(TipoConta tipoConta){
		return contaRepository.findByTipoConta(tipoConta);
	}
	
	public Conta buscarPorCreditoPreAprovado(CreditoPreAprovado creditoPreAprovado) {
		return contaRepository.findByCreditosPreAprovados(creditoPreAprovado);
	}
	
	public List<Conta> buscarPorCliente(Cliente cliente){
		return contaRepository.findByClientes(cliente);
	}
	
	public Conta buscarPorEmprestimoPreAprovado(EmprestimoPreAprovado emprestimoPreAprovado) {
		return contaRepository.findByEmprestimosPreAprovados(emprestimoPreAprovado);
	}
	
	public Conta buscarPorEmprestimoNaoPreAprovado(EmprestimoNaoPreAprovado emprestimoNaoPreAprovado) {
		return contaRepository.findByEmprestimosNaoPreAprovados(emprestimoNaoPreAprovado);
	}
	
	public Conta buscarPorTransacaoOrigem(Transacoes transacaoOrigem) {
		return contaRepository.findByTransacaoOrigem(transacaoOrigem);
	}
	
	public Conta buscarPorTransacaoDestino(Transacoes transacaoDestino) {
		return contaRepository.findByTransacaoDestino(transacaoDestino);
	}
	
	
}
