package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.CreditoPreAprovado;
import br.com.dbccompany.Repository.CreditoPreAprovadoRepository;

@Service
public class CreditoPreAprovadoService {

	@Autowired
	private CreditoPreAprovadoRepository creditoPreAprovadoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public CreditoPreAprovado salvar(CreditoPreAprovado creditoPreAprovado) {
		return creditoPreAprovadoRepository.save(creditoPreAprovado);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(long Id) {
		creditoPreAprovadoRepository.deleteById(Id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public CreditoPreAprovado editarCredito(long Id, CreditoPreAprovado creditoPreAprovado) {
		creditoPreAprovado.setId(Id);
		return creditoPreAprovadoRepository.save(creditoPreAprovado);
	}
	
	public Optional<CreditoPreAprovado> buscarCredito(long id){
		return creditoPreAprovadoRepository.findById(id);
	}
	
	
	
	public CreditoPreAprovado buscarPorValor(double valor) {
		return creditoPreAprovadoRepository.findByValor(valor);
	}
	
	public List<CreditoPreAprovado> buscarPorConta(Conta conta){
		return creditoPreAprovadoRepository.findByConta(conta);
	}
	
}
