package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Repository.AgenciaRepository;

@Service
public class AgenciaService {

	@Autowired
	private AgenciaRepository agenciaRepository; 
	
	@Transactional(rollbackFor = Exception.class)
	public Agencia salvar(Agencia agencia) {
		return agenciaRepository.save(agencia); 
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(long Id) {
		agenciaRepository.deleteById(Id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Agencia editarAgencia(long Id, Agencia agencia) {
		agencia.setId(Id);
		return agenciaRepository.save(agencia);
	}
	
	public Optional<Agencia> buscarAgencia(long id){
		return agenciaRepository.findById(id); 
	}
	
	public List<Agencia> allAgencias() {
		return (List<Agencia>) agenciaRepository.findAll();
	}
	
	public Agencia buscarPorCodigo(long codigo) {
		return agenciaRepository.findByCodigo(codigo); 
	}
	
	public Agencia buscarPorNome(String nome) {
		return agenciaRepository.findByNome(nome);
	}
	
	public Agencia buscarPorEndereco(Endereco endereco) {
		return agenciaRepository.findByEndereco(endereco);
	}
	
	public List<Agencia> buscarPorBanco(Banco banco){
		return agenciaRepository.findByBanco(banco); 
	}
	
	
	
}
