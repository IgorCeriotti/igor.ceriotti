package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class ContatoCliente {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column( name = "TELEFONE_RESIDENCIAL" )
	private long telefoneResidencial; 
	
	@Column( name = "CELULAR", nullable = false )
	private long celular; 
	
	@Column( name = "EMAIL", nullable = false )
	private String email;
	
	@ManyToMany(mappedBy = "contatos")
    private List<Cliente> clientes= new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getTelefoneResidencial() {
		return telefoneResidencial;
	}

	public void setTelefoneResidencial(long telefoneResidencial) {
		this.telefoneResidencial = telefoneResidencial;
	}

	public long getCelular() {
		return celular;
	}

	public void setCelular(long celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Cliente> getClientes() {
		return clientes;
	}

	public void pushClientes(Cliente... clientes) {
		this.clientes.addAll(Arrays.asList(clientes));
	} 
	
	



}
