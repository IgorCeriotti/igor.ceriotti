package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.TipoConta;

public interface TipoContaRepository extends CrudRepository<TipoConta, Long> {

	TipoConta findByTipo(String tipo);
	TipoConta findByContas(Conta conta); 
	
	
}
