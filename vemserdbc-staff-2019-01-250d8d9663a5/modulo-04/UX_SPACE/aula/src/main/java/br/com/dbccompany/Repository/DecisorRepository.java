package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Cargo;
import br.com.dbccompany.Entity.Decisor;
import br.com.dbccompany.Entity.SolicitacaoEmprestimo;

public interface DecisorRepository extends CrudRepository<Decisor, Long>{

	Decisor findByRegras(String regras); 
	Decisor findByCargo(Cargo cargo);
	Decisor findBySolicitacoesEmprestimos(SolicitacaoEmprestimo solicitacaoEmprestimo);
	
}
