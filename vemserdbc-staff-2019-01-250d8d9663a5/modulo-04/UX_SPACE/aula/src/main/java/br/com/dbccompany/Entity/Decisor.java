package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Decisor {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column( name = "REGRAS", nullable = false )
	private String regras;
	
	@OneToOne
	@JoinColumn ( name = "id_cargo", nullable = false )
	private Cargo cargo;
	
	@OneToMany ( mappedBy = "decisor" )
	private List<SolicitacaoEmprestimo> solicitacoesEmprestimos = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRegras() {
		return regras;
	}

	public void setRegras(String regras) {
		this.regras = regras;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public List<SolicitacaoEmprestimo> getSolicitacoesEmprestimos() {
		return solicitacoesEmprestimos;
	}

	public void pushSolicitacoesEmprestimos(SolicitacaoEmprestimo... solicitacoesEmprestimos) {
		this.solicitacoesEmprestimos.addAll(Arrays.asList(solicitacoesEmprestimos));
	} 
	
	
}
