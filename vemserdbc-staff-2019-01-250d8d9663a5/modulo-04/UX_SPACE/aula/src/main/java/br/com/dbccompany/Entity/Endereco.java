package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

@Entity
public class Endereco {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column( name = "LOGRADOURO", nullable = false )
	private String logradouro; 
	
	@Column( name = "NUMERO", nullable = false )
	private Integer numero; 
	
	@Column( name = "COMPLEMENTO", nullable = false )
	private String complemento; 
	
	@Column( name = "BAIRRO", nullable = false )
	private String bairro; 
	
	@Column( name = "CIDADE", nullable = false )
	private String cidade; 
	
	
	@OneToOne 
	@JoinColumn( name = "id_agencia" )
	private Agencia agencia; 
	
	@ManyToMany(mappedBy = "enderecos")
    private List<Cliente> clientes = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public Agencia getAgencia() {
		return agencia;
	}

	public void setAgencia(Agencia agencia) {
		this.agencia = agencia;
	}

	public List<Cliente> getClientes() {
		return clientes;
	}

	public void pushClientes(Cliente... clientes) {
		this.clientes.addAll(Arrays.asList(clientes));
	}
	
	
}
