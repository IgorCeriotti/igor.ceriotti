/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.service;

import br.com.dbc.lotr.dto.PersonagemDTO;
import br.com.dbc.lotr.entity.HibernateUtil;
import br.com.dbc.lotr.entity.PersonagemJoin;
import br.com.dbc.lotr.entity.RacaType;
import br.com.dbc.lotr.entity.Usuario;
import java.util.List;
import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author igor.ceriotti
 */
public class PersonagemServiceTest {

    public PersonagemServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of salvarPersonagem method, of class PersonagemService.
     */
    @Test
    public void testSalvarPersonagem() {

        System.out.println("salvarPersonagem");
        PersonagemDTO personagemDTO = new PersonagemDTO();
        personagemDTO.setNome("Elfo da Luz");
        personagemDTO.setRaca(RacaType.ELFO);
        personagemDTO.setDanoElfo(123d);
        Usuario usuario = new Usuario();
        usuario.setNome("User Elfico");
        usuario.setApelido("Elfinho");
        usuario.setSenha("123");
        usuario.setCpf(123l);
        Session session = HibernateUtil.getSession();
        HibernateUtil.beginTransaction();
        session.save(usuario);
        session.getTransaction().commit();

        PersonagemService instance = new PersonagemService();
        int oldsize = session.createCriteria(PersonagemJoin.class)
                .list().size();
        instance.salvarPersonagem(personagemDTO, usuario);

        final List<PersonagemJoin> personagens = session.createCriteria(PersonagemJoin.class)
                .list();
        
        Assert.assertEquals(oldsize+1, personagens.size());
        
    }

    @Test
    public void testNomePersonagem() {

        System.out.println("salvarPersonagem");
        PersonagemDTO personagemDTO = new PersonagemDTO();
        personagemDTO.setNome("Elfo da Luz");
        personagemDTO.setRaca(RacaType.ELFO);
        personagemDTO.setDanoElfo(123d);
        Usuario usuario = new Usuario();
        usuario.setNome("User Elfico");
        usuario.setApelido("Elfinho");
        usuario.setSenha("123");
        usuario.setCpf(123l);
        Session session = HibernateUtil.getSession();
        HibernateUtil.beginTransaction();
        session.save(usuario);
        session.getTransaction().commit();

        PersonagemService instance = new PersonagemService();
        instance.salvarPersonagem(personagemDTO, usuario);

        final List<PersonagemJoin> personagens = session.createCriteria(PersonagemJoin.class)
                .list();
        
        Assert.assertEquals("Elfo da Luz", personagens.get(0).getNome());
        
    }
    
    @Test
    public void testRacaPersonagem() {

        System.out.println("salvarPersonagem");
        PersonagemDTO personagemDTO = new PersonagemDTO();
        personagemDTO.setNome("Elfo da Luz");
        personagemDTO.setRaca(RacaType.ELFO);
        personagemDTO.setDanoElfo(123d);
        Usuario usuario = new Usuario();
        usuario.setNome("User Elfico");
        usuario.setApelido("Elfinho");
        usuario.setSenha("123");
        usuario.setCpf(123l);
        Session session = HibernateUtil.getSession();
        HibernateUtil.beginTransaction();
        session.save(usuario);
        session.getTransaction().commit();

        PersonagemService instance = new PersonagemService();
        instance.salvarPersonagem(personagemDTO, usuario);

        final List<PersonagemJoin> personagens = session.createCriteria(PersonagemJoin.class)
                .list();
        
        Assert.assertEquals(RacaType.ELFO, personagens.get(0).getRaca());
    }
    
    @Test
    public void testDanoPersonagem() {

        System.out.println("salvarPersonagem");
        PersonagemDTO personagemDTO = new PersonagemDTO();
        personagemDTO.setNome("Elfo da Luz");
        personagemDTO.setRaca(RacaType.ELFO);
        personagemDTO.setDanoElfo(123d);
        Usuario usuario = new Usuario();
        usuario.setNome("User Elfico");
        usuario.setApelido("Elfinho");
        usuario.setSenha("123");
        usuario.setCpf(123l);
        Session session = HibernateUtil.getSession();
        HibernateUtil.beginTransaction();
        session.save(usuario);
        session.getTransaction().commit();

        PersonagemService instance = new PersonagemService();
        instance.salvarPersonagem(personagemDTO, usuario);

        final List<PersonagemJoin> personagens = session.createCriteria(PersonagemJoin.class)
                .list();
        
        Assert.assertEquals(123d, personagemDTO.getDanoElfo(), .001);
    }
      
    
    @Test
    public void testUsuario() {

        System.out.println("salvarPersonagem");
        PersonagemDTO personagemDTO = new PersonagemDTO();
        personagemDTO.setNome("Elfo da Luz");
        personagemDTO.setRaca(RacaType.ELFO);
        personagemDTO.setDanoElfo(123d);
        Usuario usuario = new Usuario();
        usuario.setNome("User Elfico");
        usuario.setApelido("Elfinho");
        usuario.setSenha("123");
        usuario.setCpf(123l);
        Session session = HibernateUtil.getSession();
        HibernateUtil.beginTransaction();
        session.save(usuario);
        session.getTransaction().commit();

        PersonagemService instance = new PersonagemService();
        int oldsize = session.createCriteria(Usuario.class)
                .list().size();

        instance.salvarPersonagem(personagemDTO, usuario);

        final List<Usuario> usuarios = session.createCriteria(Usuario.class)
                .list();
        
        Assert.assertEquals(oldsize, usuarios.size());
    }
    
    @Test
    public void testNomeUsuario() {

        System.out.println("salvarPersonagem");
        PersonagemDTO personagemDTO = new PersonagemDTO();
        personagemDTO.setNome("Elfo da Luz");
        personagemDTO.setRaca(RacaType.ELFO);
        personagemDTO.setDanoElfo(123d);
        Usuario usuario = new Usuario();
        usuario.setNome("User Elfico");
        usuario.setApelido("Elfinho");
        usuario.setSenha("123");
        usuario.setCpf(123l);
        Session session = HibernateUtil.getSession();
        HibernateUtil.beginTransaction();
        session.save(usuario);
        session.getTransaction().commit();

        PersonagemService instance = new PersonagemService();
        instance.salvarPersonagem(personagemDTO, usuario);

        final List<Usuario> usuarios = session.createCriteria(Usuario.class)
                .list();
        
        Assert.assertEquals("User Elfico", usuarios.get(0).getNome());
    }

    @Test
    public void testApelidoUsuario() {

        System.out.println("salvarPersonagem");
        PersonagemDTO personagemDTO = new PersonagemDTO();
        personagemDTO.setNome("Elfo da Luz");
        personagemDTO.setRaca(RacaType.ELFO);
        personagemDTO.setDanoElfo(123d);
        Usuario usuario = new Usuario();
        usuario.setNome("User Elfico");
        usuario.setApelido("Elfinho");
        usuario.setSenha("123");
        usuario.setCpf(123l);
        Session session = HibernateUtil.getSession();
        HibernateUtil.beginTransaction();
        session.save(usuario);
        session.getTransaction().commit();

        PersonagemService instance = new PersonagemService();
        instance.salvarPersonagem(personagemDTO, usuario);

        final List<Usuario> usuarios = session.createCriteria(Usuario.class)
                .list();
        
        Assert.assertEquals("Elfinho", usuarios.get(0).getApelido());
    }
    
    @Test
    public void testSenhaUsuario() {

        System.out.println("salvarPersonagem");
        PersonagemDTO personagemDTO = new PersonagemDTO();
        personagemDTO.setNome("Elfo da Luz");
        personagemDTO.setRaca(RacaType.ELFO);
        personagemDTO.setDanoElfo(123d);
        Usuario usuario = new Usuario();
        usuario.setNome("User Elfico");
        usuario.setApelido("Elfinho");
        usuario.setSenha("123");
        usuario.setCpf(123l);
        Session session = HibernateUtil.getSession();
        HibernateUtil.beginTransaction();
        session.save(usuario);
        session.getTransaction().commit();

        PersonagemService instance = new PersonagemService();
        instance.salvarPersonagem(personagemDTO, usuario);

        final List<Usuario> usuarios = session.createCriteria(Usuario.class)
                .list();
        
        Assert.assertEquals("123", usuarios.get(0).getSenha());
    }
    
   @Test
    public void testCpfUsuario() {

        System.out.println("salvarPersonagem");
        PersonagemDTO personagemDTO = new PersonagemDTO();
        personagemDTO.setNome("Elfo da Luz");
        personagemDTO.setRaca(RacaType.ELFO);
        personagemDTO.setDanoElfo(123d);
        Usuario usuario = new Usuario();
        usuario.setNome("User Elfico");
        usuario.setApelido("Elfinho");
        usuario.setSenha("123");
        usuario.setCpf(123l);
        Session session = HibernateUtil.getSession();
        HibernateUtil.beginTransaction();
        session.save(usuario);
        session.getTransaction().commit();

        PersonagemService instance = new PersonagemService();
        instance.salvarPersonagem(personagemDTO, usuario);

        final List<Usuario> usuarios = session.createCriteria(Usuario.class)
                .list();
        
        Assert.assertEquals(123l, usuarios.get(0).getCpf(), .001);
    }
    
}
