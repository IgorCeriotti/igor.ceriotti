/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.dto;

import javax.persistence.Column;

/**
 *
 * @author igor.ceriotti
 */
public class UsuarioPersonagemDTO {
    private Integer idUsuario;

    
    private String nomeUsuario;

    private String apelidoUsuario;

    private String senhaUsuario;

    private Long cpfUsuario;
    
    //Personagem
    PersonagemDTO personagem;
    
    //Endereco
    EnderecoDTO endereco; 
    

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getApelidoUsuario() {
        return apelidoUsuario;
    }

    public void setApelidoUsuario(String apelidoUsuario) {
        this.apelidoUsuario = apelidoUsuario;
    }

    public String getSenhaUsuario() {
        return senhaUsuario;
    }

    public void setSenhaUsuario(String senhaUsuario) {
        this.senhaUsuario = senhaUsuario;
    }

    public Long getCpfUsuario() {
        return cpfUsuario;
    }

    public void setCpfUsuario(Long cpfUsuario) {
        this.cpfUsuario = cpfUsuario;
    }

    public PersonagemDTO getPersonagem() {
        return personagem;
    }

    public void setPersonagem(PersonagemDTO personagem) {
        this.personagem = personagem;
    }

    public EnderecoDTO getEndereco() {
        return endereco;
    }

    public void setEndereco(EnderecoDTO endereco) {
        this.endereco = endereco;
    }
    
    
    
    
}
