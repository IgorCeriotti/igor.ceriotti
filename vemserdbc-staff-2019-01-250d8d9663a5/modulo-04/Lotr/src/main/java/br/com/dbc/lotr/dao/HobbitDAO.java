/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.dao;

import br.com.dbc.lotr.entity.ElfoJoin;
import br.com.dbc.lotr.entity.HobbitJoin;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.dto.PersonagemDTO;
import br.com.dbc.lotr.dto.UsuarioPersonagemDTO;



/**
 *
 * @author igor.ceriotti
 */
public class HobbitDAO extends AbsctractDAO<HobbitJoin>{

    public HobbitJoin parseFrom(PersonagemDTO dto, Usuario usuario) {
        
        HobbitJoin hobbitEntity = null;
        
        if (dto.getId() != null) {
            hobbitEntity = buscar(dto.getId());
        }
        if (hobbitEntity == null) {
            hobbitEntity = new HobbitJoin();
        }
        
        hobbitEntity.setDanoHobbit(dto.getDanoHobbit());
        hobbitEntity.setNome(dto.getNome());
        hobbitEntity.setUsuario(usuario);
        
       return hobbitEntity;
    }

    @Override
    protected Class<HobbitJoin> getEntityClass() {
        return HobbitJoin.class; 
    }
    
    
}
