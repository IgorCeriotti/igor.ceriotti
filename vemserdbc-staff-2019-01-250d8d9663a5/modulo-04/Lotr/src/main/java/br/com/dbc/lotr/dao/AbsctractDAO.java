
package br.com.dbc.lotr.dao;

import br.com.dbc.lotr.entity.AbstractEntity;
import br.com.dbc.lotr.entity.HibernateUtil;
import br.com.dbc.lotr.entity.Usuario;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author igor.ceriotti
 */

//dao - data access object

public abstract class AbsctractDAO <E extends AbstractEntity> { //tipo generico nao pode mais ser quaçquer coisa, esse extends é um limitador. no caso pode ser um usuario, hobbitjoin ou elfojoin. 
    
    protected abstract Class<E> getEntityClass(); 
    
    
    public void criar(E entity) {
        Session session = HibernateUtil.getSession();
//        Transaction transaction = session.beginTransaction();
        session.save(entity); 
//        transaction.commit(); //vai para o banco. 
    }
    
    public void atualizar(E entity) {
        criar(entity);
    }
    
    public void remover(Integer id) {
        Session session = HibernateUtil.getSession();
        session.createQuery("delete from " + getEntityClass().getSimpleName()
                + "where id = " + id).executeUpdate();
    }
    
    public void remover(E entity) {
        remover(entity.getID()); 
    }
    
    public E buscar(Integer id) { //SELECT U.NOME FROM USUARIO U;
        Session session = HibernateUtil.getSession();
        return (E) session.createQuery("select e from " + getEntityClass().getSimpleName() 
                + " e "
                + "where id = " + id ).uniqueResult();
    }
    
    public List<E> listar() { 
        Session session = HibernateUtil.getSession();
        return session.createCriteria(getEntityClass()).list();
    }
}
