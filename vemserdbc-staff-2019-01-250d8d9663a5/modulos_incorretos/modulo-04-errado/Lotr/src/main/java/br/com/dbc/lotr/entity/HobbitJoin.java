
package br.com.dbc.lotr.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 *
 * @author igor.ceriotti
 */

@Entity
@Table(name = "HOBBIT_JOIN")
@PrimaryKeyJoinColumn(name = "id_personagem")

public class HobbitJoin extends PersonagemJoin {
    
    public HobbitJoin(){
        super.setRaca(RacaType.HOBBIT);
    }
    
    private Double danoHobbit; 

    public Double getDanoHobbit() {
        return danoHobbit;
    }

    public void setDanoHobbit(Double danoHobbit) {
        this.danoHobbit = danoHobbit;
    }
    
    
}
