package br.com.dbc.lotr;

import br.com.dbc.lotr.entity.ElfoJoin;
import br.com.dbc.lotr.entity.ElfoPerClass;
import br.com.dbc.lotr.entity.ElfoTabelao;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.entity.HibernateUtil;
import br.com.dbc.lotr.entity.HobbitJoin;
import br.com.dbc.lotr.entity.HobbitPerClass;
import br.com.dbc.lotr.entity.HobbitTabelao;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        Session session = null;
        Transaction transaction = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            Usuario usuario = new Usuario();
            usuario.setNome("Antonio");
            usuario.setApelido("antonio");
            usuario.setCpf(123l);
            usuario.setSenha("456");

            session.save(usuario);
            
            ElfoTabelao elfoTabelao = new ElfoTabelao();
            elfoTabelao.setDanoElfo(100d); 
            elfoTabelao.setNome("Legolas");
            session.save(elfoTabelao); 
            
            HobbitTabelao hobbitTabelao = new HobbitTabelao();
            hobbitTabelao.setDanoHobbit(10d);
            hobbitTabelao.setNome("Frodo");
            session.save(hobbitTabelao); 
            
            ElfoPerClass elfoPerClass = new ElfoPerClass();
            elfoPerClass.setNome("Legolas Per Class");
            elfoPerClass.setDanoElfo(100d);
            session.save(elfoPerClass);
            
            HobbitPerClass hobbitPerClass = new HobbitPerClass();
            hobbitPerClass.setNome("Bilbo");
            hobbitPerClass.setDanoHobbit(10d);
            session.save(hobbitPerClass);
            
            ElfoJoin elfoJoin = new ElfoJoin();
            elfoJoin.setNome("Legolas Join");
            elfoJoin.setDanoElfo(100d);
            session.save(elfoJoin);
            
            HobbitJoin hobbitJoin = new HobbitJoin();
            hobbitJoin.setNome("Hobbit Join");
            hobbitJoin.setDanoHobbit(10d);
            session.save(hobbitJoin);
                   

            transaction.commit(); //fecha transacao
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            System.exit(1);
        } finally {
            if (session != null) {
                session.close();
            }
            System.exit(0);

        }
    }
}
