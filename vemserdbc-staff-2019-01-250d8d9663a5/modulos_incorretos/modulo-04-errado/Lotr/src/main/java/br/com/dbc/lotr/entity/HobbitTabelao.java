
package br.com.dbc.lotr.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author igor.ceriotti
 */

@Entity
@Table(name = "HOBBIT_TABELAO")
@DiscriminatorValue("HOBBIT")

public class HobbitTabelao extends PersonagemTabelao {
    
    private Double danoHobbit; 

    public Double getDanoHobbit() {
        return danoHobbit;
    }

    public void setDanoHobbit(Double danoHobbit) {
        this.danoHobbit = danoHobbit;
    }
    
    
}
