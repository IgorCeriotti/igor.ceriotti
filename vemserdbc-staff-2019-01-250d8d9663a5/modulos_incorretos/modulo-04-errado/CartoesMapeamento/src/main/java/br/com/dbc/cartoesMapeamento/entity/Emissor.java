/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.cartoesMapeamento.entity;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author igor.ceriotti
 */
@Entity
@Table(name = "BANDEIRA")
public class Emissor {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "emissor_seq", sequenceName = "emissor_seq")
    @GeneratedValue(generator = "emissor_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "nome", length = 100, nullable = false)
    private String nome;
    
    @Column(name = "taxa", nullable = false, precision = 10, scale = 2)
    private Double taxa; 

    public Double getTaxa() {
        return taxa;
    }

    public void setTaxa(Double taxa) {
        this.taxa = taxa;
    }
    
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

   
}
