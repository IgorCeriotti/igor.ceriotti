/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.cartoesMapeamento.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 *
 * @author igor.ceriotti
 */

@Entity
@Table(name = "LANCAMENTO")
public class Lancamento {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "lancamento_seq", sequenceName = "lancamento_seq")
    @GeneratedValue(generator = "lancamento_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "id_cartao", nullable = false)
    private Integer id_cartao;
    
    @Column(name = "id_loja", nullable = false)
    private Integer id_loja;

    @Column(name = "id_credenciador ", nullable = false)
    private Integer id_credenciador;
    
    @Column(name = "descricao ", length = 50, nullable = false)
    private String descricao;
    
    @Column(name = "valor  ", precision = 10, scale = 2, nullable = false)
    private Double valor;
    
    @Column(name = "data_compra ", nullable = false)
    private Date data_compra;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_cartao() {
        return id_cartao;
    }

    public void setId_cartao(Integer id_cartao) {
        this.id_cartao = id_cartao;
    }

    public Integer getId_loja() {
        return id_loja;
    }

    public void setId_loja(Integer id_loja) {
        this.id_loja = id_loja;
    }

    public Integer getId_credenciador() {
        return id_credenciador;
    }

    public void setId_credenciador(Integer id_credenciador) {
        this.id_credenciador = id_credenciador;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Date getData_compra() {
        return data_compra;
    }

    public void setData_compra(Date data_compra) {
        this.data_compra = data_compra;
    }
    
    
}
