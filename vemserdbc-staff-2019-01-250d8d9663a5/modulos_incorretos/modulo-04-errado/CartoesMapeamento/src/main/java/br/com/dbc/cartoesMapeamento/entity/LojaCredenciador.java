/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.cartoesMapeamento.entity;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 *
 * @author igor.ceriotti
 */

@Entity
@Table(name = "LOJA_CREDENCIADOR")
public class LojaCredenciador {
    @EmbeddedId 
    private LojaCredenciadorId id;

    @Column(name = "id_loja", nullable = false)
    private Integer id_loja;
    
    @Column(name = "id_credenciador", nullable = false)
    private Integer id_credenciador; 
    
    @Column(name = "taxa", nullable = false, precision = 10, scale = 2)
    private Double taxa; 

    public Double getTaxa() {
        return taxa;
    }

    public void setTaxa(Double taxa) {
        this.taxa = taxa;
    }

    public LojaCredenciadorId getId() {
        return id;
    }

    public void setId(LojaCredenciadorId id) {
        this.id = id;
    }
    
   
    

    public Integer getId_loja() {
        return id_loja;
    }

    public void setId_loja(Integer id_loja) {
        this.id_loja = id_loja;
    }

    public Integer getId_credenciador() {
        return id_credenciador;
    }

    public void setId_credenciador(Integer id_credenciador) {
        this.id_credenciador = id_credenciador;
    }
    
    
}
