/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.cartoesMapeamento.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 *
 * @author igor.ceriotti
 */

@Entity
@Table(name = "CARTAO")

public class Cartao {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "cartao_seq", sequenceName = "cartao_seq")
    @GeneratedValue(generator = "cartao_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "nome", length = 100, nullable = false)
    private String nome;
    
    @Column(name = "chip", length = 50)
    private String chip;
    
    @Column(name = "id_cliente", nullable = false)
    private Integer id_cliente;

    @Column(name = "id_bandeira", nullable = false)
    private Integer id_bandeira;
    
    @Column(name = "id_emissor ", nullable = false)
    private Integer id_emissor;
    
    @Column(name = "vencimento ", nullable = false)
    private Date vencimento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getChip() {
        return chip;
    }

    public void setChip(String chip) {
        this.chip = chip;
    }

    public Integer getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(Integer id_cliente) {
        this.id_cliente = id_cliente;
    }

    public Integer getId_bandeira() {
        return id_bandeira;
    }

    public void setId_bandeira(Integer id_bandeira) {
        this.id_bandeira = id_bandeira;
    }

    public Integer getId_emissor() {
        return id_emissor;
    }

    public void setId_emissor(Integer id_emissor) {
        this.id_emissor = id_emissor;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }
    
    
}
