/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jdbc;

import com.sun.istack.internal.logging.Logger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import sun.util.logging.PlatformLogger;
import java.util.logging.Level;

/**
 *
 * @author igor.ceriotti
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Connection conn = Connector.connect();

        try {
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'USUARIO'")
                    .executeQuery();

            if (!rs.next()) {
                conn.prepareStatement("CREATE TABLE USUARIO (\n"
                        + "    ID NUMBER NOT NULL PRIMARY KEY,\n"
                        + "    NOME VARCHAR(100) NOT NULL,\n"
                        + "    APELIDO VARCHAR(15) NOT NULL,\n"
                        + "    SENHA VARCHAR(15) NOT NULL,\n"
                        + "    CPF NUMBER NOT NULL \n"
                        + "    )").execute();

            }
            PreparedStatement pst = conn.prepareStatement("insert into usuario(id, nome, apelido, senha, cpf)"
                    + " values (usuario_seq.nextval, ?, ?, ?, ?) ");
            pst.setString(1, "joao");
            pst.setString(2, "joaozinho");
            pst.setString(3, "123");
            pst.setInt(4, 123);
            pst.executeUpdate();
            rs = conn.prepareStatement("select * from Usuario").executeQuery();
            while (rs.next()) {
                System.out.println(String.format("Nome do usuario: %s", rs.getInt("nome")));
                System.out.println(String.format("Apelido do usuario: %s", rs.getString("apelido")));
                System.out.println(String.format("Senha do usuario: %s", rs.getString("senha")));
                System.out.println(String.format("cpf do usuario: %s", rs.getString("cpf")));

            }
        } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
