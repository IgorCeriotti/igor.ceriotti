import React, { Component } from 'react'
import './mensagemFlash.css'

export default class MensagemFlash extends Component {

  fechar = () => {
    console.log(`fechar`)
    this.props.atualizarMensagem( false )
  }

  render() {
    const { deveExibirMensagem, mensagem, cor } = this.props
    return <span onClick={ this.fechar } className={ `flash ${ cor || 'verde' } ${ deveExibirMensagem ? '' : 'invisivel' }` }>{ mensagem }</span>
  }
}
