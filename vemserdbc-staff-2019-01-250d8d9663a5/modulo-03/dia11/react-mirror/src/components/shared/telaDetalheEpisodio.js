import React, { Component } from 'react'
import EpisodiosApi from '../../models/episodiosApi'
import DetalhesEpisodio from '../../models/detalhesEpisodio'
import EpisodioUi from '../episodioUi'
import ListaEpisodios from '../../models/listaEpisodios'
import Episodio from '../../models/episodio';

export default class TelaDetalheEpisodio extends Component {
    constructor(props) {
        super(props)

        this.episodiosApi = new EpisodiosApi()
        this.state = {
            detalheEpisodio: {},
            episodio: {}
        }
    }

    componentDidMount() {
        console.log( `${ this.props.match.params }` )
        this.episodiosApi.buscarDetalhes(this.props.match.params.id)
            .then(detalhesServidor => {


                this.setState({
                    detalheEpisodio: new DetalhesEpisodio(detalhesServidor.data[0])
                })

            })
      
        // this.episodiosApi.buscar()
        //     .then(episodiosDoServidor => {
        //         this.listaEpisodios = new ListaEpisodios(episodiosDoServidor.data)
        //         this.setState({
        //             episodio: this.listaEpisodios.episodioAleatorio
        //         })

        //     })

    }

    render() {
        const { detalheEpisodio, episodio } = this.state
        return (
            !detalheEpisodio ? (
                <h3>Aguarde...</h3>
            ) : (
                    <React.Fragment>
                        <EpisodioUi episodio={episodio} />
                        <p>{detalheEpisodio.sinopse}</p>
                        <span>{detalheEpisodio.dataConvertida}</span>
                        <span>{detalheEpisodio.notaConvertida}</span>

                    </React.Fragment>
                )
        )
    }
}

