import axios from 'axios'

export default class EpisodiosApi {
  buscar() {
    return axios.get( 'http://localhost:9000/api/episodios' )
  }
  registrarNota( { nota, episodioId } ) {
    return axios.post( 'http://localhost:9000/api/notas', { nota, episodioId } )
  }

  buscarDetalhes(episodioId) {
    return axios.get(`http://localhost:9000/api/episodios/${episodioId}/detalhes`) 
  }

}
