import '../utils/number-prototypes'

export default class DetalhesEpisodio {
  constructor( {id, episodioId, notaImdb, sinopse, dataEstreia} ) {
    this.id = id
    this.episodioId = episodioId
    this.notaImdb = notaImdb
    this.sinopse = sinopse
    this.dataEstreia = dataEstreia
}

get notaConvertida(){
    return this.notaImdb/2 
}

get dataConvertida(){
    const data = new Date(this.dataEstreia).toLocaleDateString()

    
    return data 
    
   
}
}

