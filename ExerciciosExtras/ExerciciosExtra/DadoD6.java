
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class DadoD6{
    
     private static final ArrayList<Integer> NUMEROS_FACES = new ArrayList<>(  
            Arrays.asList(
                1,2,3,4,5,6
               
            )
        );

    
        static int randomNum = ThreadLocalRandom.current().nextInt(1, NUMEROS_FACES.size() + 1);
        
        public int sortear(){
            return randomNum;
            
        }
        
        public static void main(String[] args){
            System.out.println(randomNum); 
        }
        
    }


