public class Elfo {

    private String nome;
    private int indiceFlecha = 1;
    private Inventario inventario = new Inventario();
    private int experiencia = 0;
    private Status status = Status.RECEM_CRIADO;

    public Elfo(String nome) {
        this.setNome(nome);
        //this.status = Status.RECEM_CRIADO;
        this.inventario.adicionar(new Item(1, "Arco"));
        this.inventario.adicionar(new Item(2, "Flecha"));
    }

    public String getNome() {
        return this.nome;
    }
    
    public Status getStatus (){
        return this.status;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getExperiencia() {
        return this.experiencia;
    }

    private void aumentarXp() {
        //experiencia++;
        experiencia = experiencia + 1;
    }

    public Item getFlecha() {
        return this.inventario.obter(this.indiceFlecha);
    }

    public int getQtdFlechas() {
        return this.getFlecha().getQuantidade();
    }
    
    // DRY - Don't Repeat Yourself
    private boolean podeAtirarFlecha() {
        return this.getFlecha().getQuantidade() > 0;
    }

    public void atirarFlecha(Dwarf dwarf) {
        int qtdAtual = this.getFlecha().getQuantidade();
        if (podeAtirarFlecha()) {
            this.getFlecha().setQuantidade(qtdAtual - 1);
            //experiencia = experiencia + 1;
            this.aumentarXp();
            dwarf.sofrerDano();
        }
    }
}

