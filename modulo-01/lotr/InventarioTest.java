import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InventarioTest {
    @Test
    public void criarInventarioSemQuantidadeInformada() {
        Inventario inventario = new Inventario();
        assertEquals(99, inventario.getItens().length);
    }

    @Test
    public void criarInventarioInformandoQuantidadeItens() {
        Inventario inventario = new Inventario(42);
        assertEquals(42, inventario.getItens().length);
    }

    @Test
    public void adicionarUmItem() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        assertEquals(espada, inventario.getItens()[0]);
    }

    @Test
    public void adicionarDoisItens() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(3, "Escudo de aço");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        assertEquals(espada, inventario.getItens()[0]);
        assertEquals(escudo, inventario.getItens()[1]);
    }

    @Test
    public void adicionarDoisItensComEspaçoParaUmNaoAdicionaSegundo() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        Item armadura = new Item(1, "Armadura");
        inventario.adicionar(espada);
        inventario.adicionar(armadura);
        assertEquals(espada, inventario.getItens()[0]);
        assertEquals(1, inventario.getItens().length);
    }

    @Test
    public void obterItemNaPrimeiraPosicao() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        assertEquals(espada, inventario.obter(0));
    }

    @Test
    public void obterItemNaoAdicionado() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        assertNull(inventario.obter(0));
    }

    @Test
    public void removerItem() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.remover(0);
        assertNull(inventario.obter(0));
    }

    @Test
    public void removerItemAntesDeAdicionarProximo() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        Item armadura = new Item(1, "Armadura");
        inventario.adicionar(espada);
        inventario.remover(0);
        inventario.adicionar(armadura);
        assertEquals(armadura, inventario.obter(0));
        assertEquals(1, inventario.getItens().length);
    }

    @Test
    public void adicionarAposRemover() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item bracelete = new Item(1, "Bracelete de prata");
        Item armadura = new Item(1, "Armadura");
        inventario.adicionar(espada);
        inventario.adicionar(bracelete);
        inventario.remover(0);
        inventario.adicionar(armadura);
        assertEquals(armadura, inventario.obter(0));
        assertEquals(bracelete, inventario.obter(1));
    }

    @Test
    public void getDescricoesVariosItens() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        String resultado = inventario.getDescricoesItens();
        assertEquals("Espada,Escudo", resultado);
    }

    @Test
    public void getDescricoesItensRemovendoItemNoMeio() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item flechas = new Item(3, "Flechas");
        Item botas = new Item(1, "Botas de ferro");

        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(flechas);
        inventario.adicionar(botas);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(flechas);
        inventario.adicionar(botas);

        inventario.remover(1);
        inventario.remover(2);
        inventario.remover(3);
        inventario.remover(5);
        String resultado = inventario.getDescricoesItens();
        assertEquals("Espada,Espada,Flechas,Botas de ferro", resultado);
    }

    @Test
    public void getDescricoesItensVazio() {
        Inventario inventario = new Inventario();
        String resultado = inventario.getDescricoesItens();
        assertEquals("", resultado);
    }

    @Test
    public void getItemMaiorQuantidadeComVarios() {
        Inventario inventario = new Inventario();
        Item lanca = new Item(1, "Lança");
        Item espada = new Item(3, "Espada de aço valiriano");
        Item escudo = new Item(2, "Escudo de madeira");
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        Item resultado = inventario.getItemComMaiorQuantidade();
        assertEquals(espada, resultado);
    }

    @Test
    public void getItemMaiorQuantidadeInventarioVazio() {
        Inventario inventario = new Inventario(0);
        Item resultado = inventario.getItemComMaiorQuantidade();
        assertNull(resultado);
    }

    @Test
    public void getItemMaiorQuantidadeItensComMesmaQuantidade() {
        Inventario inventario = new Inventario();
        Item lanca = new Item(3, "Lança");
        Item espada = new Item(3, "Espada de aço valiriano");
        Item escudo = new Item(2, "Escudo de madeira");
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        Item resultado = inventario.getItemComMaiorQuantidade();
        assertEquals(lanca, resultado);
    }
}

