public class Dwarf {
    private String nome;
    private double vida;
    private Status status; 
    
    

    public Dwarf(String nome) {
        this.setNome(nome);
        this.vida = 110.0;
        this.status = Status.RECEM_CRIADO;
    }
    
    public Status getStatus (){
        return this.status;
    }
    
    public double getVida() {
        return this.vida;
    }
    
    public void setVida(int vida){
        this.vida = vida;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return this.nome;
    }

    private boolean podeSofrerDano() {
        return this.vida > 0;
    }

    public void sofrerDano() {
        //this.vida = this.vida - 10.0;
        if (podeSofrerDano()) {
            this.vida -= 10.0;
        }
    }
    
    public void impedeVidaDeSerNegativa(){
        if (this.getVida() < 0) {
            this.setVida(0); 
        }
    }
    
    public void checaSeAnaoEstaMorto (){
        if (this.getVida() == 0) {
            this.status = Status.MORTO;
        }
    }
}




