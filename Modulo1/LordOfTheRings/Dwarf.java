/*public class Dwarf{
    private String name; 
    private double life = 110.0; 
    
    
    public Dwarf(String name){
        this.name = name;   //this.setNome(nome).
    } 
    
    public String getName() {
        return this.name; 
    }
    
    public double getLife() {
        return this.life;
    }
    
    public boolean DwarfAllowedToSufferDamage () {
        return this.life > 0; 
    }
    
    public void setLifeAfterArrow (double life){
        if (DwarfAllowedToSufferDamage()) {
        this.life = this.life - 10.0; //this.life-=10.0
    }
    }
    
  
    
    
}
/*
 * 
 */

public class Dwarf extends Personagem {
    
    private boolean equipado = false;
    
    public Dwarf(String nome) {
        super(nome); //usa super pra chamar construtor da super classe, mesma coisa que o 'this' pra coisas locais. era this.setNome();
        this.vida = 110.0;
        this.inventario = new Inventario(1);
        this.ganharItem(new Item(1, "Escudo"));
        this.qtdDano=10.0;
        // this.inventario.adicionar(new Item(1, "Escudo"));
    }
   
    private boolean podeSofrerDano() {
        return this.vida > 0;
    }
    
    public boolean equiparEscudo (){
        return this.equipado = true; 
    }
    
    protected double calcularDano() {
        return this.equipado ? 5.0 : this.qtdDano;
    }
    
    // public void sofrerDano() {
        // //this.vida = this.vida - 10.0;
     // if (podeSofrerDano()) {
            // double qtdDano = this.equipado ? 5.0 : 10.0;
            // this.vida -= qtdDano;
            // this.status = Status.SOFREU_DANO;
        // } else {
            // this.status = Status.MORTO;
        // }
    // }
    
        //public void impedeVidaDeSerNegativa(){
    //  if (this.getVida() < 0) {
    //     this.setVida(0); 
    //    }
    //}
    
    // public void mudaStatusDoAnaoParaMorto (){
        // if (this.getVida() == 0) {
            // this.status = Status.MORTO;
        // }    
    // }
    
    public String imprimirResumo(){
        return "Dwarf"; 
    }

    
}
    





