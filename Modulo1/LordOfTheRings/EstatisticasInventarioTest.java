

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;


public class EstatisticasInventarioTest
{
    @Test
    public void calcularMediaInventarioVazio(){
        Inventario inventario = new Inventario(1);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertTrue(Double.isNaN(resultado));
        
    }
    
    @Test
    public void calcularMediaComUmItem(){
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(2, "Escudo"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertEquals(2,resultado,1e-8); 
        
    }
    
    @Test
    public void calcularMediaQtdIguais(){
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(2, "Escudo"));
        inventario.adicionar(new Item(2, "Flecha"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertEquals(2,resultado,1e-8); 
        
    }
    
    @Test
    public void calcularMediaQtdsDiferentes(){
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(4, "Escudo"));
        inventario.adicionar(new Item(4, "Flecha"));
        inventario.adicionar(new Item(1, "Botas"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertEquals(3,resultado,1e-8); 
        
    }
    
    @Test
    public void calcularMedianaComInventarioVazio(){
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(2, "Escudo"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals(2,resultado,1e-8); 
    }
    
    @Test
    public void calcularMedianaComUmItem(){
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(2, "Escudo"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals(2,resultado,1e-8); 
        
    }
    
    @Test
    public void calcularMedianaQtdImpar(){
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(2, "Escudo"));
        inventario.adicionar(new Item(10, "Adaga"));
        inventario.adicionar(new Item(20, "Poção"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals(10,resultado,1e-8); 
    }
    
        
    @Test
    public void calcularMedianaQtdPar(){
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(2, "Escudo"));
        inventario.adicionar(new Item(10, "Adaga"));
        inventario.adicionar(new Item(20, "Poção"));
        inventario.adicionar(new Item(30, "Poção"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals(15,resultado,1e-8); 
    }
    
    @Test
    public void qtdItensAcimaDaMediaVariosItens(){
        Inventario inventario = new Inventario(1);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        inventario.adicionar(new Item(5, "Escudo"));
        inventario.adicionar(new Item(10, "Adaga"));
        inventario.adicionar(new Item(20, "Poção"));
        inventario.adicionar(new Item(30, "Poção"));
        int resultado = estatisticas.qtdItensAcimaDaMedia();
        //media =16.25
        assertEquals(2,resultado); 
    }
}
