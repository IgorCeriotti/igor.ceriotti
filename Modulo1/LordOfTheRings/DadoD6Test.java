

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DadoD6Test{
    @Test
    public void testarSeNumerosGeradosSaoCorretos(){
        boolean numberRange = false;
        DadoD6 dado = new DadoD6();
        dado.sortear();
        if (dado.sortear()>=1 && dado.sortear()<=6){
            numberRange = true;
        }
       assertTrue(numberRange); 
    }
}
