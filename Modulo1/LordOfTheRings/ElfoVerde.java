
import java.util.*;
   public class ElfoVerde extends Elfo{
       
     private static final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(  //STATIC: TEM UM ARRAYLIST PRA CLASSE TODA DE ELFOVERDE, NAO CRIA UMA NOVA A CADA CRIACAO DE UM ELFO. GARANTE QUE OS OBJETOS DA CLASSE INTEIRA COMPARTILHEM DESSAS COISAS COMUNS.
            Arrays.asList(
                "Espada de aço valiriano",
                "Arco de Vidro",
                "Flecha de Vidro"
            )
        );
        
    
    public ElfoVerde(String nome) {
        super(nome);
        this.qtdExperienciaPorAtaque=2;
        // this.status = Status.RECEM_CRIADO;
        // this.ganharItem(new Item(1, "Arco"));
        // this.ganharItem(new Item(2, "Flecha"));
        // this.inventario.adicionar(new Item(1, "Arco"));
        // this.inventario.adicionar(new Item(2, "Flecha"));
    }


      // public void atirarFlecha(Dwarf dwarf) {  //ja tem esse metodo nao precisa, qdo chamar ele vai usar o aumentarXp desse aqui (o dobro).
        // int qtdAtual = this.getFlecha().getQuantidade();
        // if (podeAtirarFlecha()) {
            // this.getFlecha().setQuantidade(qtdAtual - 1);
            
            // this.aumentarXp();
            // dwarf.sofrerDano();
        // }
    // }
    
    public void ganharItem(Item item){
        boolean descricaoValida = 
           DESCRICOES_VALIDAS.contains(item.getDescricao());
        if (descricaoValida){
            this.inventario.adicionar(item);
        }
    }
    
        public void perderItem(Item item) {
        boolean descricaoValida = DESCRICOES_VALIDAS.contains(item.getDescricao());
        if (descricaoValida) {
            this.inventario.remover(item);
        }
    }
}



