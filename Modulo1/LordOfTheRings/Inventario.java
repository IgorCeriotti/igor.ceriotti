/*public class Inventario {
    
    private Item[] itens;  
    
    public Inventario(int quantidadeItens){
        this.itens = new Item[quantidadeItens];
    }
    
    public Item[] getItens(){
        return this.itens; 
    }
    
    public Inventario(){
        this.itens = new Item[99]; //this(99); 
    }
    
    public Item obter(int posicao) {
        return this.itens[posicao]; 
    }
    
    public void remover (int posicao){
         this.itens[posicao] = null; 
    }
    
    public void adicionar(Item item){
        for (int i=0; i < this.itens.length; i++){
            if (this.itens[i] == null){
                this.itens[i] = item;
                break;
            }
        }
    }
    
    public String listarNomesDosItens(){
       String s= "";
       int cont=0;
       for(int i=0; i<itens.length;i++){
           if (itens[i] != null&& cont!=0){

               s= s+", "+ itens[i].getDescricao();
           }
           if (itens[i] != null&& cont==0){

               s=itens[i].getDescricao();
               cont++;
           }


       }
       return s;
   }
       
   public Item retornarItemComMaiorQuantidade(){
       Item itemMaiorQuant=null;
       for(int i=0; i<itens.length;i++){
           if (itens[i] != null && itens[i].getQuantidade()>itemMaiorQuant.getQuantidade()){
               itemMaiorQuant=itens[i];

           }

       }
       return itemMaiorQuant;

   }
    }*/
    
import java.util.*;
public class Inventario {
    //private Item[] itens;
    // private int posicaoAPreencher = 0, ultimaPosicaoPreenchida = 0;
    private ArrayList<Item> itens;// = new ArrayList<>(); 

    public Inventario(int quantidadeItens) {
        //this.itens = new Item[quantidadeItens];
        this.itens = new ArrayList<>(quantidadeItens);
    }

    //public Inventario() {
    //   this(99);
    //}

    public ArrayList<Item> getItens() {
        //return this.itens;
        return this.itens;
    }

    public Item obter(int posicao) {
        //return this.itens[posicao];
        if (posicao >= this.itens.size()){
            return null;
        }
        return this.itens.get(posicao);
    }

    // public void remover(int posicao) {
        // //this.itens[posicao] = null;
        
        // this.itens.remove(posicao);
        // // this.posicaoAPreencher = posicao;
        
        
    // }
    
        public void remover(Item item) {
        //this.itens[posicao] = null;
        
        this.itens.remove(item);
        // this.posicaoAPreencher = posicao;
        
        
    }

    public void adicionar(Item item) {
        // for (int i = this.posicaoAPreencher; i < this.itens.length; i++) {
            // if (this.itens[i] == null) {
                // this.itens[i] = item;
                // posicaoAPreencher = i + 1;
                // ultimaPosicaoPreenchida = i;
                // break;
            // }
        // }
        
        this.itens.add(item);
    }
    
    public String getDescricoesItens() {
        StringBuilder descricoes = new StringBuilder();
        // for (int i = 0; i < this.itens.length; i++) {
            // if (this.itens[i] != null) {
                // String descricao = this.itens[i].getDescricao();
                // descricoes.append(descricao);
                // boolean deveColocarVirgula = i < this.ultimaPosicaoPreenchida;
                // if (deveColocarVirgula) {
                    // descricoes.append(",");
                // }
            // }
        // }
        
        for (int i = 0; i < this.itens.size(); i++) {
            if (this.itens.get(i) != null) {
                String descricao = this.itens.get(i).getDescricao();
                descricoes.append(descricao);
                boolean deveColocarVirgula = i < this.itens.size() -1;
                if (deveColocarVirgula) {
                    descricoes.append(",");
                }
            }
        }

        return descricoes.toString();
    }
    
    public Item getItemComMaiorQuantidade() {

        int indice = 0, maiorQuantidadeParcial = 0;
        // for (int i = 0; i < this.itens.length; i++) {
            // if (this.itens[i] != null) {
                // int qtdAtual = this.itens[i].getQuantidade();
                // if (qtdAtual > maiorQuantidadeParcial) {
                    // maiorQuantidadeParcial = qtdAtual;
                    // indice = i;
                // }
            // }
        // }
        
        for (int i = 0; i < this.itens.size(); i++) {
            if (this.itens.get(i) != null) {
                int qtdAtual = this.itens.get(i).getQuantidade();
                if (qtdAtual > maiorQuantidadeParcial) {
                    maiorQuantidadeParcial = qtdAtual;
                    indice = i;
                }
            }
        }
        return this.itens.size() > 0 ? this.itens.get(indice) : null;
    }
    
    public Item buscar (String descricao){
        for (Item itemAtual : this.itens){
            boolean encontrei = itemAtual.getDescricao().equals(descricao); 
            if (encontrei){
                return itemAtual;
            }
        }
        return null;
    }
    
    public ArrayList<Item> inverter(){
        ArrayList<Item> listaInvertida = new ArrayList<>(this.itens.size());
        for (int i = this.itens.size()-1; i>=0; i--){
            listaInvertida.add(this.itens.get(i));
        }
        return listaInvertida;
    }
    
    // public void ordenarItens(){
          // for (int i = 0; i < this.itens.size(); i++) {
              // for (int j = this.itens.size() - 1; j > i; j--) {
                  // if (this.itens.get(i).getQuantidade() > this.itens.get(j).getQuantidade()) {
    
                      // Item tmp = this.itens.get(i);
                      // this.itens.set(i,this.itens.get(j)) ;
                      // this.itens.set(j,tmp);
    
                  // }

          // }

      // }
      
    // }
    
    public void ordenarItens(){
       this.ordenarItens(TipoOrdenacao.ASC);

    }
    
   
 public void ordenarItens(TipoOrdenacao ordenacao) {
        for (int i = 0; i < this.itens.size(); i++) {
            for (int j = 0; j < this.itens.size() - 1; j++) {
                Item atual = this.itens.get(j);
                Item proximo = this.itens.get(j + 1);
                boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ? atual.getQuantidade() > proximo.getQuantidade() : atual.getQuantidade() < proximo.getQuantidade();
                if (deveTrocar) {
                    Item itemTrocado = atual;
                    this.itens.set(j, proximo);
                    this.itens.set(j + 1, itemTrocado);
                }
            }
        }
    }
    
    
}









