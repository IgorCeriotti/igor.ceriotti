public class Item {
    //quantidade, descricao 
    protected int quantidade;
    private String descricao;
    
    public Item (int quantidade, String descricao){
        this.setQuantidade(quantidade);
        this.descricao = descricao;
    }
    
    public String getDescricao() {
        return this.descricao;
    }
    
    public int getQuantidade () {
        return this.quantidade;
    }
    
    public void setDescricao (String descricao){
        this.descricao = descricao;
    }
    
    public void setQuantidade (int quantidade){
        this.quantidade = quantidade; 
    }
    
    public boolean equals(Object obj){ //para sobrescrever da classe object; "personalizar". 
        Item outroItem = (Item)obj; //casting. faz java interpretar obj como Item. fiz isso pra pegar um getQuantidade. 
        return this.quantidade == outroItem.getQuantidade() && this.descricao.equals(outroItem.getDescricao()); 
    }
    
}
