import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest {
    @Test
    public void dwarfNasceCom110DeVida() {
        Dwarf dwarf = new Dwarf("Mulungrid Redmail");
        assertEquals(110.0, dwarf.getVida(), .000001);
    }

    @Test
    public void dwarfPerdeDezDeVida() {
        // Arrange
        Dwarf dwarf = new Dwarf("Brostaeni Shadowgrip");
        // Act
        dwarf.sofrerDano();
        // Assert
        assertEquals(100.0, dwarf.getVida(), .01);
    }

    @Test
    public void dwarfPerdeDezDeVidaDuasVezes() {
        // Arrange
        Dwarf dwarf = new Dwarf("Brostaeni Shadowgrip");
        // Act
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        // Assert
        assertEquals(90.0, dwarf.getVida(), .01);
    }

    @Test
    public void dwarfPerdeDezDeVidaDozeVezes() {
        // Arrange
        Dwarf dwarf = new Dwarf("Brostaeni Shadowgrip");
        // Act
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        // Assert
        assertEquals(0.0, dwarf.getVida(), .01);
    }
    
        @Test
    public void dwarfPerdeDezDeVidaDozeVezesDeveMorrer() {
        // Arrange
        Dwarf dwarf = new Dwarf("Brostaeni Shadowgrip");
        // Act
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        // Assert
        assertEquals(Status.MORTO, dwarf.getStatus());
    }
    @Test
    public void dwarfTemStatusMorto (){
        Dwarf dwarf = new Dwarf("Brostaeni Shadowgrip");
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        assertEquals(Status.MORTO, dwarf.getStatus());
    }
    
    @Test
    public void dwarfNaoPodeTerVidaNegativa(){
        Dwarf dwarf = new Dwarf("Brostaeni Shadowgrip");
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        assertEquals(0.0, dwarf.getVida(), .000001);
    }
    
    @Test
    public void dwarfPerdeVidaEContinuaVivo(){
        Dwarf dwarf = new Dwarf("Brostaeni Shadowgrip");
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        assertEquals(Status.SOFREU_DANO, dwarf.getStatus()); 
    }
    
    @Test
    public void dwarfNasceComStatus(){
        Dwarf dwarf = new Dwarf("Brostaeni Shadowgrip");

        assertEquals(Status.RECEM_CRIADO, dwarf.getStatus()); 
    }
}
