
// import java.util.*;
// import java.util.concurrent.ThreadLocalRandom;

// public class DadoD6{
    
     // private static final ArrayList<Integer> NUMEROS_FACES = new ArrayList<>(  
            // Arrays.asList(
                // 1,2,3,4,5,6
               
            // )
        // );

        // public DadoD6(){
       
        // }
       
        
        
        // public int sortear(){
            // int randomNum = ThreadLocalRandom.current().nextInt(1, NUMEROS_FACES.size() + 1);
            // return randomNum;
            
        // }
        
        
        
    // }
    
    import java.util.Random;

public class DadoD6 implements Sorteador {
    
    private Random random = new Random();
    private static final int NUMERO_FACES = 6;
    
    public void setSeed(int seed) {
        this.random.setSeed(seed);
    }
    
    public int sortear() {
        return this.random.nextInt(NUMERO_FACES) + 1;
    }
}
