import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;
public class PaginadorInventarioTest
{
    @Test
    public void pularLimitarComInventarioVazio(){
        Inventario inventario = new Inventario(0);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(1);
        assertTrue(primeiraPagina.isEmpty());
    }
    
    @Test
    public void pularLimitarComApenasUmItem(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item (1, "Espada");
        inventario.adicionar(espada);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(1);
        assertEquals(1, primeiraPagina.size());
    }
    
    @Test
    public void pularLimitarDentroDosLimites(){
        Inventario inventario = new Inventario(3);
        Item espada = new Item (1, "Espada");
        Item escudo = new Item (2, "Escudo");
        Item lanca = new Item (10, "Lança");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(lanca);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        
        ArrayList<Item> primeiraPagina = paginador.limitar(2);
        assertEquals(espada, primeiraPagina.get(0));
        assertEquals(escudo, primeiraPagina.get(1));
        assertEquals(2, primeiraPagina.size());
    }
    
     // @Test
     // public void pularLimitarForaDosLimites(){
         // Inventario inventario = new Inventario(3);
         // Item espada = new Item (1, "Espada");
        // Item escudo = new Item (2, "Escudo");
         // Item lanca = new Item (10, "Lança");
         // inventario.adicionar(espada);
         // inventario.adicionar(escudo);
       // inventario.adicionar(lanca);
         // PaginadorInventario paginador = new PaginadorInventario(inventario);
       // paginador.pular(0);
        
         // ArrayList<Item> primeiraPagina = paginador.limitar(2000);
        // assertEquals(espada, primeiraPagina.get(0));
         // assertEquals(escudo, primeiraPagina.get(1));
        // assertEquals(2, primeiraPagina.size());
   // }
}
