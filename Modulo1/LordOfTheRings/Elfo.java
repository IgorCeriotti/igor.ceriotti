/*public class Elfo {
    private String nome;
    private int indiceFlecha = 1; 
    private Inventario inventario = new Inventario();
    private int experiencia=0;
    private Dwarf anao = new Dwarf("Gimli");
    
    public Elfo(String nome){
        this.nome = nome;   //this.setNome(nome);
        this.inventario.adicionar(new Item(1, "Arco"));
        this.inventario.adicionar(new Item(2, "Flecha"));
    } 
    
    public String getNome() {
       return this.nome; 
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }
    
    private void aumentarXp(){  //melhor privado pq publico posso alterar e ficar chamando o tempo todo para aumentar xp sem perder flechas.
        experiencia++;
    }
    
    public int getExperiencia (){
        return this.experiencia;
    }
    
    public Item getFlecha(){
        return this.inventario.obter(this.indiceFlecha);      //retornando o objeto. (flecha é objeto)
    }
    
    
    public Dwarf getAnao(){
        return this.anao;
    }
    

    
    private boolean podeAtirarFlecha (){
        return this.getFlecha().getQuantidade() > 0; 
    }
    
    public void atirarFlecha (Dwarf anao){
        int qtdAtual = this.getFlecha().getQuantidade();
        double dwarfLife = anao.getLife();
         
       // boolean podeAtirarFlecha = qtdAtual > 0;
        if (podeAtirarFlecha()){
            this.aumentarXp(); 
            this.getFlecha().setQuantidade(qtdAtual - 1);
            anao.setLifeAfterArrow(dwarfLife);
    }
    
    }
    
    public void checaFlecha (){
        int qtd = this.getFlecha().getQuantidade();
        if (qtd <0) {
            this.getFlecha().setQuantidade(0);
        }
    }
 
    /*public void HitDwarfWithArrow(){
        int qtdAtual = flecha.getQuantidade();
        double dwarfLife = anao.getLife();
        
        if (podeAtirarFlecha()){
            this.aumentarXp(); 
            flecha.setQuantidade(qtdAtual - 1);
    }
       anao.setLifeAfterArrow(dwarfLife); 
    } */
    
    //String[] filmesFavoritos = new String[15];

    public class Elfo extends Personagem {
    protected int indiceFlecha;
    protected int experiencia;
    // protected double qtdDano;
    protected int qtdExperienciaPorAtaque;
    private static int contadorDeElfos = 0;
  
    {
        this.contadorDeElfos++;
        this.inventario = new Inventario(2);
        this.indiceFlecha=1;
        this.qtdExperienciaPorAtaque=1; 
        this.experiencia=0;
        // this.qtdDano=0.0;
        this.vida = 100.0; 
    }

    public Elfo(String nome) {
        super(nome);
        // this.status = Status.RECEM_CRIADO;
        this.ganharItem(new Item(1, "Arco"));
        this.ganharItem(new Item(2, "Flecha"));
        // this.inventario.adicionar(new Item(1, "Arco"));
        // this.inventario.adicionar(new Item(2, "Flecha"));
    }
    
    // private boolean podeSofrerDano() {
        // return this.vida > 0;
    // }
    
    // public void sofrerDano() {
        // this.vida -= this.vida >= this.qtdDano ? this.qtdDano : this.vida; //com 10 de vida perde 10 nao 15 e nao fica negativo e tal. 
     // if (podeSofrerDano()) {
            // // this.vida -= qtdDano;
            // this.status = Status.SOFREU_DANO;
        // } else {
            // this.status = Status.MORTO;
            // // this.vida = 0.0;
        // }
        // //this.status=podeSofrerDano() ? Status.SOFREU_DANO : Status.MORTO; 
    // }

    public int getExperiencia() {
        return this.experiencia;
    }

    protected void aumentarXp() {
        //experiencia++;
        experiencia = experiencia + this.qtdExperienciaPorAtaque;
    }
    

    public Item getFlecha() {
        return this.inventario.obter(this.indiceFlecha);
    }

    public int getQtdFlechas() {
        return this.getFlecha().getQuantidade();
    }
    
    // DRY - Don't Repeat Yourself
    protected boolean podeAtirarFlecha() {
        return this.getFlecha().getQuantidade() > 0;
    }
    
    public String imprimirResumo(){
        return "Elfo"; 
    }

    public void atirarFlecha(Dwarf dwarf) {
        int qtdAtual = this.getFlecha().getQuantidade();
        if (podeAtirarFlecha()) {
            this.getFlecha().setQuantidade(qtdAtual - 1);
            //experiencia = experiencia + 1;
            this.aumentarXp();
            this.sofrerDano();
            dwarf.sofrerDano();
        }
    }
}

