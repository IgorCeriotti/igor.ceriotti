import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class InventarioTest {
    // @Test
    // public void criarInventarioSemQuantidadeInformada() {
        // Inventario inventario = new Inventario(42);
        // assertEquals(99, inventario.getItens().size());
    // }



    @Test
    public void adicionarUmItem() {
        Inventario inventario = new Inventario(42);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        assertEquals(espada, inventario.getItens().get(0));
    }

    @Test
    public void adicionarDoisItens() {
        Inventario inventario = new Inventario(2);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(3, "Escudo de aço");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        assertEquals(espada, inventario.getItens().get(0));
        assertEquals(escudo, inventario.getItens().get(1));
    }

    @Test
    public void adicionarDoisItensComEspaçoParaUmNaoAdicionaSegundo() {
        Inventario inventario = new Inventario(2);
        Item espada = new Item(1, "Espada");
        Item armadura = new Item(1, "Armadura");
        inventario.adicionar(espada);
        inventario.adicionar(armadura);
        assertEquals(espada, inventario.getItens().get(0));
        assertEquals(2, inventario.getItens().size());
    }

    @Test
    public void obterItemNaPrimeiraPosicao() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        assertEquals(espada, inventario.obter(0));
    }

    @Test
    public void obterItemNaoAdicionado() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        assertNull(inventario.obter(0));
    }

    @Test
    public void removerItem() {
        Inventario inventario = new Inventario(10);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.remover(espada);
        assertNull(inventario.obter(0));
    }

    @Test
    public void removerItemAntesDeAdicionarProximo() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        Item armadura = new Item(1, "Armadura");
        inventario.adicionar(espada);
        inventario.remover(espada);
        inventario.adicionar(armadura);
        assertEquals(armadura, inventario.obter(0));
        assertEquals(1, inventario.getItens().size());
    }

    @Test
    public void adicionarAposRemover() {
        Inventario inventario = new Inventario(3);
        Item espada = new Item(1, "Espada");
        Item bracelete = new Item(1, "Bracelete de prata");
        Item armadura = new Item(1, "Armadura");
        inventario.adicionar(espada);
        inventario.adicionar(bracelete);
        inventario.remover(espada);
        inventario.adicionar(armadura);
        assertEquals(bracelete, inventario.obter(0));
        assertEquals(armadura, inventario.obter(1));
    }

    @Test
    public void getDescricoesVariosItens() {
        Inventario inventario = new Inventario(2);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        String resultado = inventario.getDescricoesItens();
        assertEquals("Espada,Escudo", resultado);
    }

    // @Test
    // public void getDescricoesItensRemovendoItemNoMeio() {
        // Inventario inventario = new Inventario(42);
        // Item espada = new Item(1, "Espada");
        // Item escudo = new Item(2, "Escudo");
        // Item flechas = new Item(3, "Flechas");
        // Item botas = new Item(1, "Botas de ferro");

        // inventario.adicionar(espada);
        // inventario.adicionar(escudo);
        // inventario.adicionar(flechas);
        // inventario.adicionar(botas);
        // inventario.adicionar(espada);
        // inventario.adicionar(escudo);
        // inventario.adicionar(flechas);
        // inventario.adicionar(botas);

        // inventario.remover(1);
        // inventario.remover(2);
        // inventario.remover(3);
        // inventario.remover(5);
        // String resultado = inventario.getDescricoesItens();
        // assertEquals("Espada,Espada,Flechas,Botas de ferro", resultado);
    // }
    
     @Test
    public void criarInventarioVazioInformandoQuantidadeInicialDeItens() {
        Inventario inventario = new Inventario(42);
        assertEquals(0, inventario.getItens().size());
    }
    
    @Test
    public void getDescricoesItensVazio() {
        Inventario inventario = new Inventario(3);
        String resultado = inventario.getDescricoesItens();
        assertEquals("", resultado);
    }

    @Test
    public void getItemMaiorQuantidadeComVarios() {
        Inventario inventario = new Inventario(3);
        Item lanca = new Item(1, "Lança");
        Item espada = new Item(3, "Espada de aço valiriano");
        Item escudo = new Item(2, "Escudo de madeira");
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        Item resultado = inventario.getItemComMaiorQuantidade();
        assertEquals(espada, resultado);
    }

    @Test
    public void getItemMaiorQuantidadeInventarioVazio() {
        Inventario inventario = new Inventario(0);
        Item resultado = inventario.getItemComMaiorQuantidade();
        assertNull(resultado);
    }

    @Test
    public void getItemMaiorQuantidadeItensComMesmaQuantidade() {
        Inventario inventario = new Inventario(3);
        Item lanca = new Item(3, "Lança");
        Item espada = new Item(3, "Espada de aço valiriano");
        Item escudo = new Item(2, "Escudo de madeira");
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        Item resultado = inventario.getItemComMaiorQuantidade();
        assertEquals(lanca, resultado);
    }
    
    @Test
    public void buscarItemComInventarioVazio(){
        Inventario inventario = new Inventario(0);
        assertNull(inventario.buscar("Bracelete"));
    }
    
    @Test
    public void buscarApenasUmItem(){
        Inventario inventario = new Inventario(1);
        Item lanca = new Item(1, "Lança");
        inventario.adicionar(lanca);
        Item resultado = inventario.buscar("Lança");
        assertEquals(lanca, resultado);
        
    }
    
    @Test
    public void buscarApenasPrimeiroItemComMesmaDescricao(){
        Inventario inventario = new Inventario(1);
        Item lanca = new Item(1, "Lança");
        Item energetico1 = new Item(4, "Burn");
        Item energetico2 = new Item(2, "Burn");
        inventario.adicionar(lanca);
        inventario.adicionar(energetico1);
        inventario.adicionar(energetico2);
        Item resultado = inventario.buscar("Burn");
        assertEquals(energetico1, resultado);
        
    }
    
    @Test
    public void inverterInventarioVazio(){
        Inventario inventario = new Inventario(0);
        
        assertTrue(inventario.inverter().isEmpty());
    }
    
        
    @Test
    public void inverterComApenasUmItem(){
        Inventario inventario = new Inventario(1);
        Item espada=new Item(1, "Espada");
        inventario.adicionar(espada);
        ArrayList<Item> resultado = inventario.inverter();
        assertEquals(espada, resultado.get(0));
        assertEquals(1, resultado.size());
        
    }
    
    @Test
    public void inverterCenarioNormal(){
        Inventario inventario = new Inventario(1);
        Item espada=new Item(1, "Espada");
        Item bleh =new Item(1, "bleh");
        inventario.adicionar(espada);
        inventario.adicionar(bleh);
        ArrayList<Item> resultado = inventario.inverter();
        assertEquals(bleh, resultado.get(0));
        assertEquals(espada, resultado.get(1));
        
        assertEquals(2, resultado.size());
        
    }
    
    @Test 
    public void odernarInventarioVazio (){
        Inventario inventario = new Inventario(0);
   
        inventario.ordenarItens();
       
        assertTrue(inventario.getItens().isEmpty());
  
    }
    
    @Test
    public void ordenarComApenasUmItem (){
        Inventario inventario = new Inventario(1);
        Item espada=new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(espada));
        assertEquals(esperado, inventario.getItens());
       
    }
    
    @Test
    public void ordenarTotalmenteDesordenado(){
        Inventario inventario = new Inventario(1);
        Item espada=new Item(1, "Espada");
        Item bleh =new Item(2, "bleh");
        Item blabla =new Item(4, "blabla");
        inventario.adicionar(espada);
        inventario.adicionar(bleh);
        inventario.adicionar(blabla);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(espada, bleh, blabla));
        assertEquals(esperado, inventario.getItens());
       
    }
    
    @Test
    public void ordenarDescTotalmenteDesordenado(){
        Inventario inventario = new Inventario(3);
        Item espada=new Item(1, "Espada");
        Item bleh =new Item(2, "bleh");
        Item blabla =new Item(4, "blabla");
        inventario.adicionar(espada);
        inventario.adicionar(bleh);
        inventario.adicionar(blabla);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(blabla, bleh, espada));
        assertEquals(esperado, inventario.getItens());
       
    }
}
