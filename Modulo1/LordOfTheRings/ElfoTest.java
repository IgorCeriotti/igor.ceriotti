/*import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest
{
    @Test
    public void atirar3FlechasDevePerderFlechaAumentarXp(){
        // Arrange
        Elfo umElfo = new Elfo("Legolas"); //criei um elfo.
        Dwarf dwarf = new Dwarf ("Umdaet Broadminer");
        // Act
        umElfo.atirarFlecha(dwarf);
        umElfo.atirarFlecha(new Dwarf ("Ormaeck Wyvernmane"));
        umElfo.atirarFlecha(dwarf);//vou verificar a ação. (agi);
        // Assert
        assertEquals(2, umElfo.getExperiencia()); //experiencia deve ser igual a 2.
        assertEquals(0, umElfo.getFlecha().getQuantidade());
    }
    
    @Test
    public void testarSeElfoNasceCom2Flechas (){
        Elfo umElfo = new Elfo ("Legolas");
        assertEquals(2, umElfo.getFlecha().getQuantidade());
    }
    
    @Test 
    public void atirarFlechaEmDwarfTiraVida () {
        //Arrange
        Elfo umElfo = new Elfo ("Ayre Rorona");
        Dwarf dwarf = new Dwarf("Thurdril Underchin");
        //Act
        umElfo.atirarFlecha(dwarf); 
        //Assert
        assertEquals(100.0, dwarf.getLife(), .00001); 
        assertEquals(1, umElfo.getExperiencia()); 
        assertEquals(1, umElfo.getFlecha().getQuantidade());
    }
    
    /* @Test
    public void TestarSeAnaoPerdeVidaCorretamente (){
        Elfo umElfo = new Elfo("Legolas");
        Dwarf dwarf = new Dwarf ("Umivrid Grumblemaul");
        
        umElfo.atirarFlecha(dwarf);
        assertEquals(100.0,umElfo.getAnao().getLife(), 0.000001); 
        assertEquals(1, umElfo.getExperiencia()); 
        assertEquals(1, umElfo.getFlecha().getQuantidade());
        
    }*/
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;
public class ElfoTest
{
    @Test
    public void atirarFlechaDevePerderFlechaAumentarXp() {
        // Arrange
        Elfo umElfo = new Elfo("Legolas");
        Dwarf dwarf = new Dwarf("Barindrod Shatterdigger");
        // Act
        umElfo.atirarFlecha(dwarf);
        // Assert
        assertEquals(1, umElfo.getExperiencia());
        // Lei de Demeter - Law of Demeter
        //assertEquals(41, umElfo.getFlecha().getQuantidade());
        assertEquals(1, umElfo.getQtdFlechas());
    }

    @Test
    public void atirarFlecha3VezesDevePerderFlechaAumentarXp() {
        // Arrange
        Elfo umElfo = new Elfo("Legolas");
        Dwarf dwarf = new Dwarf("Barindrod Shatterdigger");
        // Act
        umElfo.atirarFlecha(dwarf);
        umElfo.atirarFlecha(new Dwarf("Kronabela Merryarm"));
        umElfo.atirarFlecha(new Dwarf("Weramoren Lightbreaker"));
        // Assert
        assertEquals(2, umElfo.getExperiencia());
        // Lei de Demeter - Law of Demeter
        //assertEquals(41, umElfo.getFlecha().getQuantidade());
        assertEquals(0, umElfo.getQtdFlechas());
    }

    @Test
    public void elfosNascemCom2Flechas() {
        Elfo umElfo = new Elfo("Elrond");
        assertEquals(2, umElfo.getQtdFlechas());
    }
    
    @Test
    public void atirarFlechaEmDwarfTiraVida() {
        // Arrange
        Elfo umElfo = new Elfo("Alanis Erna");
        Dwarf dwarf = new Dwarf("Skatmok Greatjaw");
        // Act
        umElfo.atirarFlecha(dwarf);
        // Assert
        assertEquals(100.0, dwarf.getVida(), .00001);
        assertEquals(1, umElfo.getExperiencia());
        assertEquals(1, umElfo.getQtdFlechas());
    }
    
    @Test
    public void elfoNasceComStatusRecemCriado (){
        Elfo elfo = new Elfo("Theoden Cairona");
        assertEquals(Status.RECEM_CRIADO, elfo.getStatus()); 
        
    }
}


