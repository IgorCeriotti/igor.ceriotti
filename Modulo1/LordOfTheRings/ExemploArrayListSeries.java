//import java.util.ArrayList;
//import java.util.LinkedList; 
import java.util.*;

public class ExemploArrayListSeries {
    public void indicar(){
        ArrayList<String> lista = new ArrayList<>();
        lista.add("Stranger Things");
        lista.add("True Detective");
        lista.add("Mr. Robot");
        lista.add("Punisher");
        lista.add("Dark");
        lista.add("Dexter");
        lista.add("Breaking Bad");
        lista.add("Game of Thrones");
        lista.add("How I Met Your Mother");
        lista.add("Westworld");
        lista.add("A Casa De Papel");
        lista.add("Black Mirror");
        lista.add("Veep");
        lista.add("Lucifer");
        lista.add("The Last Kingdom");
        lista.add("Two and a Half Men");
        
        for(int i = 0; i < lista.size(); i++){
            System.out.println(lista.get(i));
        }
        
        boolean taVazia = lista.size() == 0; //lista.isEmpty();
        System.out.println(taVazia);
        
        lista.remove(8); 
        
        //faz o mesmo que o for tradicional. 
        for(String serie : lista){
            System.out.println(serie);
        }
        
    }
}
